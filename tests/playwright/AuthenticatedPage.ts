import { LoginBoxPage } from './LoginBoxPage';

export class AuthenticatedPage extends LoginBoxPage {
  public generatedToken: string;

  constructor(loginBoxPage: LoginBoxPage, token: string) {
    super(loginBoxPage.page);
    this.generatedToken = token;
  }
}
