import { test } from './content-registration-test';
import { expect } from '@playwright/test';

// test('basic test', async ({ page }) => {
//   await page.goto('https://playwright.dev/');
//   const title = page.locator('.navbar__inner .navbar__title');
//   await expect(title).toHaveText('Playwright');
// });

test.describe.parallel('App suite', () => {
  test('Interface messages change on selection of a new locale', async ({
    page,
  }) => {
    await page.goto('/records');
    await page.getByRole('button', { name: 'en' }).click();
    await page.getByText('Deutsch').click();
    await page
      .locator('[data-test="content_registration\\.home\\.title"]')
      .click();
    await expect(
      page.locator('[data-test="content_registration\\.home\\.title"]')
    ).toContainText('Beginnen Sie mit der Einreichung des Datensatzes');
  });
  test('Navigate to grant registration', async ({ page }, _testInfo) => {
    await page.goto('/records');
    await expect(
      page.locator('[data-test="content_registration\\.home\\.title"]')
    ).toContainText('Start record submission');
  });
  test('Site has footer', async ({ page }) => {
    await page.goto('/');
    expect(
      await page.locator('footer:has-text("Creative Commons")')
    ).toBeVisible();
    expect(await page.locator('footer:has-text("crossref.org")')).toBeVisible();
    expect(await page.locator('footer:has-text("privacy")')).toBeVisible();
  });
  test('License link is correct', async ({ page }) => {
    await page.goto('/');
    const link = page.locator(
      'a:has-text("Creative Commons Attribution 4.0 International License")'
    );
    expect(link).toHaveAttribute(
      'href',
      'https://creativecommons.org/licenses/by/4.0/'
    );
  });
  test('Non-error notifications are dismissed automatically', async ({
    page,
  }) => {
    await page.goto('/');
    // Click .v-btn >> nth=0
    await page.locator('[test-id="dev-settings"]').click();

    // Click button:has-text("Success")
    await page.locator('button:has-text("Success")').click();

    const alert = await page.locator(
      'div[role="status"]:has-text("A test success alert")'
    );
    await page.waitForTimeout(2000);
    await expect(alert).not.toBeVisible();
  });
});

test('Navigating to an unprotected URL does not require authentication', async ({
  page,
}) => {
  await page.goto('/');
  await expect(page.locator('text=Start record submission')).toBeVisible();
});
