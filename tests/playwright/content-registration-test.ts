// content-registration-test.ts
/**
 * Extends playwright/test to add custom parameters
 */
import { test as base } from './fixtures/interceptPage';
import { LOGIN_USERNAME_ONE_ROLE, LOGIN_PASSWORD_ONE_ROLE } from '../constants';

export type TestOptions = {
  username: string;
  password: string;
};

export const test = base.extend<TestOptions>({
  // Default value for the person.
  username: LOGIN_USERNAME_ONE_ROLE,
  password: LOGIN_PASSWORD_ONE_ROLE,
});
