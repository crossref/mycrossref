import { Locator, Page } from '@playwright/test';

export class LoginBoxPage {
  readonly usernameInput: Locator;
  readonly passwordInput: Locator;
  readonly loginButton: Locator;
  readonly userMenuTrigger: Locator;
  readonly logoutButton: Locator;
  readonly page: Page;

  constructor(page: Page) {
    this.page = page;
    this.usernameInput = page.locator('input[name="username"]');
    this.passwordInput = page.locator('input[name="password"]');
    this.loginButton = page.locator('button:has-text("Login")');
    this.userMenuTrigger = page.locator('[data-test-id="user-menu-trigger"]');
    this.logoutButton = page.getByText('Log out');
  }

  async visit() {
    this.page.goto('/login');
  }

  async enterCredentials(username: string, password: string) {
    await this.usernameInput.fill(username);
    await this.passwordInput.fill(password);
  }

  async clickLogin() {
    await this.loginButton.click();
  }

  async login(username: string, password: string) {
    await this.enterCredentials(username, password);
    await this.clickLogin();
  }
}
