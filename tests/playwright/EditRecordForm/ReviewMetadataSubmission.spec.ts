import { articleFormWithIssueStep, expect } from './my-setup';

articleFormWithIssueStep(
  'CR-1896 Submission review page: Headings without content should not be listed ',
  { tag: '@smoke' },
  async ({ form }) => {
    const nextBtn = form.getByRole('button', { name: 'Next' });
    await expect(form.getByLabel('Volume number (optional)')).toBeVisible();

    await expect(form.getByLabel('Issue number (optional)')).toBeVisible();
    await nextBtn.click();
    await expect(
      form
        .locator('[id="\\#\\/properties\\/familyName3"]')
        .getByText('Carberry')
    ).toBeVisible();
    await expect(form.locator('body')).toContainText('#1');
    await expect(form.locator('h1')).toContainText('Review Metadata');
    const heading = form.getByRole('heading', { name: 'Issue', exact: true });

    // Assert that the heading is not visible
    await expect(heading).not.toBeVisible();
  }
);
