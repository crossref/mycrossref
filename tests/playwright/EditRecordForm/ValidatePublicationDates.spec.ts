import { articleToPubDates as test, expect } from './my-setup';

//TODO: turn this form fill-out into a shared test fixture
test(
  'Prevents progression without publication date',
  { tag: '@smoke' },
  async ({ form }) => {
    await form.getByRole('button', { name: 'Next' }).click();
    await expect(
      form.getByText('Online ISSN of journal').first()
    ).not.toBeVisible();
  }
);

test(
  'Allows progression with publication date',
  { tag: '@smoke' },
  async ({ form }) => {
    await form.getByLabel('Published In Print').fill('2024-04-12');
    await form.getByRole('button', { name: 'Next' }).click();
    await expect(
      form.getByText('Online ISSN of journal').first()
    ).toBeVisible();
  }
);
