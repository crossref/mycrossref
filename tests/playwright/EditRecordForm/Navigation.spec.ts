import { test, expect } from '../auth.fixtures';
import { ContentRegistrationPage } from '../ContentRegistrationPage';
import { strict as assert } from 'assert';
import {
  clickNext,
  fillArticleForm,
  fillArticleStep,
  fillPublicationDates,
  mockConvertSuccess,
  withMockLoginAndDeposits,
} from './my-setup';

withMockLoginAndDeposits(
  'Fills, submits and restarts record form in a new journal',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertSuccess(form);
    const contentRegistrationPage = new ContentRegistrationPage(form);
    await contentRegistrationPage.loadSubmitAndRestartInNewJournal();
  }
);

withMockLoginAndDeposits(
  'Fills, submits and restarts record form in the same journal',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertSuccess(form);
    const contentRegistrationPage = new ContentRegistrationPage(form);
    await contentRegistrationPage.loadSubmitAndRestartInSameJournal();
    await fillArticleStep(form);
    await fillPublicationDates(form);
    await form.getByRole('button', { name: 'Next' }).click();
    await expect(form.getByLabel('Full title of journal*')).toHaveValue(
      'JOURNAL FULL TITLE'
    );
  }
);

withMockLoginAndDeposits(
  'Fills, submits and restarts record form in the same journal and issue',
  { tag: '@smoke' },
  async ({ form }) => {
    await mockConvertSuccess(form);
    const contentRegistrationPage = new ContentRegistrationPage(form);
    await contentRegistrationPage.loadSubmitAndRestartInSameJournalAndIssue();
    await fillArticleStep(form);
    await fillPublicationDates(form);
    await form.getByRole('button', { name: 'Next' }).click();
    await expect(form.getByLabel('Full title of journal*')).toHaveValue(
      'JOURNAL FULL TITLE'
    );
    await form.getByRole('button', { name: 'Next' }).click();
    await expect(form.getByLabel('Issue number (optional)')).toHaveValue('11');
    await expect(form.getByLabel('Volume number (optional)')).toHaveValue('5');
  }
);

withMockLoginAndDeposits(
  'Confirms navigation away from a dirty form',
  { tag: '@smoke' },
  async ({ form }) => {
    const contentRegistrationPage = new ContentRegistrationPage(form);
    await contentRegistrationPage.loadJournalArticleRecord();
    await form.getByLabel('Article title*').fill('A new title');
    // wait for the debounced update to the form data
    await form.waitForTimeout(500);
    // navigate away from the current route
    await form.goBack();
    await expect(
      form.getByText('Are you sure you want to leave?')
    ).toBeVisible();
  }
);

test(
  'Allows immediate navigation away from a clean form',
  { tag: '@smoke' },
  async ({ authenticatedPage }) => {
    const contentRegistrationPage = new ContentRegistrationPage(
      authenticatedPage.page
    );
    await contentRegistrationPage.loadJournalArticleRecord();
    // await page.getByLabel('Article title*').fill('A new title');
    // wait for the debounced update to the form data
    // await page.goto('/records');
    await authenticatedPage.page.goBack();
    await expect(
      authenticatedPage.page.getByRole('button', { name: 'New record' })
    ).toBeVisible();
  }
);
