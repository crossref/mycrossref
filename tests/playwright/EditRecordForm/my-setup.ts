import { Page } from 'playwright';
import { test as BaseTest, expect } from '../auth.fixtures';

// const BaseTest = BaseTestPw.extend({
//   page: async ({ page }, use) => {
//     await page.route(
//       'https://status.crossref.org/embed/script.js',
//       async (route, request) => {
//         route.fulfill({
//           status: 200,
//           body: '',
//         });
//       }
//     );
//     await use(page);
//   },
// });

// Mocks
const mockConvertSuccess = async (page: Page, delay = 0) => {
  await page.route('**/beta/convert', async (route, request) => {
    await new Promise((f) => setTimeout(f, delay));
    route.fulfill({
      status: 200,
      body: '<xml></xml>',
    });
  });
};

const mockConvertFailure = async (page: Page, delay = 0, code = 400) => {
  await page.route('**/beta/convert', async (route, request) => {
    await new Promise((f) => setTimeout(f, delay));
    let body = '';
    switch (code) {
      case 400:
        body = `{
  "valid" : false,
  "evaluationPath" : "$",
  "schemaLocation" : "https://crossref.org/item-graph/schemas/api-convert-request.schema.json#",
  "instanceLocation" : "$",
  "details" : [ {
    "valid" : false,
    "evaluationPath" : "$.properties.itemTree['$ref'].properties.root['$ref'].properties.relationships.items.properties.object['$ref'].properties.relationships.items.properties.object['$ref'].properties.identifiers.items",
    "schemaLocation" : "https://crossref.org/item-graph/schemas/item.schema.json#/properties/identifiers/items",
    "instanceLocation" : "$.itemTree.root.relationships[2].object.relationships[0].object.identifiers[0]",
    "errors" : {
      "type" : "boolean found, string expected"
    }
  } ]
}`;
        break;
      case 500:
        body = `{
    "status": "error",
    "message-type": "internal-server-error",
    "message-version": "1.0.0",
    "message": "Internal server error"
}`;
        break;
      case -1:
        route.abort();
        return;
    }
    route.fulfill({
      status: code,
      body: body,
    });
  });
};

const mockLoginSuccess = async (page: Page, delay = 0) => {
  await page.route('**/servlet/login', async (route, request) => {
    await new Promise((f) => setTimeout(f, delay));
    let postData: any = {};
    const contentType = request.headers()['content-type'];

    if (contentType === 'application/x-www-form-urlencoded') {
      const searchParams = new URLSearchParams(request.postData());
      searchParams.forEach((value, key) => {
        postData[key] = value;
      });
    } else if (contentType === 'application/json') {
      postData = JSON.parse(request.postData() || '{}');
    }

    const { usr, pwd, role } = postData;

    if (!role) {
      route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
          success: true,
          message: 'Authenticated',
          roles: ['onerole'],
          authenticated: true,
          authorised: false,
        }),
      });
    } else if (role) {
      route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
          success: true,
          message: 'Authorised',
          redirect: null,
          authenticated: true,
          authorised: true,
        }),
      });
    }
  });
};

const mockDepositSuccess = async (page: Page, delay = 0) => {
  await page.route('https://test.crossref.org/v2/deposits', async (route) => {
    await new Promise((f) => setTimeout(f, delay));
    route.fulfill({
      status: 200,
      contentType: 'text/xml;charset=UTF-8',
      body: `<?xml version="1.0" encoding="UTF-8"?>
<doi_batch_diagnostic status="completed" sp="arc.local">
   <submission_id>1403850957</submission_id>
   <batch_id>org.crossref.agilmartin.0001</batch_id>
   <record_diagnostic status="Success">
      <doi>10.5555/agilmartin/1</doi>
      <msg>Successfully updated</msg>
      <citations_diagnostic deferred="1403850958" />
   </record_diagnostic>
   <batch_data>
      <record_count>1</record_count>
      <success_count>1</success_count>
      <warning_count>0</warning_count>
      <failure_count>0</failure_count>
   </batch_data>
</doi_batch_diagnostic>
`,
    });
  });
};

const mockDepositFailure = async (page: Page, delay = 0, code = 500) => {
  await page.route('https://test.crossref.org/v2/deposits', (route) => {
    page.waitForTimeout(delay);
    switch (code) {
      case -1:
        route.abort();
        return;
      case 200:
        route.fulfill({
          status: code,
          contentType: 'text/xml;charset=UTF-8',
          body: `<?xml version="1.0" encoding="UTF-8"?>
<doi_batch_diagnostic status="completed" sp="cskAir.local">
   <submission_id>1407395745</submission_id>
   <batch_id>test201707181318</batch_id>
   <record_diagnostic status="Failure" msg_id="4">
      <doi>10.5555/doitestwithcomponent_2</doi>
      <msg>Record not processed because submitted version: 2012061910233600835 is less or equal to previously submitted version (DOI match)</msg>
   </record_diagnostic>
   <batch_data>
      <record_count>1</record_count>
      <success_count>0</success_count>
      <warning_count>0</warning_count>
      <failure_count>1</failure_count>
   </batch_data>
</doi_batch_diagnostic>`,
        });
        return;
      case 403:
        route.fulfill({
          status: code,
          contentType: 'text/xml;charset=UTF-8',
          body: `<?xml version="1.0" encoding="UTF-8"?>
<doi_batch_diagnostic status="completed" sp="a-cs1">
   <submission_id>1432497879</submission_id>
   <batch_id>batch_000001</batch_id>
   <record_diagnostic status="Failure" msg_id="6">
      <doi />
      <msg>User not allowed to add or modify records for prefix: 10.32013</msg>
   </record_diagnostic>
   <batch_data>
      <record_count>1</record_count>
      <success_count>0</success_count>
      <warning_count>0</warning_count>
      <failure_count>1</failure_count>
   </batch_data>
</doi_batch_diagnostic>`,
        });
        return;
      case 500:
      default:
        route.fulfill({
          status: code,
          contentType: 'application/json',
          body: JSON.stringify({
            success: false,
            errorMessage: 'Unauthorized access',
          }),
        });
        return;
    }
  });
};

export const fillArticleStep = async (page: Page) => {
  await page.getByLabel('Title*').click();
  await page.getByLabel('Title*').fill('Test Article Title');
  await page.getByLabel('Title*').press('Tab');
  await page.getByLabel('DOI*').fill('10.55555/1234567890');
  await page.getByLabel('DOI*').press('Tab');
  await page
    .getByLabel('Article landing page URL*')
    .fill('https://crossref.org/test-article');
  await page.getByLabel('Article landing page URL*').press('Tab');
  await page.getByLabel('Family name(s) / surname(s)*').click();
  await page.getByLabel('Family name(s) / surname(s)*').fill('Carberry');
};

// Actions
const fillArticleForm = async (page: Page) => {
  await page.goto('/records/new/journal-article');
  await fillArticleStep(page);
};

export const fillPublicationDates = async (page: Page) => {
  await page.getByLabel('Published In Print*').fill('2024-04-12');
};

const completeIssueStep = async (page: Page) => {
  await fillPublicationDates(page);

  await page.getByRole('button', { name: 'Next' }).click();
  await page.getByLabel('Online ISSN of journal*').click();
  await page.getByLabel('Online ISSN of journal*').fill('1111-1111');
  await page.getByLabel('Full Title of Journal*').click();
  await page
    .getByLabel('Full Title of Journal*')
    .fill('The Journal of Test Metadata');

  await page
    .locator(
      '[data-test-id="journal-article-form-next-button"][data-test-can-proceed="true"]'
    )
    .waitFor();
  await page.getByRole('button', { name: 'Add Issue Metadata' }).click();
};

export const clickNext = async (page: Page) => {
  const btn = page.locator(
    '[data-test-id="journal-article-form-next-button"][data-test-can-proceed="true"]'
  );
  await btn.click();
};

const newJournalArticleForm = BaseTest.extend({
  form: async ({ authenticatedPage }, use) => {
    const page = authenticatedPage.page;

    await page.goto('/records/new/journal-article');
    await use(page);
  },
});

const articleFormWithIssueStep = BaseTest.extend({
  form: async ({ authenticatedPage }, use) => {
    const page = authenticatedPage.page;

    // await mockLoginSuccess(page);
    await fillArticleForm(page);
    await completeIssueStep(page);
    await use(page);
  },
});

const articleAtReviewStep = BaseTest.extend({
  form: async ({ authenticatedPage }, use) => {
    const page = authenticatedPage.page;

    await fillArticleForm(page);
    await completeIssueStep(page);
    await clickNext(page);
    await use(page);
  },
});

const articleToPubDates = BaseTest.extend({
  form: async ({ authenticatedPage }, use) => {
    const page = authenticatedPage.page;

    await mockLoginSuccess(page);
    await fillArticleForm(page);
    await use(page);
  },
});

const articleWithMockDeposits = BaseTest.extend({
  form: async ({ page }, use) => {
    // await mockLoginSuccess(page);
    await mockDepositSuccess(page);
    await fillArticleForm(page);
    await completeIssueStep(page);
    await use(page);
  },
});

const withMockLoginAndDeposits = BaseTest.extend({
  form: async ({ authenticatedPage }, use) => {
    const page = authenticatedPage.page;

    // await mockLoginSuccess(page);
    await mockDepositSuccess(page);
    await use(page);
  },
});

export {
  newJournalArticleForm,
  articleFormWithIssueStep,
  articleToPubDates,
  articleWithMockDeposits,
  articleAtReviewStep,
  expect,
  mockConvertFailure,
  mockConvertSuccess,
  mockLoginSuccess,
  mockDepositFailure,
  mockDepositSuccess,
  withMockLoginAndDeposits,
};
