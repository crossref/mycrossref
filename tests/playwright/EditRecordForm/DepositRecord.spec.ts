import { test, expect } from '../auth.fixtures';
import { ContentRegistrationPage } from '../ContentRegistrationPage';

test.describe('Deposit', () => {
  let contentRegistrationPage: ContentRegistrationPage;

  test.beforeEach(async ({ authenticatedPage }) => {
    // Create an instance of ContentRegistrationPage using authenticatedPage.page
    contentRegistrationPage = new ContentRegistrationPage(
      authenticatedPage.page
    );
  });

  test('Sends token with deposit', async ({ authenticatedPage }) => {
    await contentRegistrationPage.goto();
    await contentRegistrationPage.loadRecord(
      'tests/resources/json.formdata/journal_article_1.download.json'
    );
    await authenticatedPage.page.getByRole('button', { name: 'Next' }).click();
    await authenticatedPage.page.getByRole('button', { name: 'Next' }).click();
    await authenticatedPage.page.getByRole('button', { name: 'Next' }).click();

    // Set up the wait to capture the request to the deposit endpoint
    const depositRequestPromise = authenticatedPage.page.waitForRequest(
      (request) =>
        request.url() === 'https://test.crossref.org/v2/deposits' &&
        request.method() === 'POST'
    );
    await authenticatedPage.page
      .locator('[data-test-id="journal-article-form-next-button"]')
      .getByRole('button', { name: 'Submit' })
      .click();

    // Wait for the deposit request to be made and capture it
    const depositRequest = await depositRequestPromise;

    // Assert that the Authorization header matches the generated token
    expect(depositRequest.headers()['authorization']).toBe(
      `Bearer ${authenticatedPage.generatedToken}`
    );
  });
});
