import { test, expect } from '../auth.fixtures';
import {newJournalArticleForm} from "./my-setup";

const TEST_ARTICLE_TITLE_1 = 'Test Article Title Records.spec.ts : 1'
const TEST_ARTICLE_TITLE_2 = 'Test Article Title Records.spec.ts : 2'

async function testFieldPersistence({ form, page, fieldLabel, expectedValue }) {
  await form.getByLabel(fieldLabel).fill(expectedValue);
  await page.waitForTimeout(1000); // Allow the debounce to fire.
  await page.reload();

  const fieldInput = await form.getByLabel(fieldLabel);
  const fieldValue = await fieldInput.inputValue();
  await expect(fieldValue).toBe(expectedValue);
}

newJournalArticleForm(
  'Persists record to drafts on updates',
  {},
  async ({ form, page }) => {
    await testFieldPersistence({
      form,
      page,
      fieldLabel: 'Article Title',
      expectedValue: TEST_ARTICLE_TITLE_1
    });

    await testFieldPersistence({
      form,
      page,
      fieldLabel: 'Article Title',
      expectedValue: TEST_ARTICLE_TITLE_2
    });
  }
);

test('Redirects to Records Home for an unknown draft record', async ({authenticatedPage, page}) => {
  await page.goto(`/records/not-a-valid-record`);
  await expect(page.getByRole('button', { name: 'New record' })).toBeVisible();
})

test('Redirects to Records Home for an unknown completed record', async ({authenticatedPage, page}) => {
  await page.goto(`/records/not-a-valid-record/complete`);
  await expect(page.getByRole('button', { name: 'New record' })).toBeVisible();
})


