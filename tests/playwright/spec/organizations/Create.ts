import { OrganizationApiKeysTable } from './OrganizationApiKeysTable';
import { Answerable, Task } from '@serenity-js/core';
import { By, Click, Enter, PageElement } from '@serenity-js/web';
import { enterTextIntoDialog } from '../global-dialog/Dialog';

/**
 * This is called a "Lean Page Object".
 * Lean Page Objects describe interactive elements of a widget.
 * In this case, the create new organization form
 */
export const CreateOrganizationForm = {
  nameField: () =>
    PageElement.located(By.id('#/properties/name-input')).describedAs(
      'name field'
    ),

  memberIdField: () =>
    PageElement.located(By.id('#/properties/memberId-input  ')).describedAs(
      'member id field'
    ),

  createButton: () =>
    PageElement.located(
      By.cssContainingText('button', 'Create organisation')
    ).describedAs('create organisationn button'),

  cancelButton: () =>
    PageElement.located(By.cssContainingText('button', 'Cancel')).describedAs(
      'Cancel create organisationn button'
    ),
};

export const createNewOrganizaton = {
  using: (name: Answerable<string>, memberId: Answerable<number>) =>
    Task.where(
      `#actor creates new organisation called ${name}`,
      Enter.theValue(name).into(CreateOrganizationForm.nameField()),
      Enter.theValue(memberId).into(CreateOrganizationForm.memberIdField()),
      Click.on(CreateOrganizationForm.createButton())
    ),
};

export const createNewApiKeyDescribedAs = (description: Answerable<string>) =>
  Task.where(
    `#actor adds an API key described as ${description}`,
    Click.on(OrganizationApiKeysTable.addButton()),
    enterTextIntoDialog(description)
  );
