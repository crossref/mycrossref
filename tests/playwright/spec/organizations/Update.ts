import { Answerable, Task } from '@serenity-js/core';
import { By, Click, Enter, PageElement } from '@serenity-js/web';
import { enterTextIntoDialog, GlobalDialog } from '../global-dialog/Dialog';

/**
 * This is called a "Lean Page Object".
 * Lean Page Objects describe interactive elements of a widget.
 * In this case, the create new organization form
 */
const UpdateOrganizationDialog = {
  nameField: () =>
    PageElement.located(By.id('#/properties/name-input')).describedAs(
      'name field'
    ),

  memberIdField: () =>
    PageElement.located(By.id('#/properties/memberId-input  ')).describedAs(
      'member id field'
    ),

  okButton: () =>
    PageElement.located(
      By.cssContainingText('button', 'Create organisationn')
    ).describedAs('create organisationn button'),
};

export const updateOrganizationName = (name: Answerable<string>) =>
  Task.where(
    `#actor updates the organisation name to ${name}`,
    enterTextIntoDialog(name)
  );
