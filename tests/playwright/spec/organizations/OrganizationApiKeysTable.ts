import { getTableRowClass } from './../../../../src/core/apiKeys';
import { equals } from '@serenity-js/assertions';
import { Answerable, Task, q } from '@serenity-js/core';
import { By, Click, PageElement, PageElements, Text } from '@serenity-js/web';

export const OrganizationApiKeysTable = {
  container: () => PageElement.located(By.css('.api-keys-table')),
  addButton: () =>
    PageElement.located(By.css('.api-keys-card__add-new-key-button')),
  rowContextMenuTrigger: () =>
    PageElement.located(
      By.css('.api-keys-table__table-row__context-menu-trigger')
    ),
  rowContextMenuActivateKeyButton: () =>
    PageElement.located(
      By.css('.api-keys-table__context-menu__toggle-key-button')
    ),
  rowHelpDialogTrigger: () =>
    PageElement.located(
      By.css('[data-test-id="key-mananger-api-key-disabled-show-help-trigger"]')
    ),
  deleteButton: () =>
    PageElement.located(
      By.css('.api-keys-table__context-menu__delete-key-button')
    ),
  keyId: () =>
    PageElement.located(By.css('.api-keys-table__table-row__key-id')),
  active: () =>
    PageElement.located(By.css('.api-keys-table__table-row__enabled')),
  status: () =>
    PageElement.located(By.css('.api-keys-table__table-row__status')),
  deleteEntry: () => {
    return;
  },
};

export const firstApiKeyRow = () =>
  PageElement.located(By.css('.api-keys-table__table-row'));

export const rowByKeyId = (keyId: Answerable<string>) =>
  PageElement.located(By.css(q`.api-keys-table__row-by-id__${keyId}`)).of(
    OrganizationApiKeysTable.container()
  );

export const cellByKeyId = (keyId: Answerable<string>) =>
  PageElement.located(
    By.cssContainingText('.api-keys-table__table-row__key-id', keyId)
  ).of(OrganizationApiKeysTable.container());

export const rowByDescription = (descriptionText: string) =>
  PageElements.located(By.css('tr')).where(
    Text.of(PageElement.located(By.css('td'))),
    equals(descriptionText)
  );

export const deleteApiKeyById = (
  keyId: Answerable<string>,
  row: Answerable<PageElement>
) =>
  Task.where(
    `#actor deletes API key ${keyId}`,
    Click.on(OrganizationApiKeysTable.rowContextMenuTrigger().of(row)),
    Click.on(OrganizationApiKeysTable.deleteButton().of(row))
  );
