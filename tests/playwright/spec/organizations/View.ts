import { By, PageElement } from '@serenity-js/web';

export const OrganizationCard = {
  editButton: () =>
    PageElement.located(By.css('.organization-card__edit-name-button')),
};

export const OrganizationsUsersTable = {
  editButton: () => PageElement.located(By.css('')),
};
