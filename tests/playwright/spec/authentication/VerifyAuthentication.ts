import { Ensure, equals, includes } from '@serenity-js/assertions';
import { Answerable, Log, Task, Wait } from '@serenity-js/core';
import { By, isVisible, PageElement, Text } from '@serenity-js/web';

/**
 * VerifyAuthentication aggregates several tasks to make them easier to find:
 * - VerifyAuthentication.succeeded()
 * - VerifyAuthentication.failed()
 */
export class VerifyAuthentication {
  private static hasUserMenu = () => {
    return Task.where(
      `#actor verifies that user menu is present`,
      Wait.until(UserMenu.userMail(), isVisible())
    );
  };

  private static hasErrorMessage = () => {
    return Task.where(
      `#actor verifies that error message is present`,
      Ensure.that(LoginPage.errorMessage(), isVisible())
    );
  };

  static succeeded = (username: Answerable<string>) =>
    Task.where(
      `#actor verifies that authentication has succeeded`,
      VerifyAuthentication.hasUserMenu(),
      Ensure.that(Text.of(UserMenu.userMail()), equals(username))
    );

  static failed = () =>
    Task.where(
      `#actor verifies that authentication has failed`,
      VerifyAuthentication.hasErrorMessage(),
      Ensure.that(
        Text.of(LoginPage.errorMessage()),
        includes('Invalid username or password.')
      )
    );
}

/**
 * A tiny Lean Page Object, representing the user menu
 * that shows up when the user is logged in.
 */
const FlashMessages = {
  flashAlert: () =>
    PageElement.located(By.id('flash')).describedAs('flash message'),
};

export const UserMenu = {
  trigger: () =>
    PageElement.located(By.css('[data-test-id="user-menu-trigger"]')),
  username: () =>
    PageElement.located(By.css('[data-test-id="user-menu-username"]')),
  userMail: () =>
    PageElement.located(By.id('user-menu')).describedAs('user menu'),
  logOutTrigger: () =>
    PageElement.located(By.css('[data-test-id="user-menu-logout-trigger"]')),
};

const LoginPage = {
  errorMessage: () =>
    PageElement.located(By.id('input-error')).describedAs(
      'login error message'
    ),
};

const OrgPage = {
  testMessage: () =>
    PageElement.located(
      By.id('test-message').describedAs('test message container')
    ),
};
