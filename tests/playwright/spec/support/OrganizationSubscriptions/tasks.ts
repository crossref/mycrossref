import { contain, Ensure, equals, not } from '@serenity-js/assertions';
import { Log, notes, Task, Wait } from '@serenity-js/core';
import { Click, CssClasses, isVisible, Value } from '@serenity-js/web';

import { subscriptionEditTrigger } from './../Organizations/questions';
import {
  acceptButton,
  checked,
  dialog,
  metadataPlusSubscriptionSwitch,
  metadataPlusSubscriptionSwitchInput,
  metadataPlusSubscriptionSwitchTrigger,
  subscriptionEditDialogCancelButton,
} from './questions';
import { MyNotes } from '../../staff-managing-organizations.spec';

export const toggleTheMetaDataPlusServiceSubscription = () =>
  Task.where(
    `#actor disables the Metadata Plus service subscription`,
    notes<MyNotes>().set(
      'metadataPlusSubscriptionEnabled',
      metadataPlusSubscriptionSwitchInput().isSelected() ? 'true' : 'false'
    ),
    Log.the(notes().get('metadataPlusSubscriptionEnabled')),
    Click.on(metadataPlusSubscriptionSwitchTrigger()),
    Click.on(acceptButton()),
    Wait.until(
      CssClasses.of(metadataPlusSubscriptionSwitch()),
      not(contain('v-switch--loading'))
    ),
    Log.the(metadataPlusSubscriptionSwitchInput().isSelected()),
    Ensure.that(
      metadataPlusSubscriptionSwitchInput().isSelected(),
      not(equals(notes().get('metadataPlusSubscriptionEnabled')))
    )
  );

export const openTheOrganizationSubscriptionsDialog = () =>
  Task.where(
    `#actor opens the edit subscription modal dialog`,
    Click.on(subscriptionEditTrigger()),
    Ensure.that(subscriptionEditDialogCancelButton(), isVisible())
  );
