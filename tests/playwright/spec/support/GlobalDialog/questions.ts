import { By, PageElement } from '@serenity-js/web';
export const Dialog = {
  confirmButton: () =>
    PageElement.located(By.css('.global-dialog__confirm-button')),
};
