import { Ensure, equals, isGreaterThan } from '@serenity-js/assertions';
import { Answerable, Task, Wait } from '@serenity-js/core';
import { Click, Enter } from '@serenity-js/web';
import {
  ensure,
  isDefined,
  isGreaterThan as TTisGreaterThan,
  isInteger,
  TinyType,
} from 'tiny-types';

import { ORGANIZATIONS } from './../test-data';
import {
  organizationResultByName,
  organizationSearchResultNames,
  searchOrganizationsInput,
} from './questions';

export class OrganizationId extends TinyType {
  constructor(public readonly value: number) {
    super();
    ensure(
      'OrganizationId',
      value,
      isDefined(),
      isInteger(),
      TTisGreaterThan(0)
    );
  }
}

export const searchForOrganizationsMatching = (query: Answerable<string>) =>
  Task.where(
    `#actor searches for Organisationns matching ${query}`,
    Enter.theValue(query).into(searchOrganizationsInput()),
    /* FIXME: should this just wait for *any* search result to be returned? */
    Wait.until(organizationSearchResultNames().length, isGreaterThan(0))
  );

export const selectOrganizationByName = (name: Answerable<string>) =>
  Task.where(
    `#actor selects the organisation called ${name}`,
    Click.on(organizationResultByName(name))
  );
