import { By, PageElement, Text } from '@serenity-js/web';

export const organizationCard = () =>
  PageElement.located(By.css('.organization-card'));
export const cardTitle = () => PageElement.located(By.css('.v-card-title'));
export const textOfCardTitle = () => Text.of(cardTitle());
export const organizationCardTitle = () =>
  textOfCardTitle().of(organizationCard());
export const organizationSettingsList = () =>
  PageElement.located(By.css('.organization-settings-list'));
export const listItemTitle = () =>
  PageElement.located(By.css('.v-list-item-title'));
export const listItemSubTitle = () =>
  PageElement.located(By.css('.v-list-item-subtitle'));
export const textOfListItemTitle = () => Text.of(listItemTitle());
export const textOfListItemSubTitle = () => Text.of(listItemSubTitle());
export const organizationSettingsListOrganizationName = () =>
  textOfListItemSubTitle().of(
    PageElement.located(By.css('.organization-card__organization-name'))
  );
export const organizationSettingsListOrganizationId = () =>
  textOfListItemSubTitle().of(
    PageElement.located(By.css('.organization-card__crossref-id'))
  );
// export const toggleSwitch = () =>
//   PageElement.located(By.css('.v-switch input[type="checkbox"]'));
// Until the fix for https://github.com/vuetifyjs/vuetify/issues/18024 is released
// v-switch needs to wrapped in a div to catch clicks when it's set as read-only
export const toggleSwitch = () =>
  PageElement.located(By.css('div.switch-wrapper'));
export const subscriptionEditTrigger = () =>
  PageElement.located(By.css('.subscription-card__edit-trigger'));
export const createNewOrganizationButton = () =>
  PageElement.located(
    By.cssContainingText('.v-btn__content', 'Create new organisation')
  );
