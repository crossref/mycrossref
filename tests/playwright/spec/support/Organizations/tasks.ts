import { createNewOrganizationButton } from './questions';
import { Ensure } from '@serenity-js/assertions';
import { Task, q } from '@serenity-js/core';
import { Click, Navigate, isVisible } from '@serenity-js/web';

import { metadataPlusSubscriptionSwitch } from '../OrganizationSubscriptions/questions';

export const startOnTheOrganizationsLandingPage = () =>
  Task.where(
    `#actor starts on the Organisations Landing Page`,
    Navigate.to('/keys')
  );

export const startOnTheLandingPageForOrganization = (organizationId: number) =>
  Task.where(
    `#actor starts on the Landing Page for Organization ${organizationId}`,
    Navigate.to(`/keys/${organizationId}`)
  );

export const goToTheAdminLandingPageForOrganization = (organizationId: any) =>
  Task.where(
    `#actor starts on the Landing Page for Organization ${organizationId}`,
    Navigate.to(q`/keys/${organizationId}`)
  );

export const toggleTheMetaDataPlusService = () =>
  Task.where(
    `#actor toggles the metadata plus service`,
    Click.on(metadataPlusSubscriptionSwitch())
  );
