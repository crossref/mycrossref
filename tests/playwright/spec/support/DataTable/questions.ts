import { By, PageElement } from '@serenity-js/web';
export const contextMenuTrigger = () =>
  PageElement.located(By.css('.v-btn')).of(
    PageElement.located(By.css('td:last'))
  );
