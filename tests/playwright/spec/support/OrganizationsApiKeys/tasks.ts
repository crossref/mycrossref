import { GlobalDialog } from './../../global-dialog/Dialog';
import { contextMenuTrigger } from './../DataTable/questions';
import {
  OrganizationApiKeysTable,
  cellByKeyId,
  rowByKeyId,
} from './../../organizations/OrganizationApiKeysTable';
import { isPresent, Ensure, equals } from '@serenity-js/assertions';
import { PageElement, By, Click, isVisible, Text } from '@serenity-js/web';
import { LoginAsStaffer } from './../../authentication/Authenticate';
import { goToTheAdminLandingPageForOrganization } from './../Organizations/tasks';
import { Answerable, Log, Task, Wait, notes } from '@serenity-js/core';
import {
  ObtainAccessToken,
  createApiKey,
} from '../../../../screenplay/tasks/SetupKeycloak';
export const disableApiKey = (testInfo) =>
  Task.where(
    `#actor disables API key`,
    ObtainAccessToken(),
    createApiKey(testInfo.title),
    LoginAsStaffer(),
    goToTheAdminLandingPageForOrganization(
      notes().get('first_organization_id')
    ),
    Log.the(notes().get('apiKeyId')),
    OrganizationApiKeysTable.container().scrollIntoView(),
    Wait.until(OrganizationApiKeysTable.keyId(), isPresent()),
    notes().set('keyEnabled', Text.of(OrganizationApiKeysTable.keyId())),
    Log.the(notes().get('keyEnabled')),
    Click.on(OrganizationApiKeysTable.rowContextMenuTrigger()),
    Wait.until(
      OrganizationApiKeysTable.rowContextMenuActivateKeyButton(),
      isVisible()
    ),
    Click.on(OrganizationApiKeysTable.rowContextMenuActivateKeyButton()),
    Click.on(GlobalDialog.acceptButton()),
    Ensure.that(Text.of(OrganizationApiKeysTable.status()), equals('Inactive'))
  );
