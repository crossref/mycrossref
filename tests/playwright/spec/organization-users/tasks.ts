import { Snackbar } from './../alerts/Notifications';
import { Ensure } from '@serenity-js/assertions';
import { Click, Enter, isVisible } from '@serenity-js/web';
import { OrganizationUsersCard } from './questions';
import { Answerable, Task } from '@serenity-js/core';

export const createUserAndAddToOrg = (name: Answerable<string>) =>
  Task.where(
    `#actor creates new organisationn user called ${name}`,
    Click.on(OrganizationUsersCard.addButton()),
    Enter.theValue(name).into(OrganizationUsersCard.addUserNameField()),
    Click.on(OrganizationUsersCard.saveNewUserButton())
  );
