import { grantFormData as grantFormData1 } from './example-data/grantForm1';
import grantFormData2 from './example-data/grantForm2.json';
import grantFormData3 from './example-data/grant-form_no-investigators.json';
import { generateXMLString } from '@/services/grantsXMLGenerator';
import { XMLParser, XMLValidator } from 'fast-xml-parser';
import * as fs from 'fs';
import * as path from 'path';
import schema from '@/schema/ContentRegistrationGrantsSchema.json';
import { parseXML } from '@/utils/deposit';
import { describe, it, expect } from 'vitest';

// We need to manually import the cjs version of JSON Schema Faker as the auto-imported
// variant seems to be the Browser variant which fails here.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { JSONSchemaFaker } = require('json-schema-faker');

const fxpCheckValid = (xml: string) => {
  const parser = new XMLParser();
  parser.parse(xml);
  const isValid = XMLValidator.validate(xml);
  return isValid;
};

const DOMCheckValid = (xml: string): true | Document => {
  const parser = new DOMParser();
  const dom = parser.parseFromString(xml, 'application/xml');
  const valid = dom.getElementsByTagName('parsererror').length === 0;
  return valid || dom;
};

/**
 *
 * @param count
 * @param desc
 * @param options
 * @returns Array
 * Prune the ORCID field from the generated data as the pattern in the schema is not quite right
 * TODO: fix the ORCID schema field validation pattern, then return the ORCID field to the generated properties
 */
const generateFakeSchemaData = (
  count = 1,
  desc = 'Dynamically generated fake Grants data with all optional fields',
  options = {
    fillProperties: false,
    alwaysFakeOptionals: true,
    pruneProperties: ['ORCID'],
  }
) => {
  const fake: [string, any][] = [];
  JSONSchemaFaker.option(options);
  for (let i = 0; i < count; i++) {
    fake.push([`${desc} #${i}`, JSONSchemaFaker.generate(schema)]);
  }
  return fake;
};

const outputFolder = path.resolve(__dirname, 'output');
fs.rmSync(outputFolder, { force: true, recursive: true });

if (!fs.existsSync(outputFolder)) {
  fs.mkdirSync(outputFolder);
}

/**
 * Fake schema data is generated with alwaysFakeOptionals: true as the JSON schema does not yet
 * require all fields equivalently with the XML XSD
 * TODO: map XSD field requirements onto the JSON schema
 */
describe('XML Generation', () => {
  let outputIndex = 0;
  test.each([
    [
      'Unexpected close tag',
      `<?xml version="1.0"?>
<root att="value">
  <foo>
    <bar>Another value</bar>
  </bar>
  <baz/>
</root>`,
      '<parsererror xmlns="http://www.mozilla.org/newlayout/xml/parsererror.xml">5:8: unexpected close tag.</parsererror>',
    ],
  ])(
    'Invalid XML (%s) does not parse',
    (title, invalidXML, expectedErrorMsg) => {
      const domParsed = DOMCheckValid(invalidXML) as Document;
      expect(domParsed).not.toEqual(true);
      const errorMsg = new XMLSerializer().serializeToString(domParsed);
      expect(errorMsg).toEqual(expectedErrorMsg);
      const fxpValid = fxpCheckValid(invalidXML);
      expect(fxpValid).not.toEqual(true);
    }
  );
  type FormDataExample = [string, any];

  const exampleFormData: FormDataExample[] = [
    ['Grant Form example 1', grantFormData1],
    ['Grant Form example 2', grantFormData2],
    ['Grant Form example 3', grantFormData3],
    ...generateFakeSchemaData(10),
  ];
  it.each(exampleFormData)(
    'Check generated XML for %s is well-formed',
    (title, formData) => {
      outputIndex++;
      const generatedXML = generateXMLString(formData, true);
      const fileName = `grant_${outputIndex}.xml`;
      const generatedXMLPath = path.resolve(outputFolder, `${fileName}`);
      fs.writeFileSync(generatedXMLPath, generatedXML);
      const domParsed = DOMCheckValid(generatedXML);
      if (!domParsed) {
        const errorMsg = new XMLSerializer().serializeToString(domParsed);
        console.error('Error parsing XML: ', errorMsg);
      }
      const fxpValid = fxpCheckValid(generatedXML);
      expect(fxpValid).toEqual(true);
    }
  );
  it.each(exampleFormData)(
    'Check award and currency funding attributes are not present',
    (title, formData) => {
      outputIndex++;
      const generatedXML = generateXMLString(formData, true);
      const doc = parseXML(generatedXML);
      const amount = doc.evaluate(
        'boolean(//grant[1]/project[1]/funding[1]/@amount)',
        doc,
        null,
        XPathResult.BOOLEAN_TYPE
      );
      expect(amount.booleanValue).toEqual(false);
      const currency = doc.evaluate(
        'boolean(//grant[1]/project[1]/funding[1]/@currency)',
        doc,
        null,
        XPathResult.BOOLEAN_TYPE
      );
      expect(currency.booleanValue).toEqual(false);
    }
  );
});
