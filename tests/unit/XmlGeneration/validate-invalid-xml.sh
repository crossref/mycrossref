#!/bin/sh

find . -type f -path "./tests/unit/XmlGeneration/*" -name "invalid_*.xml" -print0 | xargs -n1 -0 xmllint --schema ./tests/unit/XmlGeneration/schemas/grant_id0.1.1.xsd --noout
test $? -eq 0 || echo "PASS: Invalid XML did not pass validation."
