import { mount } from '@vue/test-utils';
import { vi } from 'vitest';
import Breadcrumbs from '@/components/Breadcrumbs.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { useRoute, RouteLocationMatched } from 'vue-router';

// Mock the useRoute function from vue-router
vi.mock('vue-router');

describe('Breadcrumbs.vue', () => {
  // Helper function to mount the Breadcrumbs component with global plugins
  const mountFunction = () => {
    return mount(Breadcrumbs, {
      global: {
        plugins: [vuetifyInstance],
      },
    });
  };

  it('Renders correctly when there are breadcrumbs to display', async () => {
    // Mock the useRoute function to return breadcrumbs data
    vi.mocked(useRoute).mockReturnValue({
      matched: [
        { path: '/', name: 'Home', meta: { title: 'Home' } },
        { path: '/about', name: 'About', meta: { title: 'About' } },
      ],
    } as any); // Casting as any to bypass TypeScript type checking for mock data

    const wrapper = mountFunction();

    // Find all breadcrumb items and assert their count and content
    const breadcrumbs = wrapper.findAll('.v-breadcrumbs-item');
    expect(breadcrumbs).toHaveLength(2);
    expect(breadcrumbs[0].text()).toContain('Home');
    expect(breadcrumbs[1].text()).toContain('About');
  });

  it('Does not render hidden breadcrumbs', () => {
    // Mock the useRoute function to return breadcrumbs data with one hidden item
    vi.mocked(useRoute).mockReturnValue({
      matched: [
        { path: '/', name: 'Home', meta: { title: 'Home', hideBreadcrumb: true } },
        { path: '/about', name: 'About', meta: { title: 'About' } },
      ],
    } as any);

    const wrapper = mountFunction();

    // Find all breadcrumb items and assert their count and content
    const breadcrumbs = wrapper.findAll('.v-breadcrumbs-item');
    expect(breadcrumbs).toHaveLength(1);
    expect(breadcrumbs[0].text()).toContain('About');
  });

  it('Does not render when there are no breadcrumbs to display', () => {
    // Mock the useRoute function to return no breadcrumbs
    vi.mocked(useRoute).mockReturnValue({
      matched: [],
    } as any);

    const wrapper = mountFunction();

    // Find all breadcrumb items and assert there are none
    const breadcrumbs = wrapper.findAll('.v-breadcrumbs-item');
    expect(breadcrumbs).toHaveLength(0);
  });
});
