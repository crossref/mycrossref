import { mount } from '@vue/test-utils';
import OrganizationSearch from '@/components/Organizations/OrganizationSearch.vue';
import { VAutocomplete } from 'vuetify/components';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect, test } from 'vitest';
import { nextTick } from 'vue';

// Mock getOrganizationsVisibleToUser function - called by the autocomplete to populate the dropdown
vi.mock('@/core/organizations', () => {
  // vi.mock(..) is hoisted to the top of the file, so cannot reference top-level variables
  // See https://vitest.dev/api/vi.html#vi-mock
  const mockOrganisationsVisibleToUser = [
    { name: 'Organization 1', memberId: 1 },
    { name: 'Organization 2', memberId: 2 },
  ];
  return {
    getOrganizationsVisibleToUser: vi
      .fn()
      .mockResolvedValue(mockOrganisationsVisibleToUser),
  };
});

const mountFunction = (options = {}) => {
  return mount(OrganizationSearch, {
    global: {
      plugins: [vuetifyInstance],
    },
    ...options,
  });
};

describe('Organisation Search', () => {
  test('Calls update function and emits events on user input', async () => {
    // The wrapper is the encapsulation of the component under test (OrganizationSearch here)
    // https://v1.test-utils.vuejs.org/api/wrapper/
    const wrapper = mountFunction();

    // Vue Test Utils has trouble inferring the typing of a component using <script setup>
    // This bypassing typechecking in the component instance under test
    const vm = wrapper.vm as any

    // find the Vuetify Autocomplete component
    const autocomplete = wrapper.findComponent(VAutocomplete);

    // Setup a spy on the updateOrgs method of the OrganizationSearch component
    const updateOrgsSpy = vi.spyOn(vm, 'updateOrgs');

    const userSearchInput = 'Organization';

    // Find the input element inside VAutocomplete
    const input = wrapper.find(
      '[data-test-id="search-for-organizations-input"] input'
    );

    // Simulate typing into the input field
    await input.setValue(userSearchInput);

    // Assert that the Autocomplete's search property updated event was emitted once,
    // with the expected value of the user search input
    expect(autocomplete.emitted('update:search')).toHaveLength(1);
    expect(autocomplete.emitted('update:search')?.[0]).toEqual([
      userSearchInput,
    ]);

    // Assert that the Autocomplete's model value is set to the user search input
    expect(autocomplete.vm.modelValue).toEqual(userSearchInput);

    // Assert that the update function is called with the user search input
    expect(updateOrgsSpy).toHaveBeenCalledTimes(1);
    expect(updateOrgsSpy).toHaveBeenCalledWith(userSearchInput);

    // Wait for the next 'tick' of Vue's reactivity system. See https://vuejs.org/api/general.html#nexttick
    // This allows asynchronous actions to complete (like updating organisations after user input..)
    await nextTick();

    // Expected values are declared in-line becuase we can't share with vi.mock due to hoisting
    // These could be moved into an external import if they get used in many places
    expect(vm.searchResults).toEqual([
      { name: 'Organization 1', memberId: 1 },
      { name: 'Organization 2', memberId: 2 },
    ]);
  });
  test('Calls update function again after subsequent user input', async () => {
    // The wrapper is the encapsulation of the component under test (OrganizationSearch here)
    // https://v1.test-utils.vuejs.org/api/wrapper/
    const wrapper = mountFunction();
    const vm = wrapper.vm as any

    // find the Vuetify Autocomplete component
    const autocomplete = wrapper.findComponent(VAutocomplete);

    // Setup a spy on the updateOrgs method of the OrganizationSearch component
    const updateOrgsSpy = vi.spyOn(vm, 'updateOrgs');

    const userSearchInput = 'Organization';

    // Find the input element inside VAutocomplete
    const input = wrapper.find(
      '[data-test-id="search-for-organizations-input"] input'
    );

    // Simulate typing into the input field
    await input.setValue(userSearchInput);

    // Assert that the Autocomplete's model value is set to the user search input
    expect(autocomplete.vm.modelValue).toEqual(userSearchInput);

    // Assert that the update function is called with the user search input
    expect(updateOrgsSpy).toHaveBeenCalledTimes(1);
    expect(updateOrgsSpy).toHaveBeenCalledWith(userSearchInput);

    // Wait for the next 'tick' of Vue's reactivity system. See https://vuejs.org/api/general.html#nexttick
    // This allows asynchronous actions to complete (like updating organisations after user input..)
    await nextTick();

    // Expected values are declared in-line becuase we can't share with vi.mock due to hoisting
    // These could be moved into an external import if they get used in many places
    expect(vm.searchResults).toEqual([
      { name: 'Organization 1', memberId: 1 },
      { name: 'Organization 2', memberId: 2 },
    ]);

    const nextUserSearchInput = 'Second Organization'
    // Simulate typing into the input field
    await input.setValue(nextUserSearchInput);

    // Assert that the Autocomplete's model value is set to the user search input
    expect(autocomplete.vm.modelValue).toEqual(nextUserSearchInput);

    // Assert that the update function is called most recently with the second user search input
    expect(updateOrgsSpy).toHaveBeenCalledTimes(2);
    expect(updateOrgsSpy).toHaveBeenLastCalledWith(nextUserSearchInput);
  });
});
