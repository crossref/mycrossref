import { mount } from '@vue/test-utils';
import ApiKeysTable from '@/components/Organizations/ApiKeysTable.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect, test } from 'vitest';

/**
 * Mounts the `ApiKeysTable` component with optional props and global configurations.
 *
 * @param {Record<string, any>} options - Optional parameters to customize the mounting of the component.
 * @returns {VueWrapper<InstanceType<typeof ApiKeysTable>>} - A wrapper around the Vue component.
 */
const mountFunction = (options = {}) => {
  return mount(ApiKeysTable, {
    global: {
      mocks: {
        // Mocks the translation function used by the component.
        $t: (msg) => msg,
      },
      // Provides the Vuetify plugin required by the component.
      plugins: [vuetifyInstance],
    },
    ...options,
  });
};

describe('ApiKeysTable.vue', () => {
  /**
   * Test case for verifying if the API key descriptions are properly rendered
   * in the `ApiKeysTable` component.
   */
  test('CR-1702: Renders API key descriptions', async () => {
    // Mounts the component with props necessary for this specific test.
    const wrapper = mountFunction({
      props: {
        memberId: 1, // Example member ID to simulate component behavior.
        apiKeys: [
          // Mock data simulating API keys passed to the component.
          {
            keyId: '123',
            enabled: true,
            description: 'Test API Key',
          },
        ],
      },
    });

    // Asserts that the text "Test API Key" is part of the component's rendered output.
    expect(wrapper.text()).toContain('Test API Key');
  });
});
