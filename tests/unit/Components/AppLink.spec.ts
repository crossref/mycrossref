import { mount, shallowMount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import AppLink from '@/components/AppLink.vue';
import vuetifyInstance from '@/plugins/vuetify';

describe('AppLink.vue', () => {
  it('renders an external link correctly', async () => {
    const wrapper = mount(AppLink, {
      props: { to: 'https://external.com' },
      global: {
        plugins: [vuetifyInstance],
      },
    });

    const externalLink = wrapper.find('a');
    expect(externalLink.exists()).toBe(true);
    expect(externalLink.attributes('href')).toBe('https://external.com');
    expect(externalLink.attributes('target')).toBe('_blank');
    expect(externalLink.attributes('rel')).toBe('noopener noreferrer');
  });

  it('renders an internal link correctly', async () => {
    const TEST_STRING = '/internal-page';
    const wrapper = shallowMount(AppLink, {
      props: { to: TEST_STRING },
      global: {
        plugins: [vuetifyInstance],
      },
    });

    const internalLink = wrapper.find('a');
    expect(internalLink.exists()).toBe(false);
    const routerLink = wrapper.findComponent({ name: 'RouterLink' });
    expect(routerLink.exists()).toBe(true);
    expect(routerLink.props('to')).toBe(TEST_STRING);
  });
});
