import { mount } from '@vue/test-utils';
import NavDrawer from '@/components/TheNavDrawer.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { describe, it, expect } from 'vitest';
import { RouterLink } from 'vue-router';
import router from '@/router';
import VuetifyWrapper from '../Support/VuetifyWrapper.vue';
import { h } from 'vue';

const mountFunction = (options = {}) => {
  return mount(VuetifyWrapper, {
    global: {
      plugins: [vuetifyInstance, router],
    },
    slots: {
      default: h(NavDrawer),
    },
    ...options,
  });
};

describe('NavMenu.vue', () => {
  it('renders menu items based on the routes', async () => {
    const wrapper = mountFunction();
    const menuItems = wrapper.findAll('.v-list-item--link');
    expect(menuItems.length).toBeGreaterThan(0);
    const links = wrapper.findAllComponents(RouterLink);
    expect(links.length).toBeGreaterThan(0);
  });
});
