import { interpret } from 'xstate';
import { exampleMachine as machine } from '@/statemachines/example.machine';
import { test } from 'vitest';

// This helper function allows us to wait for a certain state.
const onceTransition = (service, stateValue) =>
  new Promise((resolve) => {
    const stateListener = (state) => {
      if (state.matches(stateValue)) {
        service.off(stateListener);
        resolve(null);
      }
    };

    service.onTransition(stateListener);
  });

test('example machine', async () => {
  const machineWithConfig = machine.withConfig({}, { count: 0 });
  const exampleService = interpret(machineWithConfig);
  exampleService.start();

  exampleService.send('TOGGLE');

  await onceTransition(exampleService, 'active');

  exampleService.stop();
});
