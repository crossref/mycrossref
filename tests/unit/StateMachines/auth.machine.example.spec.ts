import { interpret } from 'xstate';
import { authenticationMachine } from '@/statemachines/auth.machine.example';
import { server } from '@/mocks/server';
import { test } from 'vitest';

// This helper function allows us to wait for a certain state.
const waitForState = (service, stateValue) =>
  new Promise((resolve) => {
    const stateListener = (state) => {
      if (state.matches(stateValue)) {
        service.off(stateListener);
        resolve(null);
      }
    };

    service.onTransition(stateListener);
  });

beforeAll(() => {
  server.listen();
});

afterEach(() => {
  server.resetHandlers();
});

afterAll(() => {
  server.close();
});

test('authentication machine', async () => {
  const authService = interpret(authenticationMachine);

  authService.start();

  authService.send({
    type: 'ATTEMPT_LOG_IN',
    userDetails: { username: 'someone@crossref.org' },
  });

  await waitForState(authService, 'loggedIn');

  authService.stop();
});
