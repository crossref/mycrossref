import { describe, test } from 'vitest';
import {Identifier} from "@/itemTree/Identifier";

const doi = '10.55555/1234567890';
describe('Identifiers', () => {
  test('toJSON returns the uri property as the serialized output', () => {
    const expected = doi;
    const identifier = new Identifier(doi);
    const result = identifier.toJSON();
    expect(result).toEqual(expected);
  });
  test('toString returns the uri property as a string representation', () => {
    const expected = doi;
    const identifier = new Identifier(doi);
    const result = identifier.toString();
    expect(result).toEqual(expected);
  });
});
