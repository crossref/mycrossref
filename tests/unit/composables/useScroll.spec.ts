import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import { ref } from 'vue';
import { useScrollToFirstError } from '../../../src/composable/useScroll';
import { useElementBounding } from '@vueuse/core';

// Complete mock of useElementBounding to meet type expectations
const mockUseElementBounding = (heightValue: number) => ({
  height: ref(heightValue),
  bottom: ref(0),
  left: ref(0),
  right: ref(0),
  top: ref(0),
  width: ref(0),
  x: ref(0),
  y: ref(0),
  update: vi.fn()
});

vi.mock('@vueuse/core', () => ({
  useElementBounding: vi.fn()
}));

describe('useScrollToFirstError', () => {
  beforeEach(() => {
    vi.resetAllMocks();
    window.scrollTo = vi.fn((...args: any[]) => {});
    document.querySelector = vi.fn().mockImplementation(selector => {
      if (selector === '.custom-json-forms [errors]:not([errors=""])') {
        return { getBoundingClientRect: () => ({ top: 200 }) };
      }
      return null;
    });
  });

  it('should call window.scrollTo with correct position when header is present', () => {
    const headerRef = ref({});
    vi.mocked(useElementBounding).mockReturnValue(mockUseElementBounding(100));

    const { scrollToFirstError } = useScrollToFirstError(headerRef);
    scrollToFirstError();

    expect(useElementBounding).toHaveBeenCalledWith(headerRef);
    expect(window.scrollTo).toHaveBeenCalledWith({
      top: 200 + window.pageYOffset - 100,
      behavior: 'smooth'
    });
  });

  it('should call window.scrollTo with correct position when header is not present', () => {
    const headerRef = ref(null);
    vi.mocked(useElementBounding).mockReturnValue(mockUseElementBounding(0));

    const { scrollToFirstError } = useScrollToFirstError(headerRef);
    scrollToFirstError();

    expect(useElementBounding).toHaveBeenCalledWith(headerRef);
    expect(window.scrollTo).toHaveBeenCalledWith({
      top: 200 + window.pageYOffset,
      behavior: 'smooth'
    });
  });
});
