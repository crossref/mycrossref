import { describe, test } from 'vitest';
import form_data from '../../../resources/json.formdata/journal_article_1.json';
import item_tree from '../../../resources/json.itemtree/journal_article_from_form_data_1.json';
import {
  JournalArticleMapper,
  mapJournalArticleToItemTree,
} from '@/itemTree/mapping/JournalArticleMapper';
import { compareObjects } from './Util.spec';

describe('JournalArticleMapper', () => {
  test('correctly maps journal article form data to its corresponding item tree structure', () => {
    const result = mapJournalArticleToItemTree(form_data);
    compareObjects(result, item_tree);
  });
});
