import { describe, it, expect } from 'vitest';
import {
  hasPublicationDatesChanged,
  transformPublicationDates,
} from '../../../../src/forms/transformations/updatePublicationDatesSchema';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';

describe('hasPublicationDatesChanged', () => {
  it('should return true when publication dates have changed', () => {
    const prevData = {
      article: { publicationDates: { publishedOnline: '2023-01-01' } },
    };
    const newData = {
      article: { publicationDates: { publishedOnline: '2023-02-01' } },
    };
    const schema = {};
    const context = {};

    const result = hasPublicationDatesChanged(
      schema,
      newData,
      prevData,
      context
    );
    expect(result).toBe(true);
  });

  it('should return false when publication dates have not changed', () => {
    const commonData = {
      article: { publicationDates: { publishedOnline: '2023-01-01' } },
    };
    const schema = {};
    const context = {};

    const result = hasPublicationDatesChanged(
      schema,
      commonData,
      commonData,
      context
    );
    expect(result).toBe(false);
  });

  it('should return true when updated publication dates contain no primitive values', () => {
    const newData = {
      article: { publicationDates: {} },
    };
    const prevData = {
      article: { publicationDates: { publishedOnline: '2023-01-01' } },
    };
    const schema = {};
    const context = {};

    const result = hasPublicationDatesChanged(
      schema,
      newData,
      prevData,
      context
    );
    expect(result).toBe(true);
  });
});

describe('transformPublicationDates', () => {
  it('updates the schema to require publication dates when not present', () => {
    const data = {
      article: { publicationDates: {} }, // No publication dates present
    };
    const schema = {
      properties: {
        article: {
          properties: {
            publicationDates: {
              required: [],
            },
          },
        },
      },
    };

    const updatedSchema = transformPublicationDates(cloneDeep(schema), data);
    expect(
      get(
        updatedSchema,
        'properties.article.properties.publicationDates.required'
      )
    ).toEqual(['publishedOnline', 'publishedInPrint']);
  });

  it('does not modify the schema if publication dates are present', () => {
    const data = {
      article: { publicationDates: { publishedOnline: '2023-03-01' } },
    };
    const expectedSchema = {
      properties: {
        article: {
          properties: {
            publicationDates: {
              required: [],
            },
          },
        },
      },
    };

    const updatedSchema = transformPublicationDates(
      cloneDeep(expectedSchema),
      data
    );
    expect(updatedSchema).toEqual(expectedSchema);
  });
});
