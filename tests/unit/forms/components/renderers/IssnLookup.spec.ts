import { describe, it, expect, vi } from 'vitest';
import { mount } from '@vue/test-utils';
import IssnLookup from '@/forms/components/renderers/IssnLookup.vue';
import vuetifyInstance from '@/plugins/vuetify';
import { updateJournalDetails } from '@/forms/transformations/updateJournalDetails';

// Mock the CrossrefClient
vi.mock('@/plugins/crossref-api-client', () => ({
  CrossrefClient: vi.fn().mockImplementation(() => ({
    initializeAbortController: vi.fn(),
    journal: vi.fn((issn) => {
      if (issn === '1234-5678') {
        // Simulate a successful lookup for a specific ISSN
        return Promise.resolve({
          ok: true,
          content: {
            message: {
              title: 'Test Journal',
            },
          },
        });
      } else {
        // Simulate a failed lookup for any other ISSN
        return Promise.resolve({
          ok: false,
          content: {
            message: {},
          },
        });
      }
    }),
  })),
}));
const mockDispatch = vi.fn();

vi.mock('@/forms/transformations/updateJournalDetails', () => ({
  updateJournalDetails: vi.fn(() => {
    return {
      journal: {
        title: 'Test Journal',
      },
    };
  }),
}));

function setupIssnLookup(propsData = { issn: '1234-5678' }) {
  return mount(IssnLookup, {
    props: propsData,
    global: {
      mocks: {
        // Mocks the translation function used by the component.
        t: (msg) => msg,
      },
      plugins: [vuetifyInstance],
      provide: {
        dispatch: mockDispatch,
        jsonforms: () => ({
          core: {
            data: {},
            schema: {},
          },
        }),
      },
    },
  });
}
describe('IssnLookup.vue', () => {
  it('renders successfully and performs initial lookup', async () => {
    const wrapper = setupIssnLookup({ issn: '1234-5678' });

    // Flush promises and ensure the DOM has been updated
    await new Promise(setImmediate);

    // Assertions
    expect(wrapper.text()).toContain('Test Journal');
  });
  it('autofills journal details correctly', async () => {
    const wrapper = setupIssnLookup({ issn: '1234-5678' });

    // Wait for the component to initialize and perform the lookup
    await new Promise(setImmediate);

    const autofillButton = wrapper.find('[data-test="autofill-button"]');
    expect(autofillButton.exists()).toBe(true);

    // Simulate clicking the "Autofill" button
    await autofillButton.trigger('click');
    expect(updateJournalDetails).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();

    // Assert that `dispatch` was called as expected
    expect(mockDispatch).toHaveBeenCalledWith({
      type: 'jsonforms/UPDATE_CORE',
      data: {
        journal: {
          title: 'Test Journal',
        },
      },
      options: undefined,
      schema: undefined,
      uischema: undefined,
    });
  });

  it('correctly handles dismiss action', async () => {
    const wrapper = setupIssnLookup({ issn: '1234-5678' });

    await new Promise(setImmediate); // Wait for async operations

    const noThanksButton = wrapper.find('[data-test="dismiss-button"]');
    await noThanksButton.trigger('click');

    // Assert the `show` state is false
    expect(wrapper.vm.show).toBe(false);

    // Assert there is no card visibile in the UI
    const vCardWrapper = wrapper.find('.v-card');
    expect(vCardWrapper.exists()).toBe(false);
  });

  it('emits lookup-start and lookup-complete on successful lookup', async () => {
    const wrapper = setupIssnLookup({ issn: '1234-5678' });

    await new Promise(setImmediate); // Wait for any async ops to settle

    // Check that 'lookup-start' event is emitted
    expect(wrapper.emitted()['lookup-start']).toBeTruthy();

    // Check that 'lookup-complete' event is emitted
    expect(wrapper.emitted()['lookup-complete']).toBeTruthy();
  });

  it('emits lookup-start and lookup-complete on failed lookup', async () => {
    const wrapper = setupIssnLookup({ issn: '0000-0000' });

    await new Promise(setImmediate);

    // Check that 'lookup-start' event is emitted
    expect(wrapper.emitted()['lookup-start']).toBeTruthy();

    // Check that 'lookup-complete' event is emitted
    expect(wrapper.emitted()['lookup-complete']).toBeTruthy();
  });


  it('handles no match found correctly', async () => {
    const wrapper = setupIssnLookup({ issn: '0000-0000' });

    await new Promise(setImmediate); // Wait for async

    expect(wrapper.text()).toContain('issn-lookup-no-match-msg');
    expect(wrapper.find('[data-test="match-found-element"]').exists()).toBe(false);
    expect(wrapper.vm.lookupFailure).toBe(true);
  });
});
