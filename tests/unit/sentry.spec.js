// LoginForm.spec.js

// Libraries
import * as Sentry from '@sentry/vue';
import { createApp } from 'vue';

// Components

// Utilities
import * as sentryMock from '@/mocks/sentry-testkit';
const app = createApp();
const config = { ...sentryMock.sentryConfig, app };

Sentry.init(config);

describe('Sentry.io SDK integration', () => {
  it('Should capture exceptions in the reports stack', () => {
    Sentry.captureException(new Error('This is a test error.'));
    expect(sentryMock.testkit.reports()).toHaveLength(1);
    expect(sentryMock.testkit.reports()[0]).toHaveProperty(
      'error.message',
      'This is a test error.'
    );
  });
});
