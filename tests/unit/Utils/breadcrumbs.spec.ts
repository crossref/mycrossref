import { RouteRecordNormalized } from 'vue-router';
import { resolveBreadcrumbs } from '../../../src/utils/routes';
// breadcrumbLogic.spec.ts
import {
  RouteRecordRaw,
  RouteLocationNormalized,
  RouteLocationNormalizedLoaded,
} from 'vue-router';
import { RouteTitleResolver } from '@/router';

describe('Breadcrumb logic', () => {
  const mockRoute: Partial<RouteLocationNormalizedLoaded> = {
    path: '/home',
    fullPath: '/home',
    hash: '',
    query: {},
    matched: [],
    meta: {},
    name: 'home',
    params: {},
  };

  it('resolves to empty array if no routes are matched', () => {
    expect(
      resolveBreadcrumbs([], mockRoute as RouteLocationNormalizedLoaded)
    ).toEqual([]);
  });

  it('resolves to correct breadcrumb array for matched routes', () => {
    const matchedRoutes: Partial<RouteRecordRaw>[] = [
      {
        path: '/home',
        meta: { title: 'Home' },
      },
      {
        path: '/home/profile',
        meta: { title: 'Profile' },
      },
    ];

    expect(
      resolveBreadcrumbs(
        matchedRoutes as RouteRecordNormalized[],
        mockRoute as RouteLocationNormalizedLoaded
      )
    ).toEqual([
      { title: 'Home', hide: false, disabled: false, href: '/home' },
      {
        title: 'Profile',
        hide: false,
        disabled: false,
        href: '/home/profile',
      },
    ]);
  });

  it('ignores routes with hideBreadcrumb set to true', () => {
    const matchedRoutes: Partial<RouteRecordRaw>[] = [
      {
        path: '/home',
        meta: { title: 'Home' },
      },
      {
        path: '/home/hidden',
        meta: { title: 'Hidden', hideBreadcrumb: true },
      },
    ];

    expect(
      resolveBreadcrumbs(
        matchedRoutes as RouteRecordNormalized[],
        mockRoute as RouteLocationNormalizedLoaded
      )
    ).toEqual([
      { title: 'Home', hide: false, disabled: false, href: '/home' },
      { title: 'Hidden', hide: true, disabled: false, href: '/home/hidden' },
    ]);
  });

  it('uses function-based breadcrumb resolver correctly', () => {
    const matchedRoutes: Partial<RouteRecordRaw>[] = [
      {
        path: '/home',
        meta: {
          title: ((route: RouteLocationNormalized) =>
            `Func-${route.path}`) as RouteTitleResolver,
        },
      },
    ];

    expect(
      resolveBreadcrumbs(
        matchedRoutes as RouteRecordNormalized[],
        mockRoute as RouteLocationNormalizedLoaded
      )[0].title
    ).toBe('Func-/home');
  });
});
