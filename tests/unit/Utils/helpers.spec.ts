import { describe, it, expect } from 'vitest';
import {
  transformOrcidInput,
  formatDoiInput,
  autoFormatISSN,
} from '@/utils/helpers';

describe('transformOrcidInput', () => {
  it('should prepend https://orcid.org/ to a valid ORCID ID', () => {
    const input = '0000-0002-1825-0097';
    const expected = 'https://orcid.org/0000-0002-1825-0097';
    expect(transformOrcidInput(input)).toBe(expected);
  });

  it('should return the input unchanged if it is not a valid ORCID ID', () => {
    const input = 'invalid-orcid';
    expect(transformOrcidInput(input)).toBe(input.trim());
  });

  it('should handle input with leading and trailing spaces', () => {
    const input = '  0000-0002-1825-0097  ';
    const expected = 'https://orcid.org/0000-0002-1825-0097';
    expect(transformOrcidInput(input)).toBe(expected);
  });
});

describe('formatDoiInput', () => {
  it('should strip the domain and return just the DOI', () => {
    const input = 'https://doi.org/10.1000/xyz123';
    const expected = '10.1000/xyz123';
    expect(formatDoiInput(input)).toBe(expected);
  });

  it('should return the DOI unchanged if no domain is present', () => {
    const input = '10.1000/xyz123';
    expect(formatDoiInput(input)).toBe(input.trim());
  });

  it('should handle input with leading and trailing spaces', () => {
    const input = '  https://doi.org/10.1000/xyz123  ';
    const expected = '10.1000/xyz123';
    expect(formatDoiInput(input)).toBe(expected);
  });

  it('should return the input unchanged if it is not a valid DOI URL', () => {
    const input = 'invalid-doi';
    expect(formatDoiInput(input)).toBe(input.trim());
  });
});

describe('autoFormatISSN', () => {
  it('should format a valid 8-digit ISSN with a hyphen', () => {
    const input = '12345678';
    const expected = '1234-5678';
    expect(autoFormatISSN(input)).toBe(expected);
  });

  it('should return a valid 8-digit ISSN with a hyphen', () => {
    const input = '1234-5678';
    const expected = '1234-5678';
    expect(autoFormatISSN(input)).toBe(expected);
  });

  it('should return the input unchanged if it is not a valid ISSN', () => {
    const input = 'invalid-issn';
    expect(autoFormatISSN(input)).toBe(input.trim());
  });

  it('should handle input with leading and trailing spaces', () => {
    const input = '  12345678  ';
    const expected = '1234-5678';
    expect(autoFormatISSN(input)).toBe(expected);
  });
});
