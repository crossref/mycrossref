import { generateUuid } from '@/utils/ids';
import { MyNotes } from './../../playwright/spec/staff-managing-organizations.spec';
import { env } from '@/env';
import { useAuthToken } from './../../support/useAuthToken';
import { Ensure, isGreaterThan, equals } from '@serenity-js/assertions';
import {
  Interaction,
  List,
  actorInTheSpotlight,
  notes,
  Task,
  q,
} from '@serenity-js/core';
import { GetRequest, LastResponse, PostRequest, Send } from '@serenity-js/rest';
import { resetUsersAndGroups } from './../../support/ResetKeycloakUsersAndGroups';
import { KEYMANAGER_ORGANIZATION_QUERY_ENDPOINT } from '@/api/resourceManagementApi';
import useConfig from '../../support/useConfig';

const config = useConfig('VUE_APP_TEST_');

export const ResetKeycloakUsersAndGroups = () =>
  Interaction.where(`#actor resets the Keycloak Users and Groups`, async () => {
    const keycloakUsers = await resetUsersAndGroups();
    await actorInTheSpotlight().attemptsTo(
      Ensure.that(
        List.of(keycloakUsers.users || [])
          .count()
          .describedAs('Number of Keycloak users'),
        isGreaterThan(1)
      ),
      Ensure.that(
        List.of(keycloakUsers.groups || [])
          .count()
          .describedAs('Number of Keycloak top-level groups'),
        equals(2)
      )
    );
  });

export const ObtainAccessToken = () =>
  Interaction.where(`#actor obtains an access token`, async () => {
    const { getToken } = await useAuthToken({
      username: config.value?.AUTH?.CL?.UI?.USERNAME,
      password: config.value?.AUTH?.CL?.UI?.PASSWORD,
      realm: config.value?.AUTH?.CL?.UI?.REALM,
      clientId: config.value?.AUTH?.CL?.UI?.CLIENTID,
    });
    actorInTheSpotlight().attemptsTo(
      notes<MyNotes>().set('auth_token', getToken())
    );
  });

export const createGetApiKey = (description: string) =>
  Task.where(
    `#actor creates an API key`,
    Send.a(
      GetRequest.to(`${env().keymakerAPIBaseURL}/organization/1/key22`).using({
        headers: {
          Authorization: `Bearer ${notes().get('auth_token')}`,
        },
      })
    )
  );

export const obtainFirstOrganizationId = () =>
  Task.where(
    `#actor obtains the id for the first organization listed`,
    Send.a(
      GetRequest.to(
        `${KEYMANAGER_ORGANIZATION_QUERY_ENDPOINT}/organization`
      ).using({
        headers: {
          Authorization: `Bearer ${notes().get('auth_token')}`,
        },
      })
    ),
    notes<MyNotes>().set(
      'first_organization_id',
      LastResponse.body()[0].memberId
    )
  );

export const createApiKey = (description: string) =>
  Task.where(
    `#actor creates an API key`,
    obtainFirstOrganizationId(),
    Send.a(
      PostRequest.to(
        q`${KEYMANAGER_ORGANIZATION_QUERY_ENDPOINT}/organization/${notes().get(
          'first_organization_id'
        )}/key`
      )
        .using({
          headers: {
            Authorization: q`Bearer ${notes().get('auth_token')}`,
          },
        })
        .with({
          description: `${description} ${generateUuid()}`,
          issuedBy: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          keyId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          name: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
          enabled: false,
        })
    ),
    Ensure.that(LastResponse.status(), equals(201)),
    notes().set('apiKeyDescription', LastResponse.body().keyObject.description),
    notes().set('apiKeyId', LastResponse.body().keyObject.keyId)
  );
