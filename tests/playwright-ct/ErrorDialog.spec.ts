import { test, expect } from '@playwright/experimental-ct-vue';
import ErrorDialogTest from './ErrorDialogTestComponent.vue';
import { Dialog } from '@/stores/dialogs';
import { Locator, Page } from 'playwright/test';

class ErrorDialogPage {
  readonly page: Page;
  readonly vDialog: Locator;
  readonly closeBtn: Locator;
  readonly goToDocsBtn: Locator;

  constructor(page: Page) {
    this.page = page;
    this.vDialog = page.locator('[data-test-id="error-dialog"]');
    this.closeBtn = page.locator('button', { hasText: 'Close' });
    this.goToDocsBtn = page.getByRole('link', { name: 'Go to documentation' });
  }
}

test.use({});

test('Error Dialog is called with message', async ({ mount }) => {
  const dialog: Dialog = {
    error: new Error(''),
    callback: () => console.log('callback'),
    secondaryActionText: 'test',
    closeOnCallback: true,
  };

  const component = await mount(ErrorDialogTest, {
    props: {
      errorInfo: {
        name: 'Error',
        message: 'Test message',
      },
      dialog: dialog,
    },
  });
  const page = component.page();
  const dialogPage = new ErrorDialogPage(page);
  // await expect(page.getByText('Test message', { exact: true })).toBeVisible();
  await expect(dialogPage.closeBtn).toBeVisible();
  await page.getByRole('button', { name: 'Close' }).click();
  await expect(dialogPage.closeBtn).not.toBeVisible();
});

test('Error Dialog is called with specific error type', async ({ mount }) => {
  const dialog: Dialog = {
    error: new Error(''),
    callback: () => console.log('callback'),
    secondaryActionText: 'test',
    closeOnCallback: true,
  };

  /**
   * XmlConversionBadInputError is mapped to a specific template which calls error text from the translation framework,
   * therefore the error message in the error object should not be visible.
   */
  const component = await mount(ErrorDialogTest, {
    props: {
      errorInfo: {
        name: 'XmlConversionBadInputError',
        message: 'This should not be seen',
      },
      dialog: dialog,
    },
  });
  const page = component.page();
  const dialogPage = new ErrorDialogPage(page);
  await expect(
    page.getByText(
      'Your metadata could not be converted to XML successfully.',
      { exact: false }
    )
  ).toBeVisible();
  await expect(dialogPage.goToDocsBtn).toBeVisible();
  await expect(
    page.getByText('This should not be seen', { exact: true })
  ).not.toBeVisible();
  await expect(dialogPage.vDialog).toBeVisible();
  await dialogPage.closeBtn.click();
  await expect(dialogPage.vDialog).not.toBeVisible();
});
