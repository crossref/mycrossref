import { Locator } from '@playwright/test';

export class CntRegFormComponent {
  readonly component: unknown;
  readonly nextBtn: Locator;
  readonly previousBtn: Locator;
  readonly fieldArticleTitle;
  readonly fieldIssnOnline: Locator;
  readonly fieldIssuePublishedOnline: Locator;
  readonly fieldIssuePublishedInPrint: Locator;
  readonly fieldIssueNumber: Locator;
  readonly fieldVolumeNumber: Locator;
  readonly controlIssuePublishedOnline: Locator;
  readonly controlIssuePublishedInPrint: Locator;
  readonly isRequiredMsgIssuePublishedOnline: Locator;
  readonly isRequiredMsgIssuePublishedInPrint: Locator;

  constructor(component: any) {
    this.component = component;
    this.nextBtn = component.getByRole('button', { name: 'Next' });
    this.previousBtn = component.getByRole('button', { name: 'Previous' });
    this.fieldArticleTitle = component.getByLabel('Article title*');
    this.fieldIssnOnline = component.getByLabel('Online ISSN of journal');
    this.fieldIssuePublishedOnline = component.getByLabel(
      'Issue published online (optional)'
    );
    this.fieldIssuePublishedInPrint = component.getByLabel(
      'Issue published in print (optional)'
    );
    this.fieldIssueNumber = component.getByLabel('Issue number (optional)');
    this.fieldVolumeNumber = component.getByLabel('Volume number (optional)');
    this.controlIssuePublishedOnline = component.locator(
      'div.control[label="Issue published online (optional)"]'
    );
    this.controlIssuePublishedInPrint = component.locator(
      'div.control[label="Issue published in print (optional)"]'
    );
    this.isRequiredMsgIssuePublishedOnline =
      this.controlIssuePublishedOnline.locator(
        '.v-messages__message:has-text("is required")'
      );
    this.isRequiredMsgIssuePublishedInPrint =
      this.controlIssuePublishedInPrint.locator(
        '.v-messages__message:has-text("is required")'
      );
  }
}
