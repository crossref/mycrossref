import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';
import journalArticleSchema from '../../src/forms/schemas/json/journalArticleForm/journalArticleForm.schema.json' assert { type: 'json' };
import { ErrorObject } from 'ajv';

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    article: {
      type: 'object',
      properties: {
        contributors: {
          type: 'array',
          items: {
            $ref: '#/definitions/person',
          },
          i18n: 'contentRegistrationForm_article_contributors',
        },
      },
    },
  },
  definitions: {
    person: {
      title: 'Person',
      type: 'object',
      properties: {
        givenName: {
          i18n: 'person.givenName',
          type: 'string',
        },
        familyName: {
          i18n: 'person.familyName',
          type: 'string',
        },
        suffix: {
          i18n: 'person.suffix',
          type: 'string',
          maxLength: 10,
        },
        affiliation: {
          title: 'Affiliation',
          type: 'object',
          properties: {
            institution: {
              i18n: 'person.affiliation.institution',
              type: 'string',
            },
            ror: {
              i18n: 'person.affiliation.ror',
              type: 'string',
            },
          },
        },
        orcid: {
          i18n: 'person.ORCID',
          type: 'string',
          pattern:
            '^[Hh][Tt][Tt][Pp][Ss]://orcid.org/\\d{4}-\\d{4}-\\d{4}-\\d{3}[0-9xX]',
          uniqueValue: true,
        },
        role: {
          i18n: 'person.role',
          type: 'string',
          enum: [
            'first-author',
            'additional-author',
            'editor',
            'chair',
            'reviewer',
            'review-assistant',
            'stats-reviewer',
            'reviewer-external',
            'reader',
            'translator',
          ],
        },
        sequence: {
          type: 'string',
          i18n: 'contentRegistrationForm_article_contributor_sequence',
          enum: ['first', 'additional'],
        },
      },
      required: ['role', 'familyName', 'sequence'],
    },
  },
};

// UI schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      scope: '#/properties/article/properties/contributors',
      options: {
        showSortButtons: true,
        childLabelProp: 'familyName',
      },
    },
  ],
};

const data = {
  article: {
    contributors: [
      {
        givenName: 'Josiah',
        familyName: 'Carberry',
        suffix: 'Jr.',
        affiliation: {
          institution: 'AFFIL',
          ror: '',
        },
        orcid: 'https://orcid.org/0000-0002-1825-0097',
        role: 'first-author',
        sequence: 'first',
      },
      {
        role: 'additional-author',
        familyName: 'carberry',
        givenName: 'j',
        affiliation: {
          institution: 'University of Oxford',
          ror: 'https://ror.org/052gg0110',
        },
        orcid: 'https://orcid.org/0000-0002-1825-0097',
        sequence: 'additional',
      },
    ],
  },
};

test.use({});

// Test to check if error is shown for non-unique abstract languages
test('Displays error for duplicate Orcid IDs', async ({ mount }) => {
  let errors: ErrorObject[] = [];
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
    on: {
      'update:errors': (e) => {
        errors = e;
      },
    },
  });

  await expect.poll(() => errors).toHaveLength(2);
  // Check that each error is of the keyword 'uniqueValue'
  errors.forEach((error) => {
    expect(error.keyword).toBe('uniqueValue');
  });
  await expect(component).toContainText(
    'Please enter a unique ORCID iD for each contributor.'
  );
});

// Test to check if unique abstract languages pass validation
test('Passes validation with unique abstract languages', async ({ mount }) => {
  let errors: ErrorObject[] = [];
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
    on: {
      'update:errors': (e) => {
        errors = e;
      },
    },
  });

  await expect.poll(() => errors).toHaveLength(2);
  errors.forEach((error) => {
    expect(error.keyword).toBe('uniqueValue');
  });

  await component
    .getByLabel('ORCID iD (optional)')
    .first()
    .fill('https://orcid.org/0000-0002-1825-0095');
  await expect.poll(() => errors).toHaveLength(0);
});
