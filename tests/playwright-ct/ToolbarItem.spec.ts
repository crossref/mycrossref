import { expect, test } from '@playwright/experimental-ct-vue';
import { Locator, Page } from 'playwright/test';
import ToolbarItemTestComponent from './ToolbarItemTestComponent.vue';

class ToolbarItemPage {
  readonly page: Page;
  readonly activator: Locator;
  readonly callbackTarget: Locator;

  constructor(page: Page) {
    this.page = page;
    this.activator = page.locator('[data-test-id="toolbar-item-activator"]');
    this.callbackTarget = page.locator(
      '[data-test-id="toolbar-item-callback-target"]'
    );
  }
}

test.use({});

test('Toolbar Item renders', async ({ mount }) => {
  const component = await mount(ToolbarItemTestComponent, {});
  const page = component.page();
  const toolbarItemPage = new ToolbarItemPage(page);
  await expect(component.getByLabel('Download record')).toBeVisible();
  await toolbarItemPage.activator.click();
  await expect(toolbarItemPage.callbackTarget).toBeVisible();
  await expect(page.locator('body')).toContainText('Download record');
  await toolbarItemPage.callbackTarget.click();
  await expect(component).toContainText('CALLBACK RECEIVED');
});
