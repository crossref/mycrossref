import { test, expect } from '@playwright/experimental-ct-vue';
import { Locator, Page } from 'playwright/test';
import OrganizationUsersTestComponent from './OrganizationUsersTestComponent.vue';

const mockUsers = [
  {
    userId: '0380dce2-2de0-4009-bf2a-64f128eae10e',
    username: 'test.metadata.admin@crossref.org',
  },
];

const mockNewUser = {
  username: 'test.user@crossref.org',
};
class OrganizationUsersTablePage {
  readonly page: Page;
  readonly addBtn: Locator;
  readonly modifyUserMenuTrigger: Locator;
  readonly userEmailInput: Locator;
  readonly addNewUserSaveBtn: Locator;
  readonly deleteBtn: Locator;

  constructor(page: Page) {
    this.page = page;
    this.addBtn = page.locator('[data-test-id="add-organization-user-button"]');
    this.modifyUserMenuTrigger = page
      .getByRole('row', { name: mockUsers[0].username })
      .getByRole('button');
    this.userEmailInput = page.getByLabel('User Email*');
    this.addNewUserSaveBtn = page.locator(
      '[data-test-id="organization-users-card__add-new-user-button"]'
    );
    this.deleteBtn = page.getByText('Delete');
  }
}

let PO: OrganizationUsersTablePage;
let page: Page;

// Utility function to set up the component and page object
const setupOrganizationUsersTablePage = async (mount: any, props: any) => {
  const component = await mount(OrganizationUsersTestComponent, { props });
  page = component.page();
  PO = new OrganizationUsersTablePage(page);
  return component;
};

test.use({});

const targetUrl = 'not-a-real-url';

test.describe('Users Table', () => {
  test.describe('Staff view', () => {
    const props = {
      canModifyUsers: true,
      showDialog: false,
      dialogLoading: false,
      dialogTitle: '',
      users: mockUsers,
      addUserFormData: {},
      organization: {
        memberId: 1,
      },
    };
    test('Can see modify user controls', async ({ mount }) => {
      const component = await setupOrganizationUsersTablePage(mount, props);
      const page = component.page();
      await expect(PO.addBtn).toBeVisible();
      await expect(PO.modifyUserMenuTrigger).toBeVisible();
      await PO.modifyUserMenuTrigger.click();
      await expect(PO.deleteBtn).toBeVisible();
    });
    test('Can add a user', async ({ mount }) => {
      const component = await setupOrganizationUsersTablePage(mount, props);
      const page = component.page();
      await PO.addBtn.click();
      await PO.userEmailInput.fill(mockNewUser.username);
      await page.waitForTimeout(1000);
      await expect(PO.addNewUserSaveBtn).toBeEnabled();

      // listen out for the add request
      const requestPromise = page.waitForRequest((request) => {
        const url = new URL(request.url());
        return (
          request.method() === 'POST' &&
          url.pathname === '/auth/realms/crossref/crossref-management/user' &&
          url.searchParams.get('role') === 'USER'
        );
      });
      await PO.addNewUserSaveBtn.click();

      const request = await requestPromise;
      console.log(request.url());
    });
    test('Can delete a user', async ({ mount }) => {
      const component = await setupOrganizationUsersTablePage(mount, props);
      const page = component.page();
      await page.route(
        '*/**/auth/realms/crossref/crossref-management/user/*',
        async (route) => {
          return route.fulfill({
            status: 204,
          });
        }
      );
      await PO.modifyUserMenuTrigger.click();
      // listen out for the delete request
      const requestPromise = page.waitForRequest((request) => {
        const urlPattern =
          /auth\/realms\/crossref\/crossref-management\/user\/[0-9a-fA-F-]+/;
        return request.method() === 'DELETE' && urlPattern.test(request.url());
      });
      await PO.deleteBtn.click();
      await expect(
        page.getByText(
          'Delete Metadata Plus Admin test.metadata.admin@crossref.org?'
        )
      ).toBeVisible();
      await page.getByRole('button', { name: 'DELETE' }).click();
      const request = await requestPromise;
    });
  });

  test.describe('User view', () => {
    const props = {
      canModifyUsers: false,
      showDialog: false,
      dialogLoading: false,
      dialogTitle: '',
      users: mockUsers,
      addUserFormData: {},
      organization: {
        memberId: 1,
      },
    };
    test('Cannot see modify user controls', async ({ mount }) => {
      const component = await setupOrganizationUsersTablePage(mount, props);
      await expect(PO.addBtn).toBeHidden();
      await expect(PO.modifyUserMenuTrigger).toBeHidden();
    });
    test('Users are shown', async ({ mount }) => {
      const component = await setupOrganizationUsersTablePage(mount, props);
      await expect(
        component.getByRole('row', { name: mockUsers[0].username })
      ).toBeVisible();
    });
  });
});
