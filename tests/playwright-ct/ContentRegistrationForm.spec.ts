import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';
import ContentRegistrationForm from '@/views/ContentRegistrationForm.vue';
import { useAppMachine } from '@/composable/useAppMachine';
import { useActor } from '@xstate/vue';
import journalArticleData from '../resources/json.formdata/journal_article_1.download.json' assert { type: 'json' };
import { useFormMachine } from '@/composable/useFormMachine';
import { CntRegFormComponent } from './CntRegFormComponent';
import { ContentRegistrationPage } from '../playwright/ContentRegistrationPage';

const TEST_RECORD_ID = 'ca069826-e09f-4faf-bc1e-fba7041283b0';

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    article: {
      type: 'object',
      properties: {
        title: {
          type: 'string',
        },
      },
    },
  },
};

// ui schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      type: 'Control',
      scope: '#/properties/article/properties/title',
      i18n: 'form_input_article_title_title',
    },
  ],
};
const data = {
  article: {},
};

const formConfig = {
  debounceWait: 0,
};

test.use({});

test('Form wrapper renders', async ({ mount }) => {
  // @ts-ignore
  const component = await mount(ContentRegistrationForm, {
    props: {
      recordType: 'journal-article',
      recordName: 'test',
      recordId: TEST_RECORD_ID,
      formData: {},
    },
  });
  const page = component.page();
  const formComponent = new CntRegFormComponent(component);
  // CntRegFormComponent.nextBtn.click();

  await expect(component).toContainText('Journal');
});

test('Formdata can be passed in via props', async ({ mount }) => {
  // @ts-ignore
  const component = await mount(ContentRegistrationForm, {
    props: {
      recordType: 'journal-article',
      recordName: 'test',
      recordId: TEST_RECORD_ID,
      formData: journalArticleData.formData,
    },
  });
  const page = component.page();
  const formPage = new CntRegFormComponent(component);
  await formPage.nextBtn.click();
  await expect(component.getByLabel('Article title*')).toHaveValue(
    'ARTICLE TITLE'
  );
});

test('ISSN lookup renderer', async ({ mount }) => {
  // @ts-ignore
  const component = await mount(ContentRegistrationForm, {
    props: {
      recordType: 'journal-article',
      recordName: 'test',
      recordId: TEST_RECORD_ID,
      formData: journalArticleData.formData,
    },
  });
  const page = component.page();
  await page.route(
    'https://api.crossref.org/journals/1111-1111?mailto=frontend+console@crossref.org',
    async (route) => {
      const json = {
        status: 'ok',
        'message-type': 'journal',
        'message-version': '1.0.0',
        message: {
          'last-status-check-time': 1718938822009,
          counts: {
            'current-dois': 0,
            'backfile-dois': 2,
            'total-dois': 2,
          },
          breakdowns: {
            'dois-by-issued-year': [
              [2001, 1],
              [1994, 1],
            ],
          },
          publisher: '',
          coverage: {
            'affiliations-current': 0,
            'similarity-checking-current': 0,
            'descriptions-current': 0,
            'ror-ids-current': 0,
            'funders-backfile': 0,
            'licenses-backfile': 0,
            'funders-current': 0,
            'affiliations-backfile': 0,
            'resource-links-backfile': 0,
            'orcids-backfile': 0,
            'update-policies-current': 0,
            'ror-ids-backfile': 0,
            'orcids-current': 0,
            'similarity-checking-backfile': 0,
            'references-backfile': 0.5,
            'descriptions-backfile': 0,
            'award-numbers-backfile': 0,
            'update-policies-backfile': 0,
            'licenses-current': 0,
            'award-numbers-current': 0,
            'abstracts-backfile': 0,
            'resource-links-current': 0,
            'abstracts-current': 0,
            'references-current': 0,
          },
          title: 'SIAM E-Journal on Applied Dynamical Systems',
          subjects: [],
          'coverage-type': {
            all: {
              'last-status-check-time': 1718938822009,
              affiliations: 0,
              abstracts: 0,
              orcids: 0,
              licenses: 0,
              references: 0.5,
              funders: 0,
              'similarity-checking': 0,
              'award-numbers': 0,
              'ror-ids': 0,
              'update-policies': 0,
              'resource-links': 0,
              descriptions: 0,
            },
            backfile: {
              'last-status-check-time': 1718938822009,
              affiliations: 0,
              abstracts: 0,
              orcids: 0,
              licenses: 0,
              references: 0.5,
              funders: 0,
              'similarity-checking': 0,
              'award-numbers': 0,
              'ror-ids': 0,
              'update-policies': 0,
              'resource-links': 0,
              descriptions: 0,
            },
            current: {
              'last-status-check-time': 1718938822009,
              affiliations: 0,
              abstracts: 0,
              orcids: 0,
              licenses: 0,
              references: 0,
              funders: 0,
              'similarity-checking': 0,
              'award-numbers': 0,
              'ror-ids': 0,
              'update-policies': 0,
              'resource-links': 0,
              descriptions: 0,
            },
          },
          flags: {
            'deposits-abstracts-current': false,
            'deposits-orcids-current': false,
            deposits: true,
            'deposits-affiliations-backfile': false,
            'deposits-update-policies-backfile': false,
            'deposits-similarity-checking-backfile': false,
            'deposits-award-numbers-current': false,
            'deposits-resource-links-current': false,
            'deposits-ror-ids-current': false,
            'deposits-articles': true,
            'deposits-affiliations-current': false,
            'deposits-funders-current': false,
            'deposits-references-backfile': true,
            'deposits-ror-ids-backfile': false,
            'deposits-abstracts-backfile': false,
            'deposits-licenses-backfile': false,
            'deposits-award-numbers-backfile': false,
            'deposits-descriptions-current': false,
            'deposits-references-current': false,
            'deposits-resource-links-backfile': false,
            'deposits-descriptions-backfile': false,
            'deposits-orcids-backfile': false,
            'deposits-funders-backfile': false,
            'deposits-update-policies-current': false,
            'deposits-similarity-checking-current': false,
            'deposits-licenses-current': false,
          },
          ISSN: ['1111-1111'],
          'issn-type': [
            {
              type: 'print',
              value: '1111-1111',
            },
          ],
        },
      };
      await route.fulfill({ json });
    }
  );
  const formPage = new CntRegFormComponent(component);
  await expect(formPage.fieldArticleTitle).toHaveValue('ARTICLE TITLE');
  await formPage.nextBtn.click();
  await expect(component).toContainText('ISSN');
  await expect(formPage.fieldIssnOnline).toHaveValue('3333-4444');
  await formPage.fieldIssnOnline.fill('11111111');
  await expect(formPage.fieldIssnOnline).toHaveValue('1111-1111');
  await expect(component).toContainText('Matched ISSN to Journal');
});

test('One Publication date is required when and issue or volume number is provided', async ({
  mount,
}) => {
  // @ts-ignore
  const component = await mount(ContentRegistrationForm, {
    props: {
      recordType: 'journal-article',
      recordName: 'test',
      recordId: TEST_RECORD_ID,
      formData: journalArticleData.formData,
      config: formConfig,
    },
  });
  const page = component.page();
  const formPage = new CntRegFormComponent(component);
  await formPage.nextBtn.click();
  await formPage.nextBtn.click();
  await formPage.fieldIssuePublishedOnline.fill('');
  await formPage.fieldIssuePublishedInPrint.fill('');
  await formPage.controlIssuePublishedOnline.click();
  await expect(formPage.controlIssuePublishedOnline).toBeVisible();
  await expect(formPage.isRequiredMsgIssuePublishedOnline).toBeVisible();
  await expect(formPage.isRequiredMsgIssuePublishedInPrint).toBeVisible();
  await formPage.fieldIssuePublishedInPrint.fill('2025-01-15');
  await expect(formPage.isRequiredMsgIssuePublishedOnline).not.toBeVisible();
  await expect(formPage.isRequiredMsgIssuePublishedInPrint).not.toBeVisible();
  await formPage.fieldIssuePublishedInPrint.fill('');
  await expect(formPage.isRequiredMsgIssuePublishedOnline).toBeVisible();
  await expect(formPage.isRequiredMsgIssuePublishedInPrint).toBeVisible();
  await formPage.fieldIssuePublishedOnline.fill('2025-01-15');
  await expect(formPage.isRequiredMsgIssuePublishedOnline).not.toBeVisible();
  await expect(formPage.isRequiredMsgIssuePublishedInPrint).not.toBeVisible();
});
