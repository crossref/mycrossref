import { test, expect } from '@playwright/experimental-ct-vue';
import JsonFormsTestComponent from './JsonFormsTestComponent.vue';

// JSON Schema for the form data structure
const schema = {
  type: 'object',
  properties: {
    article: {
      type: 'object',
      properties: {
        "title": {
          "type": "string"
        },
      },
    },
  },
};

// ui schema for the form rendering structure
const uischema = {
  type: 'VerticalLayout',
  elements: [
    {
      "type": "Control",
      "scope": "#/properties/article/properties/title",
      "i18n": "form_input_article_title_title"
    },
  ],
};
const data = {
  article: {
  },
};

test.use({});

test('Info text is shown', async ({
  mount,
}) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
    },
  });
  const page = component.page();

  await expect(component).toContainText('The title of the article.');
});

test('Form is readonly', async ({
  mount,
}) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
      readonly: true,
    },
  });
  const page = component.page();

  await expect(component).toContainText('The title of the article.');
  await expect(component.getByLabel('Article title')).not.toBeEditable();
});

test('Debounce wait config is passed down', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
      config: {
        debounceWait: 123,
      },
      testConfig: {
        showConfig: true,
      },
    },
  });

  await expect(component).toContainText('"debounceWait": 123');
});

test('Whitespace is trimmed (start and end)', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
      config: {
        debounceWait: 0,
        trim: 'trimBoth',
      },
      testConfig: {
        showRawData: true,
      },
    },
  });
  const titleField = component.getByLabel('Article title');
  const rawDataOutput = component.locator('#data');
  await titleField.fill('  test title  ');
  await component.getByText('Article title').first().click();
  await expect(rawDataOutput).toContainText('"title": "test title"');
});

test('Whitespace is trimmed (all)', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
      config: {
        debounceWait: 0,
        trim: 'trimAll',
      },
      testConfig: {
        showRawData: true,
      },
    },
  });
  const titleField = component.getByLabel('Article title');
  const rawDataOutput = component.locator('#data');
  await titleField.fill('  test title  ');
  await   component.getByText('Article title').first().click();
  await expect(rawDataOutput).toContainText('"title": "testtitle"');
});

test('Whitespace is not trimmed by default', async ({ mount }) => {
  const component = await mount(JsonFormsTestComponent, {
    props: {
      schema: schema,
      uischema: uischema,
      initialData: data,
      config: {
        debounceWait: 0,
      },
      testConfig: {
        showRawData: true,
      },
    },
  });
  const titleField = component.getByLabel('Article title');
  const rawDataOutput = component.locator('#data');
  await titleField.fill('  test title  ');
  await component.getByText('Article title').first().click();
  await expect(rawDataOutput).toContainText('"title": "  test title  "');
});
