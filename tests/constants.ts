export const LOGIN_USERNAME_ONE_ROLE = 'onerole@domain.tld';
export const LOGIN_PASSWORD_ONE_ROLE = 'onerolepassword';
export const LOGIN_USERNAME_MANY_ROLES = 'manyroles@domain.tld';
export const LOGIN_PASSWORD_MANY_ROLES = 'manyrolespassword';
export const LOGIN_USERNAME_NO_ROLES = 'noroles@domain.tld';
export const LOGIN_PASSWORD_NO_ROLES = 'norolespassword';
