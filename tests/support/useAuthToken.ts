import { env } from '@/env';
import { setTlsRejectUnauthorized } from './setTlsRejectUnauthorized';
import { ref, Ref } from 'vue';
import { Issuer, TokenSet } from 'openid-client';
export type Token = Ref<string | null>;

export interface AuthConfig {
  realm: string;
  clientId: string;
  username: string;
  password: string;
}

export interface TokenProvider {
  getToken: () => Promise<string | undefined>;
  cleanup: () => void;
}

const setTokenValue = (tokenSet: TokenSet, token: Ref<string | null>): void => {
  if (tokenSet.access_token) {
    token.value = tokenSet.access_token;
    logTokenExpiration(tokenSet);
  } else {
    token.value = null;
    console.log('No access token was provided.');
  }
};

const logTokenExpiration = (tokenSet: TokenSet): void => {
  const expiresIn = tokenSet.expires_at
    ? tokenSet.expires_at - Math.floor(Date.now() / 1000)
    : 0;

  console.log(`Token expires in ${expiresIn} seconds.`);
};

export const useAuthToken = async (
  authConfig: AuthConfig
): Promise<TokenProvider> => {
  setTlsRejectUnauthorized(env().keycloakServerURL);
  const token = ref('');

  const keycloakIssuer: Issuer = await Issuer.discover(
    `${env().keycloakServerURL}/realms/${authConfig.realm}`
  );

  const client = new keycloakIssuer.Client({
    client_id: authConfig.clientId,
    token_endpoint_auth_method: 'none',
  });

  let tokenSet = await client.grant({
    grant_type: 'password',
    username: authConfig.username,
    password: authConfig.password,
  });

  setTokenValue(tokenSet, token);

  const refreshTokenIntervalId = setInterval(async () => {
    console.log('Refreshing token...');
    const refreshToken = tokenSet.refresh_token;
    if (refreshToken) {
      tokenSet = await client.refresh(refreshToken);
      setTokenValue(tokenSet, token);
    }
  }, 58 * 1000);

  return {
    getToken: () => Promise.resolve(token.value),
    cleanup: () => {
      clearInterval(refreshTokenIntervalId);
    },
  };
};
