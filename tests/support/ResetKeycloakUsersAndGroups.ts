import {
  addUsersToGroups,
  assignRolesToUser,
  createGroup,
  createNestedGroups,
  createUser,
  deleteAllChildOrganizations,
  deleteAllGroups,
  deleteAllUsers,
  fetchAllGroups,
  fetchAllUsers,
} from './KeycloakAdmin';
import { setTlsRejectUnauthorized } from './setTlsRejectUnauthorized';
import axios from 'axios';
import { Issuer, TokenSet } from 'openid-client';
import { ref, Ref } from 'vue';
import { env } from '../../src/env';

type Token = Ref<string | null>;

// Configuration
const keycloakBaseUrl = `${env().keycloakServerURL}`;
const realmName = 'crossref';
const token: Ref<null | string> = ref(null); // Initialize with null or an initial value

const userData = [
  {
    email: 'fake.staff@crossref.org',
    password: 'pwdPWD1!',
    roles: ['ROLE_USER', 'ROLE_STAFF'],
  },
  {
    email: 'fake.user@crossref.org',
    password: 'pwdPWD1!',
    roles: ['ROLE_USER'],
  },
];

const groupsData = [
  {
    name: 'organizations',
    children: [
      { name: 'org1', memberId: 1, users: ['fake.user@crossref.org'] },
      { name: 'org2', memberId: 2, users: ['fake.user@crossref.org'] },
      { name: 'org3', memberId: 3, users: [] },
      { name: 'org4', memberId: 4, users: [] },
    ],
  },
  {
    name: 'void',
    children: [],
  },
];

setTlsRejectUnauthorized(keycloakBaseUrl);

const setTokenValue = (
  tokenSet: { access_token?: string },
  token: Ref<string | null>
): void => {
  if (tokenSet.access_token) {
    token.value = tokenSet.access_token;
  } else {
    token.value = null;
  }
};

const logTokenExpiration = (tokenSet: TokenSet): void => {
  const expiresIn = tokenSet.expires_at
    ? tokenSet.expires_at - Math.floor(Date.now() / 1000)
    : 0;

  // Log the remaining time in seconds
  console.log(`Token expires in ${expiresIn} seconds.`);
};

export const resetUsersAndGroups = async () => {
  const users = [];
  const groups = [];
  const keycloakIssuer: Issuer = await Issuer.discover(
    `${keycloakBaseUrl}/realms/master`
  );

  const client = new keycloakIssuer.Client({
    client_id: 'admin-cli', // Same as `clientId` passed to client.auth()
    token_endpoint_auth_method: 'none', // to send only client_id in the header
  });

  // Use the grant type 'password'
  let tokenSet = await client.grant({
    grant_type: 'password',
    username: 'admin',
    password: 'admin',
  });

  setTokenValue(tokenSet, token);

  logTokenExpiration(tokenSet);

  // Periodically using refresh_token grant flow to get new access token here
  const refreshTokenIntevalId = setInterval(async () => {
    console.log('Refreshing token...');
    const refreshToken = tokenSet.refresh_token;
    if (refreshToken) {
      tokenSet = await client.refresh(refreshToken);
      setTokenValue(tokenSet, token);
      logTokenExpiration(tokenSet);
      // kcAdminClient.setAccessToken(tokenSet.access_token);
    }
  }, 58 * 1000); // 58 seconds

  try {
    await deleteAllUsers(token, keycloakBaseUrl, realmName);
    // await deleteAllGroups(token, keycloakBaseUrl, realmName);
    await deleteAllChildOrganizations(token, keycloakBaseUrl, realmName);
    // Create groups and nested groups
    const createdGroups = await createNestedGroups(
      token,
      groupsData,
      keycloakBaseUrl,
      realmName
    );

    for (const user of userData) {
      await createUser(
        token,
        user.email,
        user.password,
        keycloakBaseUrl,
        realmName
      );
      const userResponse = await axios.get(
        `${keycloakBaseUrl}/admin/realms/${realmName}/users`,
        {
          params: { username: user.email },
          headers: { Authorization: `Bearer ${token.value}` },
        }
      );

      const createdUser = userResponse.data[0];
      await assignRolesToUser(
        token,
        createdUser.id,
        user.roles,
        keycloakBaseUrl,
        realmName
      );
    }
    addUsersToGroups(token, groupsData, keycloakBaseUrl, realmName);
    return {
      users: await fetchAllUsers(token, keycloakBaseUrl, realmName),
      groups: await fetchAllGroups(token, keycloakBaseUrl, realmName),
    };
  } catch (e) {
    console.log(e);
    return {};
  } finally {
    clearInterval(refreshTokenIntevalId);
  }
};
