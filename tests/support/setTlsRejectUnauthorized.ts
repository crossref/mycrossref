export const setTlsRejectUnauthorized = (url: string): void => {
  const { protocol, hostname } = new URL(url);

  if (
    protocol.startsWith('http') &&
    (hostname === '127.0.0.1' ||
      hostname === 'localhost' ||
      hostname === '::1' ||
      hostname.endsWith('.local'))
  ) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
  } else {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '1';
  }
};
