# Crossref Console

The frontend for Crossref services.

## Prerequisites

- Git
- Node 18+

## Install

- `npm ci`

## Develop

- `npm run dev`

## Running tests

Unit tests (Vitest)

- `npm run test:unit`

End to end tests (Playwright)

- `npm run test`

Component tests (Playwright component testing)

- `npm run test-ct`

## Type checking

- `npm run type-check`

## Building

(Runs type check automatically before building)

- `npm run build`

### Editor setup

We recommend developing using VS Code or Webstorm.
Using the [recommended extensions](.vscode/extensions.json) type checking, formatting and linting should work out of the box.

### Environment Variables

Configuration can be passed in via the environment. The environment is parsed and collated into configuration values by `env.ts`.
All environment variables are treated as strings. If the applicable environment variable has not been set, all configuration values are assigned safe defaults (false).
Boolean configuration settings should be named such that their default can always be 'false', and should be set to true only by an explicit check for the string 'true'.

Values are taken in this order:

1. From `process.env` (baked into the app during `npm run build` or at runtime by the webpack dev server).
2. From `window.env` which is populated with any environment variables prefixed by `VUE_APP_` when the `./inject_configuration.sh` script is run.

`inject_configuration.sh` can be run after the app has been compiled, to allow for configuration changes without rebuilding the entire project.

These variables are available (where no default is specified, this implies an empty string).

| name                                      | description                                                                | default value                                      |
|-------------------------------------------|----------------------------------------------------------------------------|----------------------------------------------------|
| VUE_APP_AUTH_API_BASE_URL                 | For Legacy auth                                                            | https://test.crossref.org                          |
| VUE_APP_DEV_SETTINGS_MENU                 | Developer settings menu                                                    | false                                              |
| VUE_APP_HTTP_MOCKING                      | HTTP mocking                                                               | false                                              |
| VUE_APP_I18N_DEBUG_LOGGING                | Verbose i18n logging                                                       | false                                              |
| VUE_APP_PASSWORD_RESET_URL                | Password reset URL                                                         | https://authenticator.crossref.org/reset-password/ |
| VUE_APP_REDIRECT_ON_ROLE_AUTHORISATION    | For Legacy auth                                                            | false                                              |
| VUE_APP_SENTRY_DSN                        | Sentry.io data source URL, available from the Sentry dashboard             |                                                    |
| VUE_APP_SENTRY_ENVIRONMENT                | Sentry.io environment eg production or staging                             |                                                    |
| VUE_APP_SENTRY_RELEASE                    | Sentry.io release tag, normally the semantic version from Semantic Release |                                                    |
| VUE_APP_USERSNAP_API_KEY                  | Available from the Usersnap feedback widget dashboard                      | false                                              |
| VUE_APP_USERSNAP_GLOABL_API_KEY           | Available from the Usersnap feedback widget dashboard                      | false                                              |
| VUE_APP_XML_DEPOSIT_API_HOST              | Host used to for XML deposit                                               | https://test.crossref.org                          |
| VUE_APP_XSTATE_INSPECTOR_ENABLED          | XState State Machine Visual Inspector, for use in local development        | false                                              |
| VUE_APP_KEYCLOAK_ENABLED                  | Enable the Keycloak auth Javascript integration                            | false                                              |
| VUE_APP_KEYCLOAK_DEBUG                    | Enable the Keycloak Javascript debug logging                               | false                                              |
| VUE_APP_KEYCLOAK_URL                      | The URL of the Keycloak Authentication service                             |                                                    |
| VUE_APP_KEYMAKER_API_BASE_URL             | Base URL of the idm API                                                    |
| VUE_APP_SHOW_THEME_PALETTE                | Show the theme options palette                                             | false                                              |
| VUE_APP_TEST_AUTH_CL_CLIENTNAME_USERNAME  | Username for Keycloak CLIENTNAME in e2e tests                              |
| VUE_APP_TEST_AUTH_CL_CLIENTNAME_PASSWORD  | Password for Keycloak CLIENTNAME in e2e tests                              |
| VUE_APP_TEST_AUTH_CL_CLIENTNAME_CLIENT_ID | Client ID for Keycloak CLIENTNAME in e2e tests                             |
| VUE_APP_TEST_AUTH_CL_CLIENTNAME_REALM     | Realm for Keycloak CLIENTNAME in e2e tests                                 | 
| VUE_APP_JOURNAL_ARTICLE_FORM_ENABLED      | Feature flag to enable Journal Article Registration form                   | false                                              |

## Configuration Injection

### Into compiled files

`inject_configuration.sh dist/index.html`

This will inject any variables prefixed with `VUE_APP_` into a variable `window.env` that is defined within `dist/index.html`.

### During development

Any variables set on `process.env` will override both any set within `window.env` and the defaults from `env.ts`.

You can use the built-in `dotenv` handling from vue-cli-service to read in variable from a `.env` file, bearing in mind that
only variables prefixed with `VUE_APP_` will be loaded.

## Authentication

Authentication is provided by the Keycloak backend, and dsb-norge/vue-keycloak-js (which wraps the official Keycloak Javascript adapter) on the frontend.
A user's authentication state is check on every page load by a re-direct to the Keycloak backend, thereafter the Javascript adapter makes periodic checks.
Some pages require the user to be authenticated. This is determined by the vue-router package, based on whether a route has the `meta` property `requiresAuthentication: true`. If a logged-out user accesses any of these routes, they are automatically re-directed to the Keycloak login page. The other way a user can login is by visiting the `/login` route.

Developers may find these entrypoints useful:

- [`src/composable/useAuth.ts`](src/composable/useAuth.ts) provides the composables which are used throughout the application to access properties like ready state and user information
- [`src/composable/useKeycloak.ts`](src/composable/useKeycloak.ts) wraps the underlying authentication provider and provides composables that are used to access keycloak state directly
- [`src/router/index.ts`](src/router/index.ts) contains the applications routes and logic to check whether a user needs to login before accessing them
- [`src/i18n/forms`](src/i18n/forms.ts) contains the glue code between `i18next` and JSON Forms
- [`src/main`](src/main.ts) integrates `dbs-norge/vue-keycloak-js` into the application and adds behaviour to the `onReady` / `onError` hooks
- [`src/components/TheUserMeny`](src/components/TheUserMenu.vue) renders the user menu and provides the logout button

## Loading state

The application's loading spinner can be displayed whenever there is a pending promise. Pending promises, can be added to the loading state composable. Once there are no unsettled promises, the loading spinner fades out.

These entrypoints may be useful:

- [`src/composable/useLoadingWithPromises.ts`](src/composable/useLoadingWithPromises.ts) provides the composable used throughout the application to add promises of pending operations, and access the application's loading state
- [`src/composable/useLoadingColors.ts`](src/composable/useLoadingWithPromises.ts) provides the composable used to provide synchronised color changes to the loading spinners and indicators
- [`src/components/LoadingSpinner.vue`](src/components/LoadingSpinner.vue) renders the main application loading spinner

## Notifications

Snackbar notifications displayed to the user are managed via the `notificationStore` Pinia store. This accepts notifications at any time, and queues them for display.

Unless over-ridden, notifications auto-dismiss after 4 seconds. Callbacks can be passed into a notification along with the text for their trigger button.

## Resource Management API

AKA the backend for Keymanager used an API client which is generated by `openapi-generator` from the OpenAPI spec. These generated files are found under `src/generated` and should not be manually modified.
There is currently a hard limit on 100 entries in the responses from keymanager, as the client generator does not yet handle pagination.

## Form editing

To add/remove/edit a form field, update the schema for the form, e.g. `src/schema/ContentRegistrationGrantSchema.json`

To update the messages for a form field

- Check for an `i18n` property in the JSON Schema for the field. Create it if need be.
- Each form translation is managed in its own namespace, e.g. `public/locales/en/content-registration-grant.ftl`
- Locate or add the message using the `i18n` property as the message name prefix, and the
  message you want to add or change as the suffix - For example, to add the description for a field with the `i18n` property of `depositor.depositor_name` add `depositor_depositor_name_description = Text of the description` to the english messages file - Add other messages using the same prefix, changing the suffix as required. The available suffixes are: - `title` - `description` - JSON Schema [error keywords](https://json-schema.org/draft/2019-09/json-schema-validation.html), e.g. - `error.required` - `error.pattern`
- As shown in the example above: In case dot characters (i.e. `.`) are used in an `i18n` key, then this dot must be replaced by an underscore (i.e. `_`) in the `.ftl` messages file.
- In case you want to share translations between multiple forms or the app itself: Place them in the `app.ftl` namespace which serves as a fallback in case a key is not available in a more specialized namespace.

## Internationalization

We use [i18next](https://www.i18next.com/).
The localization files are maintained in [`public/locales`](public/locales/).
We support the default i18next JSON format as well as [Fluent](https://projectfluent.org/) with the latter being preferred.

Developers might find the following entry points useful:

- [`src/composable/useI18n`](src/composable/useI18n.ts) sets up the `i18next` backend and provides the composables used throughout the application
- [`src/i18n/i18n`](src/i18n/i18n.ts) contains i18n constants like available locales and namespaces plus some helpers
- [`src/i18n/forms`](src/i18n/forms.ts) contains the glue code between `i18next` and JSON Forms
- [`src/main`](src/main.ts) integrates `i18next-vue` into the application, making `$t` in all components
- [`src/components/TheLanguageMenu`](src/components/TheLanguageMenu.vue) renders the language switcher and synchronizes the locale information on change through the whole application, i.e. `i18next`, `vuetify` and the state machines.

In development mode you can visit `/fluent-demo` to play around with the Fluent integration.
This component is not integrated in production.

For development you might want to enable multiple locales (e.g. `VUE_APP_LOCALES_ENABLED=en,de`) and enable i18n logging (i.e. `VUE_APP_I18N_DEBUG_LOGGING=true`) in your environment.

## Customize VueJS configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Troubleshooting

### Can't access variable before declaration in component tests

Likely to be due to an ESbuild [bug](https://github.com/evanw/esbuild/issues/1433) in handling the order of circular dependencies. Have you added an import deeply nested when it could be accessed from higher up the tree?

`import { aFunc } from '@package/name/quite/a/deep/path'`

might be exposed at 

`import { aFunc } from '@package/name'`

Switching to the highest export path will likely resolve correctly  .

## License

Console is released under the [MIT License](https://opensource.org/licenses/MIT)

Copyright Crossref and Contributors
