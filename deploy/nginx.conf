server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;
    root   /usr/share/nginx/html;
    index  index.html index.htm;

    location / {
        add_header Cache-Control "public, max-age=300"; # Cache for 5 minutes
        try_files $uri $uri/ /index.html;
    }

    location = /service-worker.js {
        add_header Cache-Control "no-cache,no-store,must-revalidate"; # Dont cache
        try_files $uri $uri/ /index.html;
    }

    location ~* \.(css|js|js.map)$ {
        access_log off;
        add_header Cache-Control "public, max-age=31556952"; # Cache for 1 year
        try_files $uri $uri/ /index.html;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    location /heartbeat/ {
        return 200 'OK';
        add_header Content-Type text/plain;
    }
}
