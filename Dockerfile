FROM node:16.15.0-alpine
ENV CYPRESS_INSTALL_BINARY="0"
RUN apk --no-cache add make g++ py3-pip git
RUN pip3 install awscli
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json /app/

CMD ["sh", "serve.sh"]
