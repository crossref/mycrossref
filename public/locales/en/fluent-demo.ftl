-demo_brand = Crossref
demo_variable = Hello { $name }
demo_placeable = Welcome to { -demo_brand }
demo_multi_line_text = multi = Text can also span multiple lines
  as long as each new line is indented
  by at least one space.
demo_block =
  Sometimes it's more readable to format
  multiline text as a "block", which means
  starting it on a new line. All lines must
  be indented by at least one space.  
demo_large_number = 1234567890.1234
demo_large_number_no_fractions = { NUMBER($demo_large_number, maximumFractionDigits: 0) }
demo_date = 2023 
demo_selctor = 
    { $unreadEmails ->
        [one] You have one unread email.
       *[other] You have { $unreadEmails } unread emails.
    }
demo_rank = { NUMBER($pos, type: "ordinal") ->
   [1] You finished first!
   [one] You finished {$pos}st
   [two] You finished {$pos}nd
   [few] You finished {$pos}rd
  *[other] You finished {$pos}th
}
demo_attributes = Predefined value
    .placeholder = email@example.com
    .aria-label = Login input value
    .title = Type your login email
