records_home_title = Mulai pengiriman       

accessibility_high_contrast_theme = Tema kontras tinggi

accessibility_field_style_outline = Garis besar bidang

organization_setting_api_title = Kunci Api

organization_setting_metadata_title = Pengguna Metadata Plus

organization_setting_org_title = Pengaturan Organisasi

resource-manager-organisation-users_title = Pengguna Metadata Plus

resource-manager-organisation-users_edit-form-title = Tambahkan pengguna

menu_item_content_registration_label = Pendaftaran Hibah

menu_item_psychoceramix_label = Psikoceramix

language_menu_button = Bahasa

error_required = Dibutuhkan

button-close = Menutup

button-ok = OKE

button_cancel_label = Membatalkan

button_continue_text = Melanjutkan

button_submit_text = Kirim

button_download_text = Unduh

home_headline = Selamat datang di MyCrossref

site_license_name = Lisensi Internasional Creative Commons Attribution 4.0

site_license_preamble = Konten situs ini dilisensikan di bawah a

arraylayout_add = Tambah baru

organization-create-confirm-button = Buat organisasi

confirm-leave-page-with-unsaved-changes = Anda yakin ingin keluar? Setiap perubahan yang belum disimpan akan hilang.

confirm-leave-page-confirm-button = Meninggalkan

customer_support_email_address = support@crossref.org

customer_support_contact_url = https://www.crossref.org/contact

open_dialog = buka dialog

i_accept = saya menerima

privacy_policy_title = Kebijakan pribadi

login_succeeded_no_roles_assigned = Tidak ada peran yang ditautkan ke akun Anda

user_avatar__logout = Keluar

ror_no_associated_ror_id = Tidak ada ID ROR terkait

ror_start_typing_to_search = Mulai mengetik untuk mencari

ror_no_matched_institutions = Tidak ada institusi yang cocok

ror_matched_institutions =
  { $count ->
    [one] Cocok dengan satu institusi
   *[other] Cocok { $count } institusi
  }

-metadata-plus = Metadata Plus

subscription-row-label =
  { $name ->
    [METADATA_PLUS] { -metadata-plus }
   *[OTHER] Umum
  } Langganan

subscription-status-text-label-active = aktif

subscription-status-text-label-inactive = tidak aktif

dialog-title-confirm-subscription-change = Konfirmasi perubahan Langganan

dialog-message-confirm-subscription-enable-METADATA_PLUS = Benar-benar memungkinkan { -metadata-plus } berlangganan?

dialog-message-confirm-subscription-disable-METADATA_PLUS = Benar-benar menonaktifkan { -metadata-plus } berlangganan?

user-added-to-organization = { $username } ditambahkan ke { $organization }

api-key-table-title = Kunci API

api-key-created-title = Kunci API dibuat

api-key-created-subtitle = Ini akan menjadi satu-satunya saat kunci akan ditampilkan.

api-key-created-keep-safe-instructions = Salin kunci dan simpan dengan aman.

api-key-created-copy-key-action = Salin kunci

api-key-created-key-copied = Kunci disalin

api-key-created-no-key-error = Terjadi kesalahan. Tidak ada kunci API yang dikembalikan. Silakan coba lagi.

api-key-created-copy-to-clipboard-error = Terjadi kesalahan saat menyalin ke papan klip. { $error }

api-key-disabled-help-contact-support-team = Hubungi tim dukungan Plus

api-key-disabled-help-title = Kunci API yang dinonaktifkan

api-key-disabled-help-keyid-title = Kunci { $apiKeyId }

api-key-disabled-help-explanation = Kunci ini telah dinonaktifkan. Setiap klien yang menggunakan kunci ini tidak akan lagi memiliki akses ke Layanan Crossref Plus. Silakan hubungi dukungan Crossref Plus untuk mengaktifkan kembali kunci API

error-no-clipboard-access-in-nonsecure-contexts = Akses clipboard hanya tersedia dalam konteks aman (HTTPS, localhost).

api-key-delete-confirmation-instructions = Silakan ketik DELETE untuk melanjutkan

api-key-delete-confirmation-subtitle = Yakin ingin menghapus kunci API ini?

api-key-delete-confirmation-title = Hapus kunci API

api-key-delete-confirmation-warning = Apa kamu yakin? Menghapus kunci API ini akan segera mencabutnya, dan akses setiap klien yang menggunakannya.

api-key-activate = Aktifkan Kunci

api-key-deactivate = Nonaktifkan Kunci

api-key-activate-confirm-title = Aktifkan Kunci API?

api-key-deactivate-confirm-title = Nonaktifkan Kunci API?

api-key-enable = Nonaktifkan Kunci

api-key-enable-confirm-button = Aktifkan Kunci

api-key-disable = Aktifkan Kunci

api-key-disable-confirm-button = Nonaktifkan Kunci

api-key-toggle = Alihkan kunci API

api-key-status-active = Aktif

api-key-status-inactive = Tidak aktif

