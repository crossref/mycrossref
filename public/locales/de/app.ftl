brandname = Crossreffe
welcome-to-brand = Wilcommen bei { $brandname }

action-no-thanks = Neine, danke

action-autofill = Automatisches Ausfüllen

learn-more = Mehr erfahren

records_home_title = Beginnen Sie mit der Einreichung des Datensatzes

accessibility_high_contrast_theme = Kontrastreiches Thema

accessibility_field_style_outline = Umrissfelder

organization_setting_api_title = API-Schlüssel

organization_setting_metadata_title = Metadata Plus-Benutzer

organization_setting_org_title = Organisationseinstellungen

resource-manager-organisation-users_title = Metadata Plus-Benutzer

resource-manager-organisation-users_edit-form-title = Benutzer hinzufügen

menu_item_content_registration_label = Grant-Registrierung

menu_item_psychoceramix_label = Psychoceramix

language_menu_button = Sprache

error_required = erforderlich

button-close = Schließen

button-ok = OK

button_cancel_label = Stornieren

button_continue_text = Weitermachen

button_submit_text = Einreichen

button_download_text = Herunterladen

home_headline = Willkommen bei MyCrossref

site_license_name = Creative Commons Attribution 4.0 Internationale Lizenz

site_license_preamble = Der Inhalt dieser Website ist lizenziert unter a

arraylayout_add = Neue hinzufügen

organization-create-confirm-button = Organisation schaffen

confirm-leave-page-with-unsaved-changes = Bist du sicher dass du gehen willst? Alle nicht gespeicherten Änderungen gehen verloren.

confirm-leave-page-confirm-button = Verlassen

customer_support_email_address = support@crossref.org

customer_support_contact_url = https://www.crossref.org/contact

open_dialog = Dialog öffnen

i_accept = Ich nehme an

privacy_policy_title = Datenschutz-Bestimmungen

login_succeeded_no_roles_assigned = Mit Ihrem Konto sind keine Rollen verknüpft

user_avatar__logout = Ausloggen

ror_no_associated_ror_id = Keine zugehörige ROR-ID

ror_start_typing_to_search = Beginnen Sie mit der Eingabe, um zu suchen

ror_no_matched_institutions = Keine passenden Institutionen

ror_matched_institutions =
  { $count ->
    [one] Entspricht einer Institution
   *[other] Passend { $count } Institutionen
  }

-metadata-plus = Metadaten Plus

subscription-row-label =
  { $name ->
    [METADATA_PLUS] { -metadata-plus }
   *[OTHER] Generisch
  } Abonnement

subscription-status-text-label-active = aktiv

subscription-status-text-label-inactive = inaktiv

dialog-title-confirm-subscription-change = Bestätigen Sie die Abonnementänderung

dialog-message-confirm-subscription-enable-METADATA_PLUS = Wirklich ermöglichen { -metadata-plus } Abonnement?

dialog-message-confirm-subscription-disable-METADATA_PLUS = Wirklich deaktivieren { -metadata-plus } Abonnement?

user-added-to-organization = { $username } wurde hinzugefügt { $organization }

api-key-table-title = API-Schlüssel

api-key-created-title = API-Schlüssel erstellt

api-key-created-subtitle = Dies ist das einzige Mal, dass der Schlüssel angezeigt wird.

api-key-created-keep-safe-instructions = Kopieren Sie den Schlüssel und bewahren Sie ihn sicher auf.

api-key-created-copy-key-action = Schlüssel kopieren

api-key-created-key-copied = Schlüssel kopiert

api-key-created-no-key-error = Ein Fehler ist aufgetreten. Es wurde kein API-Schlüssel zurückgegeben. Bitte versuche es erneut.

api-key-created-copy-to-clipboard-error = Beim Kopieren in die Zwischenablage ist ein Fehler aufgetreten. { $error }

api-key-disabled-help-contact-support-team = Kontaktieren Sie das Plus-Supportteam

api-key-disabled-help-title = Deaktivierter API-Schlüssel

api-key-disabled-help-keyid-title = Taste { $apiKeyId }

api-key-disabled-help-explanation = Dieser Schlüssel wurde deaktiviert. Jeder Kunde, der diesen Schlüssel verwendet, hat keinen Zugriff mehr auf die Crossref Plus-Dienste. Bitte wenden Sie sich an den Crossref Plus-Support, um den API-Schlüssel zu reaktivieren

error-no-clipboard-access-in-nonsecure-contexts = Der Zugriff auf die Zwischenablage ist nur in sicheren Kontexten (HTTPS, localhost) verfügbar.

api-key-delete-confirmation-instructions = Bitte geben Sie DELETE ein, um fortzufahren

api-key-delete-confirmation-subtitle = Sind Sie sicher, dass Sie diesen API-Schlüssel löschen möchten?

api-key-delete-confirmation-title = API-Schlüssel löschen

api-key-delete-confirmation-warning = Bist du dir sicher? Durch das Löschen dieses API-Schlüssels wird er sofort widerrufen und der Zugriff aller Clients, die ihn verwenden, wird sofort widerrufen.

api-key-activate = Schlüssel aktivieren

api-key-deactivate = Schlüssel deaktivieren

api-key-activate-confirm-title = API-Schlüssel aktivieren?

api-key-deactivate-confirm-title = API-Schlüssel deaktivieren?

api-key-enable = Schlüssel deaktivieren

api-key-enable-confirm-button = Schlüssel aktivieren

api-key-disable = Schlüssel aktivieren

api-key-disable-confirm-button = Schlüssel deaktivieren

api-key-toggle = API-Schlüssel umschalten

api-key-status-active = Aktiv

api-key-status-inactive = Inaktiv

content-registration-grants-title = Zuschuss registrieren
content-registration-new-record-title = Neuer Datensatz

key-manager-title = Schlüsselverwalter
