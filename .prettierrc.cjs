module.exports = {
  editorconfig: true, // map ".editorconfig" to prettier options
  singleQuote: true,
};
