/* tslint:disable */
/* eslint-disable */
/**
 * Crossref resource management API
 * This API will allow to manage Crossref resources within the keycloak framework
 *
 * The version of the OpenAPI document: 1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  APIKey,
  APIKeyResponse,
  ErrorResponse,
  Organization,
  User,
} from '../models';
import {
    APIKeyFromJSON,
    APIKeyToJSON,
    APIKeyResponseFromJSON,
    APIKeyResponseToJSON,
    ErrorResponseFromJSON,
    ErrorResponseToJSON,
    OrganizationFromJSON,
    OrganizationToJSON,
    UserFromJSON,
    UserToJSON,
} from '../models';

export interface AddAPIKeyRequest {
    memberId: number;
    origin?: string;
    aPIKey?: APIKey;
}

export interface AddOrgRequest {
    origin?: string;
    organization?: Organization;
}

export interface AddOrgSubscriptionRequest {
    memberId: number;
    subscription: AddOrgSubscriptionSubscriptionEnum;
    origin?: string;
}

export interface AddUserRequest {
    role?: AddUserRoleEnum;
    origin?: string;
    user?: User;
}

export interface AddUserToOrgRequest {
    memberId: number;
    userId: string;
    origin?: string;
}

export interface DeleteAPIKeyRequest {
    keyId: string;
    origin?: string;
}

export interface DeleteUserRequest {
    userId: string;
    origin?: string;
}

export interface DisableAPIKeyRequest {
    keyId: string;
    origin?: string;
}

export interface EnableAPIKeyRequest {
    keyId: string;
    origin?: string;
}

export interface FindOrgsRequest {
    name?: string;
    page?: number;
    pagesize?: number;
    origin?: string;
}

export interface GetOrgRequest {
    memberId: number;
    origin?: string;
}

export interface GetOrgSubscriptionRequest {
    memberId: number;
    origin?: string;
}

export interface GetUsersRequest {
    email?: string;
    page?: number;
    pagesize?: number;
    origin?: string;
}

export interface ListAPIKeysRequest {
    memberId: number;
    page?: number;
    pagesize?: number;
    origin?: string;
}

export interface ListUserOrgsRequest {
    userId: string;
    name?: string;
    page?: number;
    pagesize?: number;
    origin?: string;
}

export interface ListUsersInOrgRequest {
    memberId: number;
    page?: number;
    pagesize?: number;
    origin?: string;
}

export interface RemoveOrgSubscriptionRequest {
    memberId: number;
    subscription: RemoveOrgSubscriptionSubscriptionEnum;
    origin?: string;
}

export interface RemoveUserFromOrgRequest {
    memberId: number;
    userId: string;
    origin?: string;
}

export interface UpdateAPIKeyRequest {
    origin?: string;
    aPIKey?: APIKey;
}

export interface UpdateOrgRequest {
    origin?: string;
    organization?: Organization;
}

/**
 * 
 */
export class DefaultApi extends runtime.BaseAPI {

    /**
     * Creates an API Key
     * Create an API Key
     */
    async addAPIKeyRaw(requestParameters: AddAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<APIKeyResponse>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling addAPIKey.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/key`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: APIKeyToJSON(requestParameters.aPIKey),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => APIKeyResponseFromJSON(jsonValue));
    }

    /**
     * Creates an API Key
     * Create an API Key
     */
    async addAPIKey(requestParameters: AddAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<APIKeyResponse> {
        const response = await this.addAPIKeyRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Creates an organization
     * Create an Organization
     */
    async addOrgRaw(requestParameters: AddOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Organization>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: OrganizationToJSON(requestParameters.organization),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => OrganizationFromJSON(jsonValue));
    }

    /**
     * Creates an organization
     * Create an Organization
     */
    async addOrg(requestParameters: AddOrgRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Organization> {
        const response = await this.addOrgRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * adds a subscription to an Org
     * Add subscription to an org
     */
    async addOrgSubscriptionRaw(requestParameters: AddOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling addOrgSubscription.');
        }

        if (requestParameters.subscription === null || requestParameters.subscription === undefined) {
            throw new runtime.RequiredError('subscription','Required parameter requestParameters.subscription was null or undefined when calling addOrgSubscription.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/subscription/{subscription}`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))).replace(`{${"subscription"}}`, encodeURIComponent(String(requestParameters.subscription))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * adds a subscription to an Org
     * Add subscription to an org
     */
    async addOrgSubscription(requestParameters: AddOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.addOrgSubscriptionRaw(requestParameters, initOverrides);
    }

    /**
     * Creates a normal user
     * Create a User
     */
    async addUserRaw(requestParameters: AddUserRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<User>> {
        const queryParameters: any = {};

        if (requestParameters.role !== undefined) {
            queryParameters['role'] = requestParameters.role;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/user`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: UserToJSON(requestParameters.user),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => UserFromJSON(jsonValue));
    }

    /**
     * Creates a normal user
     * Create a User
     */
    async addUser(requestParameters: AddUserRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<User> {
        const response = await this.addUserRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Adds a normal user to an organization
     * Add user to organization
     */
    async addUserToOrgRaw(requestParameters: AddUserToOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling addUserToOrg.');
        }

        if (requestParameters.userId === null || requestParameters.userId === undefined) {
            throw new runtime.RequiredError('userId','Required parameter requestParameters.userId was null or undefined when calling addUserToOrg.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/user/{userId}`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))).replace(`{${"userId"}}`, encodeURIComponent(String(requestParameters.userId))),
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Adds a normal user to an organization
     * Add user to organization
     */
    async addUserToOrg(requestParameters: AddUserToOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.addUserToOrgRaw(requestParameters, initOverrides);
    }

    /**
     * Deletes API Key
     * Delete API Key
     */
    async deleteAPIKeyRaw(requestParameters: DeleteAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.keyId === null || requestParameters.keyId === undefined) {
            throw new runtime.RequiredError('keyId','Required parameter requestParameters.keyId was null or undefined when calling deleteAPIKey.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/key/{keyId}`.replace(`{${"keyId"}}`, encodeURIComponent(String(requestParameters.keyId))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Deletes API Key
     * Delete API Key
     */
    async deleteAPIKey(requestParameters: DeleteAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.deleteAPIKeyRaw(requestParameters, initOverrides);
    }

    /**
     * Deletes a user
     * Delete a User
     */
    async deleteUserRaw(requestParameters: DeleteUserRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.userId === null || requestParameters.userId === undefined) {
            throw new runtime.RequiredError('userId','Required parameter requestParameters.userId was null or undefined when calling deleteUser.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/user/{userId}`.replace(`{${"userId"}}`, encodeURIComponent(String(requestParameters.userId))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Deletes a user
     * Delete a User
     */
    async deleteUser(requestParameters: DeleteUserRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.deleteUserRaw(requestParameters, initOverrides);
    }

    /**
     * Disables API Key
     * Disable API Key
     */
    async disableAPIKeyRaw(requestParameters: DisableAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.keyId === null || requestParameters.keyId === undefined) {
            throw new runtime.RequiredError('keyId','Required parameter requestParameters.keyId was null or undefined when calling disableAPIKey.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/key/{keyId}/disable`.replace(`{${"keyId"}}`, encodeURIComponent(String(requestParameters.keyId))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Disables API Key
     * Disable API Key
     */
    async disableAPIKey(requestParameters: DisableAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.disableAPIKeyRaw(requestParameters, initOverrides);
    }

    /**
     * Enables API Key
     * Enable API Key
     */
    async enableAPIKeyRaw(requestParameters: EnableAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.keyId === null || requestParameters.keyId === undefined) {
            throw new runtime.RequiredError('keyId','Required parameter requestParameters.keyId was null or undefined when calling enableAPIKey.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/key/{keyId}/enable`.replace(`{${"keyId"}}`, encodeURIComponent(String(requestParameters.keyId))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Enables API Key
     * Enable API Key
     */
    async enableAPIKey(requestParameters: EnableAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.enableAPIKeyRaw(requestParameters, initOverrides);
    }

    /**
     * Returns all the organizations. If the parameter *name* is provided, results will be filtered by name (containing *name* as a substring).
     * Get organizations
     */
    async findOrgsRaw(requestParameters: FindOrgsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<Organization>>> {
        const queryParameters: any = {};

        if (requestParameters.name !== undefined) {
            queryParameters['name'] = requestParameters.name;
        }

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        if (requestParameters.pagesize !== undefined) {
            queryParameters['pagesize'] = requestParameters.pagesize;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(OrganizationFromJSON));
    }

    /**
     * Returns all the organizations. If the parameter *name* is provided, results will be filtered by name (containing *name* as a substring).
     * Get organizations
     */
    async findOrgs(requestParameters: FindOrgsRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<Organization>> {
        const response = await this.findOrgsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Returns the organization with matching the member ID provided
     * Get organization by member ID
     */
    async getOrgRaw(requestParameters: GetOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Organization>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling getOrg.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => OrganizationFromJSON(jsonValue));
    }

    /**
     * Returns the organization with matching the member ID provided
     * Get organization by member ID
     */
    async getOrg(requestParameters: GetOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Organization> {
        const response = await this.getOrgRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Gets organization subscriptions
     * Get org subscriptions
     */
    async getOrgSubscriptionRaw(requestParameters: GetOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<string>>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling getOrgSubscription.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/subscription`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse<any>(response);
    }

    /**
     * Gets organization subscriptions
     * Get org subscriptions
     */
    async getOrgSubscription(requestParameters: GetOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<string>> {
        const response = await this.getOrgSubscriptionRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Gets all list of users
     * Get users
     */
    async getUsersRaw(requestParameters: GetUsersRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<User>>> {
        const queryParameters: any = {};

        if (requestParameters.email !== undefined) {
            queryParameters['email'] = requestParameters.email;
        }

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        if (requestParameters.pagesize !== undefined) {
            queryParameters['pagesize'] = requestParameters.pagesize;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/user`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(UserFromJSON));
    }

    /**
     * Gets all list of users
     * Get users
     */
    async getUsers(requestParameters: GetUsersRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<User>> {
        const response = await this.getUsersRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Lists API Key for a given organization
     * List API Keys
     */
    async listAPIKeysRaw(requestParameters: ListAPIKeysRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<APIKey>>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling listAPIKeys.');
        }

        const queryParameters: any = {};

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        if (requestParameters.pagesize !== undefined) {
            queryParameters['pagesize'] = requestParameters.pagesize;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/key`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(APIKeyFromJSON));
    }

    /**
     * Lists API Key for a given organization
     * List API Keys
     */
    async listAPIKeys(requestParameters: ListAPIKeysRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<APIKey>> {
        const response = await this.listAPIKeysRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Lists organizations a given user belongs to
     * List user organizations
     */
    async listUserOrgsRaw(requestParameters: ListUserOrgsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<Organization>>> {
        if (requestParameters.userId === null || requestParameters.userId === undefined) {
            throw new runtime.RequiredError('userId','Required parameter requestParameters.userId was null or undefined when calling listUserOrgs.');
        }

        const queryParameters: any = {};

        if (requestParameters.name !== undefined) {
            queryParameters['name'] = requestParameters.name;
        }

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        if (requestParameters.pagesize !== undefined) {
            queryParameters['pagesize'] = requestParameters.pagesize;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/user/{userId}/organizations`.replace(`{${"userId"}}`, encodeURIComponent(String(requestParameters.userId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(OrganizationFromJSON));
    }

    /**
     * Lists organizations a given user belongs to
     * List user organizations
     */
    async listUserOrgs(requestParameters: ListUserOrgsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<Organization>> {
        const response = await this.listUserOrgsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * List users within an organization
     * List organization users
     */
    async listUsersInOrgRaw(requestParameters: ListUsersInOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Array<User>>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling listUsersInOrg.');
        }

        const queryParameters: any = {};

        if (requestParameters.page !== undefined) {
            queryParameters['page'] = requestParameters.page;
        }

        if (requestParameters.pagesize !== undefined) {
            queryParameters['pagesize'] = requestParameters.pagesize;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/users`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(UserFromJSON));
    }

    /**
     * List users within an organization
     * List organization users
     */
    async listUsersInOrg(requestParameters: ListUsersInOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Array<User>> {
        const response = await this.listUsersInOrgRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     * Removes an Org subscription
     * Remove Org subscription
     */
    async removeOrgSubscriptionRaw(requestParameters: RemoveOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling removeOrgSubscription.');
        }

        if (requestParameters.subscription === null || requestParameters.subscription === undefined) {
            throw new runtime.RequiredError('subscription','Required parameter requestParameters.subscription was null or undefined when calling removeOrgSubscription.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/subscription/{subscription}`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))).replace(`{${"subscription"}}`, encodeURIComponent(String(requestParameters.subscription))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Removes an Org subscription
     * Remove Org subscription
     */
    async removeOrgSubscription(requestParameters: RemoveOrgSubscriptionRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.removeOrgSubscriptionRaw(requestParameters, initOverrides);
    }

    /**
     * removes a normal user from an organization
     * Remove user from organization
     */
    async removeUserFromOrgRaw(requestParameters: RemoveUserFromOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters.memberId === null || requestParameters.memberId === undefined) {
            throw new runtime.RequiredError('memberId','Required parameter requestParameters.memberId was null or undefined when calling removeUserFromOrg.');
        }

        if (requestParameters.userId === null || requestParameters.userId === undefined) {
            throw new runtime.RequiredError('userId','Required parameter requestParameters.userId was null or undefined when calling removeUserFromOrg.');
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization/{memberId}/user/{userId}`.replace(`{${"memberId"}}`, encodeURIComponent(String(requestParameters.memberId))).replace(`{${"userId"}}`, encodeURIComponent(String(requestParameters.userId))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * removes a normal user from an organization
     * Remove user from organization
     */
    async removeUserFromOrg(requestParameters: RemoveUserFromOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.removeUserFromOrgRaw(requestParameters, initOverrides);
    }

    /**
     * Updates API Key, only description can be updated
     * Update API Key
     */
    async updateAPIKeyRaw(requestParameters: UpdateAPIKeyRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/key`,
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: APIKeyToJSON(requestParameters.aPIKey),
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     * Updates API Key, only description can be updated
     * Update API Key
     */
    async updateAPIKey(requestParameters: UpdateAPIKeyRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.updateAPIKeyRaw(requestParameters, initOverrides);
    }

    /**
     * Creates or updates an organization
     * Update an Organization
     */
    async updateOrgRaw(requestParameters: UpdateOrgRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<Organization>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        if (requestParameters.origin !== undefined && requestParameters.origin !== null) {
            headerParameters['origin'] = String(requestParameters.origin);
        }

        const response = await this.request({
            path: `/organization`,
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: OrganizationToJSON(requestParameters.organization),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => OrganizationFromJSON(jsonValue));
    }

    /**
     * Creates or updates an organization
     * Update an Organization
     */
    async updateOrg(requestParameters: UpdateOrgRequest = {}, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<Organization> {
        const response = await this.updateOrgRaw(requestParameters, initOverrides);
        return await response.value();
    }

}

/**
 * @export
 */
export const AddOrgSubscriptionSubscriptionEnum = {
    MetadataPlus: 'METADATA_PLUS'
} as const;
export type AddOrgSubscriptionSubscriptionEnum = typeof AddOrgSubscriptionSubscriptionEnum[keyof typeof AddOrgSubscriptionSubscriptionEnum];
/**
 * @export
 */
export const AddUserRoleEnum = {
    Staff: 'STAFF',
    User: 'USER',
    Apikey: 'APIKEY',
    Admin: 'ADMIN',
    Pwdsync: 'PWDSYNC'
} as const;
export type AddUserRoleEnum = typeof AddUserRoleEnum[keyof typeof AddUserRoleEnum];
/**
 * @export
 */
export const RemoveOrgSubscriptionSubscriptionEnum = {
    MetadataPlus: 'METADATA_PLUS'
} as const;
export type RemoveOrgSubscriptionSubscriptionEnum = typeof RemoveOrgSubscriptionSubscriptionEnum[keyof typeof RemoveOrgSubscriptionSubscriptionEnum];
