import { setActivePinia, createPinia } from 'pinia';
import { describe, it, expect, beforeEach } from 'vitest';
import { useUserStore } from '@/stores/user';
import { AuthenticatorLoggedInUser } from '@/composable/useAuth';
import jwt from 'jsonwebtoken';

// Mock user data based on AuthenticatorLoggedInUser type
const mockUser: AuthenticatorLoggedInUser = {
  username: 'testuser',
  usr: 'test_usr',
  role: 'admin',
  token: 'valid-jwt-token',
  refreshToken: 'valid-refresh-token',
};

// Secret for signing JWTs (can be any string)
const JWT_SECRET = 'a_very_secret_key';

// Generate a JWT token with a customizable expiration time
function createJwtToken(expirationInSeconds: number): string {
  return jwt.sign(
    { exp: Math.floor(Date.now() / 1000) + expirationInSeconds },
    JWT_SECRET
  );
}

describe('useUserStore', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it('logs out the user by clearing the user object', () => {
    const store = useUserStore();
    store.user = mockUser;
    store.logoutUser();
    expect(store.user).toBeNull();
  });

  it('validates a valid JWT token', () => {
    const store = useUserStore();
    const validToken = createJwtToken(60); // Token expires in 60 seconds
    const isValid = store.isTokenValid(validToken);
    expect(isValid).toBe(true);
  });

  it('invalidates an expired JWT token', () => {
    const store = useUserStore();
    const expiredToken = createJwtToken(-60); // Token expired 60 seconds ago
    const isValid = store.isTokenValid(expiredToken);
    expect(isValid).toBe(false);
  });

  it('invalidates a token with an invalid payload', () => {
    const store = useUserStore();
    const invalidToken = 'invalid-token';
    const isValid = store.isTokenValid(invalidToken);
    expect(isValid).toBe(false);
  });

  it('retrieves a valid user if the token is valid', () => {
    const store = useUserStore();
    const validToken = createJwtToken(60); // Valid token
    store.user = { ...mockUser, token: validToken };

    const user = store.getUserIfValid();
    expect(user).toEqual(store.user);
  });

  it('logs out and returns null if the token is invalid', () => {
    const store = useUserStore();
    const expiredToken = createJwtToken(-60); // Expired token
    store.user = { ...mockUser, token: expiredToken };

    const user = store.getUserIfValid();
    expect(user).toBeNull();
    expect(store.user).toBeNull(); // User should be logged out
  });

  it('retrieves the valid token', () => {
    const store = useUserStore();
    const validToken = createJwtToken(60); // Valid token
    store.user = { ...mockUser, token: validToken };

    const token = store.getValidToken();
    expect(token).toBe(validToken);
  });

  it('returns null for an invalid token', () => {
    const store = useUserStore();
    const expiredToken = createJwtToken(-60); // Expired token
    store.user = { ...mockUser, token: expiredToken };

    const token = store.getValidToken();
    expect(token).toBeNull();
  });

  it('invalidates a valid JWT token that is about to expire within the safety margin', () => {
    const store = useUserStore();

    // Token that expires in 15 seconds, with a safety margin of 30 seconds
    const tokenExpiringSoon = createJwtToken(15);
    const safetyMarginSeconds = 30;

    const isValid = store.isTokenValid(tokenExpiringSoon, safetyMarginSeconds);

    // Expect the token to be invalid because it's within the safety margin
    expect(isValid).toBe(false);
  });

  it('validates a JWT token that expires after the safety margin', () => {
    const store = useUserStore();

    // Token that expires in 45 seconds, with a safety margin of 30 seconds
    const tokenValidForNow = createJwtToken(45);
    const safetyMarginSeconds = 30;

    const isValid = store.isTokenValid(tokenValidForNow, safetyMarginSeconds);

    // Expect the token to be valid because it's outside the safety margin
    expect(isValid).toBe(true);
  });
});
