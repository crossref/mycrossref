import { setActivePinia, createPinia } from 'pinia';
import { describe, it, expect, beforeEach } from 'vitest';
import { useRecordStore } from '@/stores/records';
import { ContentRegistrationFormPayload } from '@/common/types';
import { DepositResponse } from '@/utils/deposit';

const TEST_RECORD_ID = 'ca069826-e09f-4faf-bc1e-fba7041283b0';
const MOCK_DEPOSIT_RESPONSE: DepositResponse = {
  statusCode: 200,
  status: 'success',
  submissionId: 12345,
  deferredCitationsDiagnosticId: 98766543210,
  doi: '10.1234/example.doi',
  recordCount: 1,
  warningCount: 0,
  failureCount: 0,
  successCount: 1,
  message: 'Deposit completed successfully',
  body: 'Response body content',
};

describe('useRecordStore', () => {
  let store: ReturnType<typeof useRecordStore>;

  const mockFormPayload: ContentRegistrationFormPayload = {
    formData: { field1: 'value1', field2: 'value2' },
    fileName: 'testFile',
    recordType: 'journal-article',
    recordName: 'Test Record',
    recordId: TEST_RECORD_ID,
    recordExtension: 'json',
    schemaName: 'testSchema',
    schemaVersion: 'v1',
  };

  beforeEach(() => {
    setActivePinia(createPinia());
    store = useRecordStore();
    store.$patch({ drafts: new Map() }); // Reset the draft map
  });

  it('initializes with empty draft and completed maps', () => {
    expect(store.drafts.size).toBe(0);
    expect(store.completed.size).toBe(0);
  });

  it('saves record to draft', () => {
    store.saveDraft(mockFormPayload);
    expect(store.drafts.get(TEST_RECORD_ID)).toEqual(mockFormPayload);
  });

  it('moves record to completed using moveRecordToCompleted', () => {
    store.saveDraft(mockFormPayload);
    store.moveRecordToCompleted(mockFormPayload, MOCK_DEPOSIT_RESPONSE);

    expect(store.completed.has(TEST_RECORD_ID)).toBe(true);
    const completedRecord = store.completed.get(TEST_RECORD_ID);
    expect(completedRecord).toBeDefined();
    expect(completedRecord?.depositedAt).toBeInstanceOf(Date);
    expect(completedRecord?.depositResponse).toEqual(MOCK_DEPOSIT_RESPONSE);
    expect(store.drafts.has(TEST_RECORD_ID)).toBe(false);
  });

  it('moves record to completed without existing draft', () => {
    store.moveRecordToCompleted(mockFormPayload, MOCK_DEPOSIT_RESPONSE);

    expect(store.completed.has(TEST_RECORD_ID)).toBe(true);
    const completedRecord = store.completed.get(TEST_RECORD_ID);
    expect(completedRecord).toBeDefined();
    expect(completedRecord?.depositedAt).toBeInstanceOf(Date);
    expect(completedRecord?.depositResponse).toEqual(MOCK_DEPOSIT_RESPONSE);
    expect(store.drafts.has(TEST_RECORD_ID)).toBe(false);
  });
});
