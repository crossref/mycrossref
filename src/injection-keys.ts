/**
 * `injection-keys.ts` acts as a central registry for Symbols used as injection keys within the application.
 * These symbols serve as unique identifiers for dependency injection, ensuring that the injected values are
 * correctly typed and preventing naming collisions.
 */

import { InjectionKey, Ref } from 'vue';

/**
 * Symbol for enabling or disabling error filtering before a form control has been touched.
 * This key is used to inject a global configuration value that determines whether form errors
 * should be filtered and displayed only after the user has interacted with the form.
 */
export const enableFilterErrorsBeforeTouchKey: InjectionKey<Ref<boolean>> =
  Symbol('enableFilterErrorsBeforeTouch');
