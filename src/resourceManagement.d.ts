import { DefaultApi } from './generated/openapi';

declare module 'vue/types/vue' {
  interface Vue {
    $resourceManagementApi: DefaultApi;
  }
}
