export declare type Translator = {
  (id: string, defaultMessage?: string, values?: any): string;
  (id: string, defaultMessage?: undefined, values?: any): string | undefined;
};

declare module 'vue/types/vue' {
  interface Vue {
    $t: Translator;
  }
}
