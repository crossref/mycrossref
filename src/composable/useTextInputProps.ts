import { computed } from 'vue';
import { useVuetifyControl } from '@pvale/vue-vuetify';
import isArray from 'lodash/isArray';
import every from 'lodash/every';
import isString from 'lodash/isString';
import {useInfoText} from "@/forms/compositions";

// Extract the type of the entire return value of useVuetifyControl
type UseVuetifyControlReturn = ReturnType<typeof useVuetifyControl>;

type UseInfoTextReturn = ReturnType<typeof useInfoText>;

export const useTextInputProps = (vControl: UseVuetifyControlReturn, infoText: UseInfoTextReturn, localValue: any) => {
  const suggestions = computed(() => {
    const sugg = vControl.control.value.uischema.options?.suggestion;
    if (isArray(sugg) && every(sugg, isString)) {
      return sugg;
    }
    return undefined;
  });

  const textInputProps = computed(() => ({
    controlId: vControl.control.value.id,
    inputClass: vControl.styles?.control?.input,
    disabled: !vControl.control.value.enabled,
    autofocus: vControl.appliedOptions.value.focus,
    placeholder: vControl.appliedOptions.value.placeholder,
    label: vControl.computedLabel.value,
    hint: infoText.msg.value ? '' : vControl.control.value.description?.value,
    persistentHint: vControl.persistentHint(),
    required: vControl.control.value.required,
    errorMessages: vControl.filteredErrors.value,
    modelValue: localValue.value,
    maxLength: vControl.appliedOptions.value.restrict ? vControl.control.value.schema.maxLength : undefined,
    counter: vControl.control.value.schema.maxLength !== undefined ? vControl.control.value.schema.maxLength : undefined,
    suggestions: suggestions.value,
    vuetifyProps: vControl.vuetifyProps,
  }));

  return {
    textInputProps,
  };
};
