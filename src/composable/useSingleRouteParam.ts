import { computed } from 'vue';
import { useRoute } from 'vue-router';

/**
 * A composable to get a single, string-converted route parameter.
 * If the parameter is an array, it returns the first element; otherwise, it returns the parameter itself.
 * @param {string} paramName - The name of the route parameter to extract.
 * @returns {ComputedRef<string>} The single value of the route parameter, always as a string.
 */
export function useSingleRouteParam(paramName) {
  const route = useRoute();
  return computed(() => {
    const param = route.params[paramName];
    return Array.isArray(param) ? param[0] : param;
  });
}
