import { useElementBounding } from '@vueuse/core'

export const useScrollToFirstError = (headerRef) => {
  const scrollToFirstError = () => {
    // Use a fallback value for height if headerRef is not properly defined
    const headerHeight = headerRef ? useElementBounding(headerRef).height.value : 0;
    const errorElement = document.querySelector('.custom-json-forms [errors]:not([errors=""])');

    if (errorElement) {
      const elementTop = errorElement.getBoundingClientRect().top;
      const scrollOffset = window.scrollY;
      const elementPosition = elementTop + scrollOffset - headerHeight;
      window.scrollTo({ top: elementPosition, behavior: 'smooth' });
    }
  };

  return { scrollToFirstError };
};
