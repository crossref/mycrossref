import { RouteNames } from '@/router';
import { useRouter } from 'vue-router';

export const useRouteResolver = () => {
  const router = useRouter();
  const getCDSAuthCancelRoute = () => {
    const previousURL = router.options.history.state.back;
    if (typeof previousURL === 'string') {
      const previousRoute = router.resolve(previousURL);
      if (
        previousRoute.name !== RouteNames.notFound &&
        !previousRoute.meta.requiresCDSAuth
      ) {
        return previousRoute;
      }
    }
    return router.resolve({ name: RouteNames.index });
  };

  const getRedirectRoute = () => {
    const queryPath = router.currentRoute.value.query?.redirect;
    if (typeof queryPath === 'string') {
      const queryRoute = router.resolve(queryPath);
      return queryRoute;
    }

    return router.resolve({ name: RouteNames.index });
  };

  return { getCDSAuthCancelRoute, getRedirectRoute };
};
