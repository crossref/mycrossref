import { env } from '@/env';
import {
  Configuration,
  ConfigurationParameters,
  DefaultApi,
} from '@/generated/openapi';

export const KEYMANAGER_ORGANIZATION_QUERY_ENDPOINT = `${
  env().keymakerAPIBaseURL
}/realms/crossref/crossref-management`;

const wrapInArray = (value: unknown): any[] =>
  Array.isArray(value) ? value : [value];

export const getApiClient = (middleware: any) => {
  const configParams: ConfigurationParameters = {
    basePath: KEYMANAGER_ORGANIZATION_QUERY_ENDPOINT,
    middleware: wrapInArray(middleware),
  };
  const apiConfig = new Configuration(configParams);

  return new DefaultApi(apiConfig);
};

export type ApiClient = typeof DefaultApi;
