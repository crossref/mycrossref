export const ROR_ORGANIZATION_QUERY_ENDPOINT =
  'https://api.ror.org/organizations';

interface RawOrganizationQueryResponse {
  number_of_results?: number;
  items: Organization[];
}

const isOrganizationQueryResponse = (
  obj: unknown
): obj is RawOrganizationQueryResponse => {
  if (!!obj && typeof obj === 'object' && !Array.isArray(obj)) {
    return (
      (!('number_of_results' in obj) ||
        typeof obj.number_of_results === 'number') &&
      'items' in obj &&
      typeof obj.items === 'object' &&
      Array.isArray(obj.items) &&
      obj.items.every(isOrganization)
    );
  }
  return false;
};

interface Label {
  iso639: string;
  label: string;
}
const isLabel = (obj: unknown): obj is Label => {
  return (
    typeof obj === 'object' &&
    !!obj &&
    'iso639' in obj &&
    typeof obj.iso639 === 'string' &&
    'label' in obj &&
    typeof obj.label === 'string'
  );
};

interface Country {
  country_code: string;
  country_name: string;
}
const isCountry = (obj: unknown): obj is Country => {
  return (
    typeof obj === 'object' &&
    !!obj &&
    'country_code' in obj &&
    typeof obj.country_code === 'string' &&
    'country_name' in obj &&
    typeof obj.country_name === 'string'
  );
};

export interface Organization {
  id: string;
  name: string;
  labels: Label[];
  country: Country;
  acronyms: string[];
  // The actual organization has many more properties!
}

const isOrganization = (obj: unknown): obj is Organization => {
  return (
    typeof obj === 'object' &&
    !!obj &&
    'id' in obj &&
    typeof obj.id === 'string' &&
    'name' in obj &&
    typeof obj.name === 'string' &&
    'labels' in obj &&
    Array.isArray(obj.labels) &&
    obj.labels.every(isLabel) &&
    'country' in obj &&
    isCountry(obj.country) &&
    'acronyms' in obj &&
    Array.isArray(obj.acronyms) &&
    obj.acronyms.every((acr) => typeof acr === 'string')
  );
};

export interface OrganizationQueryResponse {
  type: 'success';
  numberOfResults?: number;
  organizations: Organization[];
}

export interface QueryError {
  type: 'error';
  message: string;
  data: any;
}

export interface Abort {
  type: 'abort';
}

export const queryOrganizations = async (
  search?: string,
  abortSignal?: AbortSignal
): Promise<OrganizationQueryResponse | QueryError | Abort> => {
  // ror endpoint throws errors for some characters, so filter them here
  // (, ), ", /, \, [, ], {, }, :
  const url = search
    ? `${ROR_ORGANIZATION_QUERY_ENDPOINT}?${new URLSearchParams({
        query: search.replaceAll(/(\(|\)|"|\/|\\|\[|\]|{|}|:)/g, ''),
      })}`
    : ROR_ORGANIZATION_QUERY_ENDPOINT;
  try {
    const response = await fetch(url, { signal: abortSignal });
    if (!response.ok) {
      return {
        type: 'error',
        message: 'Received response indicates error',
        data: response,
      };
    }
    const responseJson = await response.json();
    if (isOrganizationQueryResponse(responseJson)) {
      return {
        type: 'success',
        numberOfResults: responseJson.number_of_results,
        organizations: responseJson.items,
      };
    }
    return {
      type: 'error',
      message: 'Received response does not look like expected',
      data: responseJson,
    };
  } catch (e: unknown) {
    if (typeof e === 'object' && e && 'name' in e && e.name === 'AbortError') {
      return {
        type: 'abort',
      };
    }
    return {
      type: 'error',
      message: 'A error occurred while performing the ror organization query',
      data: e,
    };
  }
};
