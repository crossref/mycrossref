import { Middleware, ResponseContext } from '@/generated/openapi';
import { useAuth } from '@/composable/useAuth';

export default class UnauthorizedMiddleware implements Middleware {
  public post?(context: ResponseContext): Promise<Response | void> {
    const { login } = useAuth();
    if (context.response.status === 401) {
      login();
    }
    return Promise.resolve(context.response);
  }
}
