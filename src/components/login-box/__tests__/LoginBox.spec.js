// LoginForm.spec.js

// Components
import LoginBox from '@/components/login-box/LoginBox';
import BaseInputUsername from '@/components/base/BaseInputUsername';
import BaseInputPassword from '@/components/base/BaseInputPassword';

// Utilities
import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';

describe('LoginBox.vue', () => {
  const mountFunction = (options) => {
    const wrapper = mount(LoginBox, {
      global: {
        plugins: [vuetifyInstance],
      },
      props: { title: 'Foobar' },
      ...options,
    });
    return wrapper;
  };

  it('should work', () => {
    const wrapper = mountFunction();

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should have a username input', () => {
    const wrapper = mountFunction();
    const usernameComponent = wrapper.findAllComponents(BaseInputUsername);
    expect(usernameComponent).toHaveLength(1);
  });

  it('should have a password input', () => {
    const wrapper = mountFunction();
    const usernameComponent = wrapper.findAllComponents(BaseInputPassword);
    expect(usernameComponent).toHaveLength(1);
  });
});
