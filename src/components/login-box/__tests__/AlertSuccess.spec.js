// AlertSuccess
import AlertSuccess from '@/components/login-box/AlertSuccess';

import { describe, it, expect, beforeEach } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';

// Utilities
import { mount } from '@vue/test-utils';

describe('AlertSuccess', () => {
  function mountFunction(options = {}) {
    const wrapper = mount(AlertSuccess, {
      global: {
        plugins: [vuetifyInstance],
      },
      props: { title: 'Foobar' },
      ...options,
    });
    return wrapper;
  }

  it('should work', () => {
    const wrapper = mountFunction({
      propsData: {
        message: 'Hello world',
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('displays message', () => {
    const wrapper = mountFunction({
      propsData: {
        message: 'Hello world',
      },
    });

    expect(wrapper.text()).toEqual('Hello world');
  });
});
