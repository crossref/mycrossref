import { mount } from '@vue/test-utils';
import { vi } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';
import BaseInputPassword from '@/components/base/BaseInputPassword';

// // eslint-disable-next-line no-unused-vars
// const mockedRootParent = {
//   data() {
//     return {
//       config: {
//         authApiBaseUrl: 'doi.crossref.org',
//       },
//     };
//   },
// };

describe('LoginBox.vue', () => {
  const mountFunction = () => {
    return mount(BaseInputPassword, {
      global: {
        plugins: [vuetifyInstance],
      },
    });
  };

  it('should work', () => {
    const wrapper = mountFunction({
      props: {
        modelValue: '',
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });
});
