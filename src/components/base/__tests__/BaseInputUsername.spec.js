// LoginForm.spec.js

// Libraries
import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import vuetifyInstance from '@/plugins/vuetify';

// Components
import BaseInputUsername from '@/components/base/BaseInputUsername';
import VMessages from 'vuetify';

// eslint-disable-next-line no-unused-vars
const mockedRootParent = {
  data() {
    return {
      config: {
        authApiBaseUrl: 'doi.crossref.org',
      },
    };
  },
};

describe('LoginBox.vue', () => {
  const mountFunction = (options) => {
    const wrapper = mount(BaseInputUsername, {
      global: {
        plugins: [vuetifyInstance],
      },
      ...options,
    });
    return wrapper;
  };

  it('should work', () => {
    const wrapper = mountFunction({
      props: {
        modelValue: '',
      },
    });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should display the username', () => {
    const wrapper = mountFunction({
      props: {
        modelValue: 'testuser',
      },
    });

    const inputUsernameComponent = wrapper.findComponent(BaseInputUsername);
    const input = inputUsernameComponent.find('input[name="username"]');
    const value = input.element.value;
    expect(value).toEqual('testuser');
  });
});
