export const EMAIL_SUPPORT = 'support@crossref.org';
export const EMAIL_GENERIC_CONTENT_REGISTRATION_GRANT = 'content-registration-form+grant@crossref.org'
export const EMAIL_GENERIC_CONTENT_REGISTRATION_JOURNAL_ARTICLE = 'content-registration-form+ja@crossref.org'
export const EMAIL_GENERIC_CONTENT_REGISTRATION_UKNOWN_RECORD_TYPE = 'content-registration-form+unknown-type@crossref.org'
export const EMAIL_CONTENT_REGISTRATION_DEPOSITOR_UNKNOWN = 'content-registration-form+unknown-depositor@crossref.org'
export const NAME_CONTENT_REGISTRATION_DEPOSITOR_UNKNOWN = 'Unknown Depositor'
