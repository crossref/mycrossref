export enum ComponentNames {
  ToolbarIcon = 'ToolbarIcon',
  ToolbarItemDownloadRecord = 'ToolbarItemDownloadRecord',
  ToolbarItemRecordRegistrationHelp = 'ToolbarItemRecordRegistrationHelp',
}
