import { useErrorMessageTranslation } from '@/composable/useI18n';
import { isSecureContext } from '@/utils/isSecureContext';
/**
 * Copies the provided text to the clipboard using the Clipboard API.
 * This function requires a secure context (HTTPS, localhost).
 *
 * @param {string} text - The text to be copied to the clipboard.
 * @throws {Error} Will throw an error if the text cannot be copied to the clipboard or the context is non-secure.
 */

const te = useErrorMessageTranslation();

export const clipboardCopy = async (text: string) => {
  // Check if the current context is secure (served over HTTPS or localhost)
  if (isSecureContext()) {
    try {
      // Attempt to use the Clipboard API to write the text to the clipboard
      await navigator.clipboard.writeText(text);
    } catch (error) {
      // If an error occurs, throw a new error with a custom message
      throw new Error('Failed to copy text to clipboard: ' + error);
    }
    return;
  }
  // If the context is non-secure, throw an error informing users that the feature is only available in secure contexts
  throw new Error(
    te(
      'error-no-clipboard-access-in-nonsecure-contexts',
      'Clipboard access is only available in secure contexts (HTTPS, localhost).'
    )
  );
};
