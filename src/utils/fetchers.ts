import axios from 'axios';
import { userProvidedCredentials } from '@/statemachines/auth.machine';
import { Response } from '@/common/types';
import { useApi } from '@/composable/useApi';

export const RESOURCE_MANAGEMENT_API_ROLE_USER = 'USER';
export const RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE = 100;

const { api } = useApi();

export const fetchLoginState = async function (
  creds?: userProvidedCredentials
): Promise<Response> {
  const { data } = await axios.post('/login', creds);
  const loginResponse: Response = data;
  return loginResponse;
};

export const getOrganization = async (memberId: number) => {
  return await api.getOrg({
    memberId: memberId as number,
  });
};

export const fetchUsersInOrganization = async (memberId: number) => {
  return await api.listUsersInOrg({
    memberId: memberId as number,
    page: 1,
    pagesize: RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE,
  });
};
