/**
 * Fetches environment variables that start with a specified prefix,
 * and returns a nested configuration object.
 *
 * The function splits the variable name (excluding the prefix) into parts based on underscores.
 * Each part, except the last one, is used as a key in the nested object.
 * The last part is associated with the variable's value.
 *
 * For example, given a prefix 'TEST_' and an environment variable 'TEST_VARIABLE_ONE_TWO_THREE=10',
 * the function returns the object { VARIABLE: { ONE: { TWO: { THREE: 10 }}}} .
 *
 * @param {string} prefix The prefix of the environment variables to include in the configuration object.
 * @returns {object} The configuration object.
 */
export const fetchConfigFromEnv = (prefix: string) => {
  const config: any = {};

  for (const key in process.env) {
    if (key.startsWith(prefix)) {
      const parts = key.substring(prefix.length).split('_');
      const value = process.env[key] || '';
      parts.reduce((obj: any, part: string, i: number) => {
        if (i === parts.length - 1) {
          // If we are at the last part, set the value
          obj[part] = value;
        } else {
          // Otherwise, if the part doesn't exist in the object yet, create an empty object
          if (!obj[part]) {
            obj[part] = {};
          }
        }
        // Continue the reduce with the current part of the object
        return obj[part];
      }, config);
    }
  }

  return config;
};
