export function isSecureContext() {
  // Check if the window object exists
  if (typeof window !== 'undefined') {
    return window.isSecureContext;
  } else {
    // Return false if the window object is not available
    return false;
  }
}
