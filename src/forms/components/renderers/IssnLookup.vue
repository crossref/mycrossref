<template>
  <v-skeleton-loader type="card" :loading="loading">
    <v-fade-transition v-if="show">
      <v-card
        v-if="!loading && journalTitle"
        class="w-100"
        color=""
        variant="outlined"

      >
        <template #title>{{ t('issn-lookup-match-msg') }}</template>
        <template #subtitle>{{ issn }}</template>
        <template #prepend>
          <v-progress-circular
            v-if="loading"
            indeterminate
            color=""
          ></v-progress-circular>
          <v-icon v-if="!loading" icon="mdi-check" color=""></v-icon>
        </template>
        <template #actions>
          <v-spacer />
          <v-btn
            variant="outlined"
            data-test="dismiss-button"
            @click="dismiss"
            >{{ t('action-no-thanks') }}</v-btn
          >
          <v-btn
            color=""
            variant="outlined"
            data-test="autofill-button"
            @click="autofillJournalDetails"
            >{{ t('action-autofill') }}</v-btn
          >
        </template>
        <v-card-text>
          <div class="flex-container py-1">
            <div class="flex-item-label">{{ t('journalTitle') }}</div>
            <div class="flex-item-content">{{ journalTitle }}</div>
          </div>
          <div class="flex-container py-1">
            <div class="flex-item-label">{{ t('printIssn') }}</div>
            <div class="flex-item-content">
              {{ localizedPrintIssnMatchMsg }}
            </div>
          </div>
          <div class="flex-container py-1">
            <div class="flex-item-label">{{ t('onlineIssn') }}</div>
            <div class="flex-item-content">
              {{ localizedOnlineIssnMatchMsg }}
            </div>
          </div>
          <p class="mt-2">{{ message }}</p>
        </v-card-text>
      </v-card>
      <v-card v-if="lookupFailure && !loading" class="" variant="outlined">
        <template #title>{{ t('issn-lookup-no-match-msg') }}</template>
        <template #subtitle>{{ issn }}</template>
        <template #prepend>
          <v-progress-circular
            v-if="loading"
            indeterminate
            color=""
          ></v-progress-circular>
          <v-icon v-if="!loading" icon="mdi-alert" color=""></v-icon>
        </template>
        <template #actions>
          <v-spacer />
          <v-btn variant="outlined" @click="dismiss">OK</v-btn>
        </template>
        <v-card-text>{{ t('issn-lookup-no-match-description') }}</v-card-text>
      </v-card>
    </v-fade-transition>
  </v-skeleton-loader>
</template>

<script lang="ts">
import {
  defineComponent,
  onMounted,
  watch,
  ref,
  PropType,
  computed,
  toRaw,
  inject,
} from 'vue';
import { CrossrefClient } from '@/plugins/crossref-api-client';
import { useTranslationNamespace } from '@/composable/useI18n';
import { I18nNamespace } from '@/i18n';
import { updateJournalDetails } from '@/forms/transformations/updateJournalDetails';
import {
  Actions,
  CoreActions,
  Dispatch,
  JsonFormsSubStates,
  JsonSchema,
} from '@jsonforms/core';
import { IssnType } from '@/models/Issn';

/**
 * A Vue component for looking up journal information from an ISSN.
 * Uses the Crossref Works API to fetch journal details.
 * Allows users to autofill journal details or dismiss the UI.
 */
export default defineComponent({
  name: 'IssnLookup',
  props: {
    issn: {
      type: String as PropType<string>,
      required: true,
    },
  },
  emits: ['lookup-start', 'lookup-complete'],
  setup(props, { emit }) {
    const t = useTranslationNamespace(I18nNamespace.ContentRegistration);
    const dispatch = inject<Dispatch<CoreActions>>('dispatch');
    const jsonforms = inject<JsonFormsSubStates>('jsonforms');
    const schema = jsonforms?.core?.schema;
    const loading = ref(false);
    const show = ref(true);
    const lookupResult: any = ref(null);
    const lookupFailure = ref(false);
    // We pass an email address in here to use the 'Polite' api pool
    const crossrefClient = new CrossrefClient('frontend+console@crossref.org');

    const journalTitle = computed(() => {
      return lookupResult.value
        ? lookupResult.value?.content?.message?.title
        : null;
    });
    const getIssnByType = (type: IssnType) => {
      // Check if lookupResult is truthy and has the expected structure
      if (
        lookupResult.value &&
        lookupResult.value.content &&
        lookupResult.value.content.message &&
        lookupResult.value.content.message['issnType']
      ) {
        // Find an ISSN by the specified type
        const issnObj = lookupResult.value.content.message['issnType'].find(
          (issn) => issn.type === type
        );
        return issnObj ? issnObj.value : null;
      }
      // If lookupResult does not exist or does not contain the necessary data, return null
      return null;
    };

    // Computed properties for print and online ISSN
    const printISSN = computed(() => getIssnByType(IssnType.Print));
    const onlineISSN = computed(() => getIssnByType(IssnType.Online));
    const title = computed(() => {
      if (journalTitle.value) {
        return t(
          'issnLookup_matched_autoFillOffer_title'
        );
      }
      return t('contentRegistrationForm_issn_issnLookup_title');
    });
    const subTitle = computed(() => {
      return t('contentRegistrationForm_issn_issnLookup_subtitle', {
        issn: props.issn,
        journalTitle: journalTitle.value,
      });
    });
    const journalMatchMessage = computed(() => {
      if (journalTitle.value) {
        return t('issnLookup_matched_autoFillOffer_title', {
          journalTitle: journalTitle.value,
          issn: props.issn,
        });
      }
      return '';
    });
    const localizedJournalTitleMsg = computed(() => {
      return journalTitle.value
        ? t('issnLookup_matched_journal_title_msg', {
            title: journalTitle.value,
          })
        : '';
    });
    const localizedJournalTitleMatchMsg = computed(() => {
      return journalTitle.value ? journalTitle : t('noMatchFound');
    });
    const localizedPrintIssnMatchMsg = computed(() => {
      return printISSN.value ? printISSN.value : t('noMatchFound');
    });
    const localizedOnlineIssnMatchMsg = computed(() => {
      return onlineISSN.value ? onlineISSN.value : t('noMatchFound');
    });
    /**
     * Performs a lookup to fetch journal information from the works API by ISSN.
     * Emits `lookup-start` at the start and `lookup-complete` upon completion.
     * Updates the component state based on the lookup result.
     *
     * @async
     * @param {String} issn - The ISSN number to lookup.
     */
    const performLookup = async (issn: string) => {
      loading.value = true;
      emit('lookup-start');
      lookupFailure.value = false;
      lookupResult.value = null;

      try {
        show.value = true;
        crossrefClient.initializeAbortController();
        const response = await crossrefClient.journal(issn);
        if (response.ok) {
          lookupResult.value = response;
        } else {
          lookupFailure.value = true;
        }
      } catch (error) {
        console.debug('ISSN Lookup Error:', error);
        lookupFailure.value = true;
      } finally {
        loading.value = false;
        emit('lookup-complete');
      }
    };

    /**
     * Autofills the journal details using the result from the last successful lookup.
     * Dispatches an update action to update journal details in the form state.
     */
    const autofillJournalDetails = () => {
      const data = toRaw(jsonforms?.core?.data);
      const updatedData = updateJournalDetails(
        data,
        null,
        lookupResult.value?.content?.message
      );
      if (updatedData) {
        if (dispatch) {
          dispatch(Actions.updateCore(updatedData, schema as JsonSchema));
        }
      }
      dismiss();
    };

    /**
     * Dismisses the lookup result UI
     */
    const dismiss = () => (show.value = false);

    watch(
      () => props.issn,
      (newValue, oldValue) => {
        if (newValue && newValue !== oldValue) {
          performLookup(newValue);
        }
      },
      { immediate: true }
    );

    return {
      t,
      loading,
      show,
      lookupResult,
      lookupFailure,
      journalTitle,
      title,
      subTitle,
      message: journalMatchMessage,
      dismiss,
      autofillJournalDetails,
      schema,
      onlineISSN,
      printISSN,
      localizedJournalTitleMatchMsg,
      localizedOnlineIssnMatchMsg,
      localizedPrintIssnMatchMsg,
    };
  },
});
</script>

<style scoped>
.flex-container {
  display: flex;
}

.flex-item-label {
  flex-basis: 20%;
  font-weight: bold;
}

.flex-item-content {
  flex-grow: 1;
}
</style>
