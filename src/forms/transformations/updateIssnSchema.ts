import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';
import set from 'lodash/set';
import unset from 'lodash/unset';
import { FormData } from '@/forms/types';
import { JsonSchema } from '@jsonforms/core';
import {
  hasAtLeastOnePropertyWithValue,
  hasPropertyWithValue,
} from '@/utils/helpers';
import {
  SchemaTransformationPair,
  SchemaTransformationTester,
  SchemaTransformer,
} from '@/forms/types';

/**
 * Determines if the ISSN values within the form data have changed, necessitating a schema transformation.
 * This matches the `SchemaTransformationTester` signature.
 *
 * @param schema - The current JSON Schema of the form. Unused here but required by the type signature.
 * @param newData - The new form data.
 * @param prevData - The previous form data.
 * @param context - Optional context. Unused here but required by the type signature.
 * @returns A boolean indicating whether a transformation should be applied.
 */
export const hasIssnChanged: SchemaTransformationTester = (
  schema: JsonSchema,
  newData: FormData,
  prevData: FormData = {},
  context?: any
): boolean => {
  return (
    // issn will be empty on a new form load, which also requires transformation
    !hasAtLeastOnePropertyWithValue(newData.journal?.issn) ||
    !isEqual(
      {
        printIssn: newData.journal?.issn?.printIssn,
        onlineIssn: newData.journal?.issn?.onlineIssn,
      },
      {
        printIssn: prevData.journal?.issn?.printIssn,
        onlineIssn: prevData.journal?.issn?.onlineIssn,
      }
    )
  );
};

/**
 * Defines the transformation logic to be applied to the form's JSON Schema, based on ISSN fields.
 * This matches the `SchemaTransformer` signature.
 *
 * @param schema - The current JSON Schema of the form.
 * @param data - The current form data.
 * @returns The transformed JSON Schema.
 */
export const transformIssnFields: SchemaTransformer = (
  schema: JsonSchema,
  data: FormData
): JsonSchema => {
  const schemaCopy = cloneDeep(schema);
  const requiredIssns: string[] = [];
  if (!hasAtLeastOnePropertyWithValue(data.journal.issn)) {
    if (!hasPropertyWithValue(data.journal.issn, 'printIssn')) {
      requiredIssns.push('printIssn');
    }
    if (!hasPropertyWithValue(data.journal.issn, 'onlineIssn')) {
      requiredIssns.push('onlineIssn');
    }
  }

  // Safely remove the 'anyOf' constraint for ISSNs
  unset(schemaCopy, 'properties.journal.properties.issn.anyOf');

  // Safely set the 'required' property for ISSNs
  set(
    schemaCopy,
    'properties.journal.properties.issn.required',
    requiredIssns.length > 0 ? requiredIssns : []
  );

  return schemaCopy;
};

/**
 * An entry for the schema transformation logic, specifying when and how the form schema should be transformed based on ISSN changes.
 */
export const entry: SchemaTransformationPair = {
  tester: hasIssnChanged,
  transform: transformIssnFields,
};

export default entry;
