import cloneDeep from 'lodash/cloneDeep';
import set from 'lodash/set';
import { Journal } from '@/plugins/crossref-api-client';
import { IssnType } from '@/models/Issn';

interface ApiIssnType {
  type: IssnType.Print | IssnType.Online;
  value: string;
}

function isIssnType(issn: unknown): issn is ApiIssnType {
  return (
    (issn as ApiIssnType).type !== undefined &&
    (issn as ApiIssnType).value !== undefined
  );
}

export const updateJournalDetails = (
  newData,
  _prevData,
  context: Partial<Journal>
) => {
  // Create a deep copy of the data object
  const dataCopy = cloneDeep(newData);
  // Reset print and online ISSN first
  set(dataCopy, 'journal.issn.printIssn', undefined);
  set(dataCopy, 'journal.issn.onlineIssn', undefined);

  const printIssnInfo = context.issnType?.find(
    (issn) => isIssnType(issn) && issn.type === IssnType.Print
  );
  const onlineIssnInfo = context.issnType?.find(
    (issn) => isIssnType(issn) && issn.type === IssnType.Online
  );

  if (context.title) {
    set(dataCopy, 'journal.titles.fullTitle', context.title);
  }
  // Since `isIssnType` already checks the structure, we can assert the type directly in the condition
  if (printIssnInfo && isIssnType(printIssnInfo)) {
    set(dataCopy, 'journal.issn.printIssn', printIssnInfo.value);
  }
  if (onlineIssnInfo && isIssnType(onlineIssnInfo)) {
    set(dataCopy, 'journal.issn.onlineIssn', onlineIssnInfo.value);
  }
  return dataCopy;
};

export const entry = {
  // this should never be called as part of an automatic set of tranformations
  tester: () => false,
  transform: updateJournalDetails,
};
