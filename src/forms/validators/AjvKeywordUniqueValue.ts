import type { AnySchemaObject, FuncKeywordDefinition } from "ajv";
import { DataValidationCxt } from "ajv/dist/types";

/**
 * JSON Schema user-defined keyword uniqueValue
 *
 * Applies to object properties within an array.
 * When set on an object prop, it will ensure that that property is unique across all objects within the array.
 */

export default function getDef(): FuncKeywordDefinition {
  return {
    keyword: "uniqueValue", // Name of the custom keyword
    type: "string", // This keyword applies to string type fields
    schemaType: "boolean", // The schema for this keyword should be a boolean
    compile() {
      // Compile function to create the validation function
      return function validate(data: any, dataCxt?: DataValidationCxt): boolean {
        if (!dataCxt) return true; // If no data context is provided, assume valid
        const { instancePath, parentDataProperty, rootData } = dataCxt;

        // Split the instance path to navigate the data structure
        const pathParts = instancePath.split('/');
        pathParts.pop(); // Remove the field name to get the path to the object
        pathParts.pop(); // Remove the index to get the path to the array

        // Retrieve the parent array using the path parts
        const parentArray = pathParts.reduce((acc, curr) => {
          if (curr === '') return acc; // Skip the first empty string from the split of the root path
          return acc[curr];
        }, rootData);

        const seen = new Map(); // Map to track seen values
        if (parentArray && parentArray.length > 1) {
          // Check for duplicates in the parent array
          const isDuplicate = parentArray.some((item) => {
            const fieldValue = item[parentDataProperty]; // Dynamically access the field
            // Only add non-empty string values to the Map
            if (
              fieldValue &&
              typeof fieldValue === 'string' &&
              fieldValue.trim().length > 0
            ) {
              if (seen.has(fieldValue)) {
                return true; // Found a duplicate
              }
              seen.set(fieldValue, true); // Add the valid, non-empty string to the seen Map
            }
            return false;
          });
          if (isDuplicate) return false; // Return false if a duplicate was found
        }
        return true; // Return true if no duplicates were found
      };
    },
    metaSchema: {
      type: "boolean", // Meta-schema indicating that the keyword's schema should be a boolean
    },
  };
}
