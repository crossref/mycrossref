import {
  and,
  isBooleanControl,
  isDateControl,
  isDateTimeControl,
  isEnumControl,
  isIntegerControl,
  isLayout,
  isNumberControl,
  isObjectArrayControl,
  isObjectArrayWithNesting,
  isObjectControl,
  isStringControl,
  JsonFormsRendererRegistryEntry,
  optionIs,
  or,
  rankWith,
  scopeEndsWith,
  Tester,
  uiTypeIs,
  withIncreasedRank,
} from '@jsonforms/core';
import labeledObjectRenderer from '@/components/form-renderers/LabeledObjectRenderer.vue';
import SpacerRenderer from '@/components/form-renderers/SpacerRenderer.vue';
import ArrayRenderer from '@/components/form-renderers/ArrayRenderer.vue';
import RORRenderer from '@/components/form-renderers/ror/RorRenderer.vue';
import PublicationDatesRenderer from '@/forms/components/renderers/PublicationDatesRenderer.vue';
import IssnRenderer from '@/forms/components/renderers/IssnRenderer.vue';
import ContributorRoleRenderer from '@/forms/components/renderers/ContributorRoleRenderer.vue';
import StringControlRenderer from '@/forms/components/renderers/StringControlRenderer.vue';
import EnumControlRenderer from '@/forms/components/renderers/controls/EnumControlRenderer.vue';
import DateControlRenderer from '@/forms/components/renderers/controls/DateControlRenderer.vue';
import DateTimeControlRenderer from '@/forms/components/renderers/controls/DateTimeControlRenderer.vue';
import AutocompleteEnumControlRenderer from '@/forms/components/renderers/AutocompleteEnumControlRenderer.vue';
import ReviewListRenderer from './components/renderers/review/ReviewListRenderer.vue';
import ReviewTextControlRenderer from '@/forms/components/renderers/review/ReviewTextControlRenderer.vue';
import ReviewArrayRenderer from '@/forms/components/renderers/review/ReviewArrayRenderer.vue';
import ReviewVerticalLayoutRenderer from '@/forms/components/renderers/review/ReviewVerticalLayoutRenderer.vue';
import ReviewGroupRenderer from '@/forms/components/renderers/review/ReviewGroupRenderer.vue';
import MultiStringControlRenderer from '@/forms/components/renderers/controls/MultiStringControlRenderer.vue';
import ReferencesRenderer from '@/forms/components/renderers/controls/ReferencesControlRenderer.vue';

import { verticalLayoutRendererEntry as UpstreamVerticalLayoutRendererEntry } from '@pvale/vue-vuetify';
import { groupRendererEntry as UpstreamGroupRendererEntry } from '@pvale/vue-vuetify';
import { multiStringControlRendererEntry as UpstreamMultiStringControlRendererEntry } from '@pvale/vue-vuetify';
import { dateControlRendererEntry as UpstreamDateControlRendererEntry } from '@pvale/vue-vuetify';
import { dateTimeControlRendererEntry as UpstreamDateTimeControlRendererEntry } from '@pvale/vue-vuetify';
import { enumControlRendererEntry as UpstreamEnumControlRendererEntry } from '@pvale/vue-vuetify';
import { stringControlRendererEntry as UpstreamStringControlRendererEntry } from '@pvale/vue-vuetify';
import { autocompleteEnumControlRendererEntry as UpstreamAutocompleteEnumControlRendererEntry } from '@pvale/vue-vuetify';
import LanguageChooserRenderer from '@/forms/components/renderers/LanguageChooserRenderer.vue';
import OrcidRenderer from '@/forms/components/renderers/OrcidRenderer.vue';
import DoiRenderer from '@/forms/components/renderers/DoiRenderer.vue';
import VerticalLayoutRenderer from '@/forms/components/renderers/VerticalLayoutRenderer.vue';
import GroupRenderer from '@/forms/components/renderers/GroupRenderer.vue';

enum CustomRank {
  Highest = 100,
  High = 75,
  AboveAverage = 60,
  Average = 50,
  BelowAverage = 40,
  Low = 25,
}

/**
 * Builds a renderer registry entry for JSON Forms with a specified tester and rank.
 * This function is used to create custom renderer entries that can be registered
 * with JSON Forms to render specific UI elements based on the provided testing logic.
 *
 * @param renderer - The Vue component that will be used as the renderer. This component
 *                       should be capable of rendering a specific part of a form based on
 *                       the JSON schema and UI schema provided to JSON Forms.
 * @param tester - A tester function that determines whether the renderer should be used
 *                 for a given schema/UI schema combination. It evaluates the schema/UI schema
 *                 and returns a boolean indicating applicability and a rank indicating priority.
 * @param rank - An optional rank value (default is 3) to prioritize this renderer over others.
 *               Higher values indicate higher priority.
 * @returns A JSON Forms renderer registry entry object that includes the renderer component
 *          and its associated tester with applied ranking.
 *
 * @example
 * const customRendererEntry = buildRendererRegistryEntry(CustomRenderer, isObjectControl, 10);
 */
export function buildRendererRegistryEntry(
  renderer: any,
  tester: Tester,
  rank = 3
) {
  const entry: JsonFormsRendererRegistryEntry = {
    renderer: renderer,
    tester: rankWith(rank, tester),
  };
  return entry;
}

/**
 * Predefined renderer registry entry for rendering spaces or separators in forms.
 * Custom renderer for 'Spacer' elements with a UI schema `type` 'Spacer'.
 * Assigned a low priority rank of 1.
 */
export const spacerRendererEntry = buildRendererRegistryEntry(
  SpacerRenderer,
  uiTypeIs('Spacer'),
  1
);

/**
 * Predefined renderer registry entry for rendering labeled objects.
 * Injects (translated) object titles in case they were not specified in manually written UI Schemas
 * Assigned a high priority rank of 30, making it more likely to be chosen for applicable schemas.
 */
export const labeledObjectRendererEntry = buildRendererRegistryEntry(
  labeledObjectRenderer,
  isObjectControl,
  30
);

/**
 * Predefined renderer registry entry for rendering a custom component for 'institution' scoped fields.
 * Utilizes the `RORRenderer` Vue component with a `scopeEndsWith('institution')` tester.
 * Assigned the highest priority rank of 50, making it very likely to be chosen for institution fields.
 */
export const rorRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: RORRenderer,
  tester: rankWith(50, scopeEndsWith('institution')),
};

/**
 * Predefined renderer registry entry for rendering arrays, including those with nested objects,
 * to make sure that it's invoked for all arrays, except for arrays of primitives.
 * Utilizes the `ArrayRenderer` Vue component with a combined `isObjectArrayControl` or
 * `isObjectArrayWithNesting` tester. Assigned a moderate priority rank of 5.
 */
export const arrayRendererEntry = {
  renderer: ArrayRenderer,
  tester: rankWith(5, or(isObjectArrayControl, isObjectArrayWithNesting)),
};

export const StringControlRendererEntry = {
  renderer: StringControlRenderer,
  tester: withIncreasedRank(1, UpstreamStringControlRendererEntry.tester),
};

export const PublicationDatesRendererEntry = buildRendererRegistryEntry(
  PublicationDatesRenderer,
  scopeEndsWith('publicationDates'),
  50
);

export const IssnRendererEntry = buildRendererRegistryEntry(
  IssnRenderer,
  scopeEndsWith('Issn'),
  50
);

export const ContributorRoleRendererEntry = buildRendererRegistryEntry(
  ContributorRoleRenderer,
  scopeEndsWith('role'),
  50
);

export const ReferencesRendererEntry = {
  renderer: ReferencesRenderer,
  tester: rankWith(
    50,
    and(scopeEndsWith('/properties/references'), isStringControl)
  ),
};

export const EnumControlRendererEntry = {
  renderer: EnumControlRenderer,
  tester: withIncreasedRank(1, UpstreamEnumControlRendererEntry.tester),
};
export const DateControlRendererEntry = {
  renderer: DateControlRenderer,
  tester: withIncreasedRank(1, UpstreamDateControlRendererEntry.tester),
};
export const DateTimeControlRendererEntry = {
  renderer: DateTimeControlRenderer,
  tester: withIncreasedRank(1, UpstreamDateTimeControlRendererEntry.tester),
};

export const AutocompleteEnumControlRendererEntry = {
  renderer: AutocompleteEnumControlRenderer,
  tester: withIncreasedRank(
    1,
    UpstreamAutocompleteEnumControlRendererEntry.tester
  ),
};

export const ReviewListRendererEntry = {
  renderer: ReviewListRenderer,
  tester: rankWith(
    75,
    and(scopeEndsWith('/properties/references'), isStringControl)
  ),
};

export const ReviewTextRendererEntry = {
  renderer: ReviewTextControlRenderer,
  tester: rankWith(
    70,
    or(
      isStringControl,
      isEnumControl,
      isEnumControl,
      isNumberControl,
      isDateControl,
      isDateTimeControl,
      isBooleanControl,
      isIntegerControl
    )
  ),
};

export const ReviewGroupRendererEntry = {
  renderer: ReviewGroupRenderer,
  tester: rankWith(3, and(isLayout, uiTypeIs('Group'))),
};

export const ReviewArrayRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: ReviewArrayRenderer,
  tester: rankWith(5, or(isObjectArrayControl, isObjectArrayWithNesting)),
};

export const ReviewVerticalLayoutRendererEntry: JsonFormsRendererRegistryEntry =
  {
    renderer: ReviewVerticalLayoutRenderer,
    tester: withIncreasedRank(3, UpstreamVerticalLayoutRendererEntry.tester),
  };

export const MultiStringControlRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: MultiStringControlRenderer,
  tester: withIncreasedRank(1, UpstreamMultiStringControlRendererEntry.tester),
};

export const LanguageChooserRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: LanguageChooserRenderer,
  tester: rankWith(
    CustomRank.Highest,
    optionIs('x-rendering-hint', 'languageChooser')
  ),
};

export const OrcidRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: OrcidRenderer,
  tester: rankWith(CustomRank.Highest, optionIs('x-rendering-hint', 'orcid')),
};

export const DoiRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: DoiRenderer,
  tester: rankWith(CustomRank.Highest, optionIs('x-rendering-hint', 'doi')),
};

export const VerticalLayoutRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: VerticalLayoutRenderer,
  tester: withIncreasedRank(2, UpstreamVerticalLayoutRendererEntry.tester),
};

export const GroupRendererEntry: JsonFormsRendererRegistryEntry = {
  renderer: GroupRenderer,
  tester: withIncreasedRank(2, UpstreamGroupRendererEntry.tester),
};

export const baseHorizontalLabelRenderers = [ arrayRendererEntry,
  AutocompleteEnumControlRendererEntry,
  DateControlRendererEntry,
  DateTimeControlRendererEntry,
  EnumControlRendererEntry,
  labeledObjectRendererEntry,
  rorRendererEntry,
  spacerRendererEntry,
  StringControlRendererEntry,
  MultiStringControlRendererEntry,
  VerticalLayoutRendererEntry,
  GroupRendererEntry
]
