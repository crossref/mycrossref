import { containsPrimitiveValue } from '@/utils/helpers';
import articleUiSchema from './article.uischema.json';
import issueUiSchema from './issue.uischema.json';
import journalUiSchema from './journal.uischema.json';

interface ArticleFormData {
  article?: object;
  issue?: object;
  journal?: object;
}

/**
 * Dynamically generates UI schema based on the presence of certain properties
 * in the provided data object and whether those properties contain primitive values.
 */
const generateUiSchema = (data: ArticleFormData) => {
  const schema = {
    type: 'VerticalLayout',
    elements: [] as any[],
  };

  // Add UI schema elements based on data presence and content
  const addUiSchemaSection = (
    key: keyof ArticleFormData,
    uiSchemaPart: any,
    i18nKey: string
  ) => {
    if (data[key] && containsPrimitiveValue(data[key])) {
      schema.elements.push({
        type: 'Group',
        i18n: i18nKey,
        elements: [...uiSchemaPart.elements],
        scope: `#/properties/${key}`,
      });
    }
  };

  // Add sections conditionally based on data properties
  addUiSchemaSection(
    'article',
    articleUiSchema,
    'records_editForm_section_article'
  );
  addUiSchemaSection(
    'journal',
    journalUiSchema,
    'records_editForm_section_journal'
  );
  addUiSchemaSection('issue', issueUiSchema, 'records_editForm_section_issue');

  return schema;
};

export default generateUiSchema;
