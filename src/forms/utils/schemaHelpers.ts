import { UISchemaTester } from '@jsonforms/core';

const matchSchemaPath =
  (path: string): UISchemaTester =>
  (_schema, schemaPath) =>
    schemaPath === path ? 10 : -1;

const matchPath =
  (testPath: string): UISchemaTester =>
  (_schema, _schemaPath, path) =>
    path === testPath ? 10 : -1;

export { matchSchemaPath, matchPath };
