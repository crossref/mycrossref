import {getI18nKey} from "@jsonforms/core";
import localeCode, {LanguageCode} from "iso-639-1";

export const getI18nKeyFor = (suffix: string, control) => {
  return getI18nKey(
    control.control.value.schema,
    control.control.value.uischema,
    control.control.value.path,
    suffix
  );
};

export const getLanguageList = () => {
  return localeCode.getLanguages(localeCode.getAllCodes());
}

export const getLanguageCodes = () => {
  const langs = getLanguageList();

  return langs.map(lang => lang.code);
}
