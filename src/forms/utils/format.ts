import { unref } from "vue";

/**
 * Enum representing the trim options available.
 */
export enum TrimOption {
  Start = 'trimLeading',
  End = 'trimTrailing',
  Both = 'trimBoth',
  All = 'trimAll'
}

/**
 * Trims the input string based on the provided trim options.
 *
 * This function is designed to be called from JSON Forms renderers, where the applied options
 * are the merged config passed into the form control.
 *
 * @param input - The input to be trimmed. If the input is not a string, it is returned as is.
 * @param appliedOptions - The options object that may contain the trim option. This can be a reactive reference.
 * @returns The trimmed string based on the trim option or the original input if not a string.
 */
export const trim = (input: any, appliedOptions: any = {}) => {
  // Safely return if an improper type is passed
  if (typeof input !== 'string') {
    return input;
  }

  const options = unref(appliedOptions);

  // Trim the input string based on the trim option
  switch (options?.trim) {
    case TrimOption.Start:
      return input.trimStart();
    case TrimOption.End:
      return input.trimEnd();
    case TrimOption.Both:
      return input.trimStart().trimEnd();
    case TrimOption.All:
      return input.replace(/\s+/g, '');
    default:
      return input;
  }
}
