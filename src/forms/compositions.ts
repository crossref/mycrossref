import { computed } from 'vue';
import { getI18nKey } from '@jsonforms/core';
import { useTranslationNamespace } from '@/composable/useI18n';
import { I18nNamespace } from '@/i18n';
import { getI18nKeyPrefix } from '@jsonforms/core';
import { env } from '@/env';

export const DEFAULT_FORM_INPUT_DEBOUNCE_WAIT = 300

export const useI18nKeyPrefix = (control) => {
  return getI18nKeyPrefix(
    control.value.schema,
    control.value.uischema,
    control.value.path
  );
};

export const useInfoText = (vControl, i18n) => {
  const t = useTranslationNamespace(I18nNamespace.ContentRegistration);
  const tooltipMsgKey = getI18nKey(
    vControl.control.value.schema,
    vControl.control.value.uischema,
    vControl.control.value.path,
    'tooltip'
  );

  const tooltipLinkKey = getI18nKey(
    vControl.control.value.schema,
    vControl.control.value.uischema,
    vControl.control.value.path,
    'learn-more-link'
  );

  // Reactive translation for the tooltip message
  const tooltipMsg = computed(() => {
    const translation = i18n.value.translate(tooltipMsgKey);
    return typeof translation === 'string' && translation.trim() !== ''
      ? translation
      : undefined;
  });

  // Reactive translation for the learn more link
  const tooltipLearnMoreLink = computed(() => {
    const translation = i18n.value.translate(tooltipLinkKey);
    return typeof translation === 'string' && translation.trim() !== ''
      ? translation
      : undefined;
  });

  const description = computed(() => {
    return tooltipMsg.value ? '' : vControl.control.value.description;
  });

  const r = {
    msg: tooltipMsg,
    link: tooltipLearnMoreLink,
    description: description
  };
  if (env().isDevMode) {
    console.log(vControl.control.value.label, r);
  }
  return r;
};

export const useDescription = (infoText, control) => {

}
