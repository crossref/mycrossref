import { inject, InjectionKey, provide } from 'vue';
import { useActor, useInterpret } from '@xstate/vue';
import { createMachine, assign, InterpreterFrom } from 'xstate';
import { useInspector } from '@/statemachines/utils';
import { useDynamicRouter } from '@/composable/useDynamicRouter';
import { ContentRegistrationType } from '@/common/types';
import {RouteNames} from "@/router";

const { routerInstance } = useDynamicRouter();
const router = await routerInstance;

type NewRecordEvent =
  | { type: 'RECORD_TYPE_CHOSEN'; recordType: string }
  | { type: 'RECORD_NAME_UPDATED'; recordName: string }
  | { type: 'CONTINUE' }
  | { type: 'CANCEL' };

type NewRecordContext = {
  recordType: string;
  recordName: string;
  step: number;
};

export type NewRecord = {
  recordType: string;
  recordName: string;
  recordExtension: 'json';
};

export const newRecordMachine = createMachine<NewRecordContext, NewRecordEvent>(
  {
    predictableActionArguments: true,
    id: 'new-record',
    initial: 'chooseType',
    context: {
      recordType: ContentRegistrationType.JournalArticle,
      recordName: '',
      step: 1,
    },
    states: {
      chooseType: {
        on: {
          CANCEL: {
            actions: ['goToHome'],
          },
          RECORD_TYPE_CHOSEN: {
            target: '',
            actions: [
              assign({ recordType: (_context, event) => event.recordType }),
            ],
          },
          CONTINUE: {
            target: 'complete',
            cond: 'typeValid',
            actions: [assign({ step: (_context, _event) => 2 })],
          },
        },
      },
      complete: {
        type: 'final',
      },
    },
  },
  {
    guards: {
      typeValid: (context) => context.recordType.length > 0,
      nameValid: (context) => context.recordName.length > 0,
    },
    actions: {
      goToHome: () => {
        router.push({ name: RouteNames.records.index });
      },
    },
  }
);

export type NewRecordService = InterpreterFrom<typeof newRecordMachine>;
export const newRecordServiceSymbol: InjectionKey<NewRecordService> = Symbol(
  'create-record.machine.service'
);

export function newRecordRegistrationService() {
  const service = useInterpret(newRecordMachine, { devTools: useInspector() });
  return service;
}

export function provideNewRecordService() {
  const service = newRecordRegistrationService();
  /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
  // @ts-ignore
  provide(newRecordServiceSymbol, service);

  return service;
}

export function useNewRecordService() {
  const service = inject(newRecordServiceSymbol);

  if (!service) {
    throw new Error('New Record service not provided.');
  }

  return useActor(service);
}
