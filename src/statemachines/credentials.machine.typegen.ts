// This file was automatically generated. Edits will be overwritten

export interface Typegen0 {
  '@@xstate/typegen': true;
  internalEvents: {
    'xstate.init': { type: 'xstate.init' };
  };
  invokeSrcNameMap: {
    listenForAuthEvents: 'done.invoke.Credentials Machine.loginBoxShown:invocation[0]';
  };
  missingImplementations: {
    actions: never;
    delays: never;
    guards: never;
    services: never;
  };
  eventsCausingActions: {
    clearCredentials: 'CLEAR_CREDENTIALS';
    reportLoginBoxClosed: 'HIDE_LOGIN_BOX';
    sendLoginSuccessEvent: 'LOGIN_SUCCESS';
    setUserInStore: 'LOGIN_SUCCESS';
    updateCredentials: 'LOGIN_SUCCESS' | 'UPDATE_CREDENTIALS';
  };
  eventsCausingDelays: {};
  eventsCausingGuards: {};
  eventsCausingServices: {
    listenForAuthEvents: 'SHOW_LOGIN_BOX';
  };
  matchesStates: 'idle' | 'loginBoxShown';
  tags: never;
}
