import { env } from '@/env';

export const useInspector = (): boolean => {
  return env().isXstateInspector && env().isDevMode;
};
