import { useApi } from '@/composable/useApi';
import { generateUuid } from '@/utils/ids';
import { AddAPIKeyRequest } from '@/generated/openapi/apis/DefaultApi';
import { APIKey } from '@/generated/openapi/models';
import { RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE } from '@/utils/fetchers';
import { isNotEmptyString } from '@/utils/helpers';
import { Answerable } from '@serenity-js/core';
import { computed, ref } from 'vue';
import { useAlert } from './../composable/useAlert';
import {
  DeleteAPIKeyRequest,
  UpdateAPIKeyRequest,
} from './../generated/openapi/apis/DefaultApi';
import { extractErrorMessageAsync } from './../utils/error/extractMessage';

const { alertError, alertSuccess } = useAlert();

export type ApiKeyChangeRequest = {
  memberId: number;
  apiKey: string;
  active: boolean;
};

export const getTableRowClass = (keyId: Answerable<string>) =>
  `api-keys-table__row-by-id__${keyId}`;

const { api } = useApi();

/**
 * Check whether a apiKey is active
 *
 * @param apiKey
 * @param activeApiKeys
 * @returns boolean
 */
export const isApiKeyActive = (apiKey: APIKey) => apiKey.enabled;

export const addApiKey = (request: AddAPIKeyRequest) => {
  return api.addAPIKey(request);
};

/**
 * Get all active ApiKeys for an Organization
 * @param memberId number
 */
export const getApiKeysForOrganization = async (memberId: number) => {
  return await api.listAPIKeys({
    memberId: memberId,
    page: 1,
    pagesize: RESOURCE_MANAGER_API_DEFAULT_PAGE_SIZE,
  });
};

const apiKeys = ref<APIKey[] | null>([]);
const updatingKeys = ref<string[]>([]);

/**
 * A composable holding reactive state for an Organization's ApiKeys and the
 * means to update them
 * @param memberId number
 */
export const useApiKeys = (memberId: number) => {
  const loading = ref(false);
  const initialized = ref(false);

  const isKeyUpdating = computed(() => (key: APIKey) => {
    return !key.keyId || updatingKeys.value.includes(key.keyId);
  });

  const load = async () => {
    loading.value = true;
    try {
      getApiKeysForOrganization(memberId).then((response) => {
        apiKeys.value = response;
      });
    } finally {
      loading.value = false;
      initialized.value = true;
    }
  };

  const create = async (description: any) => {
    try {
      const request: AddAPIKeyRequest = {
        memberId: memberId,
      };
      // The API requires all APIKey properties to be set, but will overwrite all bar description.
      // Therefore we provide fake data for the other properties.
      if (isNotEmptyString(description)) {
        request.aPIKey = {
          description: description,
          issuedBy: generateUuid(),
          keyId: generateUuid(),
          name: generateUuid(),
        };
      }
      const addKeyResult = await api.addAPIKey(request);
      return addKeyResult;
      alertSuccess(`API key created`);
    } catch (e) {
      const message = await extractErrorMessageAsync(e);
      alertError(
        `There was an error creating the API Key: ${message}. Please try again.`
      );
    }
  };

  const update = async (request: UpdateAPIKeyRequest) => {
    try {
      await api.updateAPIKey(request);
      alertSuccess(`API key updated`);
      load();
    } catch (e) {
      console.error(e);
      const message = await extractErrorMessageAsync(e);
      alertError(
        `There was an error updating the API Key: ${message}. Please try again.`
      );
    } finally {
    }
  };

  const hasKeyId = (apiKey: APIKey): boolean => {
    return (
      apiKey.keyId !== undefined && apiKey.keyId !== null && apiKey.keyId !== ''
    );
  };

  const getKeyId = (apiKey: APIKey): string => {
    if (hasKeyId(apiKey) && apiKey.keyId) {
      const keyId: string = apiKey.keyId;
      return keyId;
    } else {
      throw new Error('No api keyId found.');
    }
  };

  const toggleKey = async (apiKey: APIKey) => {
    return apiKey.enabled ? disableKey(apiKey) : enableKey(apiKey);
  };

  const disableKey = async (apiKey: APIKey) => {
    const keyId = getKeyId(apiKey);
    return await api.disableAPIKey({ keyId });
  };

  const enableKey = async (apiKey: APIKey) => {
    const keyId = getKeyId(apiKey);
    return await api.enableAPIKey({ keyId });
  };

  const deleteKey = async (request: DeleteAPIKeyRequest) => {
    try {
      await api.deleteAPIKey(request);
      alertSuccess(`API key deleted`);
      load();
    } catch (e) {
      console.error(e);
      const message = await extractErrorMessageAsync(e);
      alertError(
        `There was an error deleting the API Key: ${message}. Please try again.`
      );
    }
  };

  const clearUpdatingKey = (key: string) => {
    updatingKeys.value = [];
  };

  return {
    apiKeys,
    load,
    create,
    update,
    deleteKey,
    updatingKeys,
    isKeyUpdating,
    clearUpdatingKey,
    toggleKey,
  };
};
