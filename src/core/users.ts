import { GetUsersRequest } from './../generated/openapi/apis/DefaultApi';
import { extractErrorCode } from './../utils/error/extractCode';
import { useApi } from '@/composable/useApi';

const { api } = useApi();

import {
  AddUserRequest,
  AddUserToOrgRequest,
  DeleteUserRequest,
} from '@/generated/openapi/apis/DefaultApi';
import { User } from '@/generated/openapi/models/User';
import { extractErrorMessageAsync } from '@/utils/error/extractMessage';

export const createUser = async (username: string) => {
  const newUser: User = {
    username: username,
  };
  const newUserRequest: AddUserRequest = {
    user: newUser,
    role: 'USER',
  };
  return await api.addUser(newUserRequest);
};

export const deleteUser = async (userId: string) => {
  const deleteUserRequest: DeleteUserRequest = {
    userId: userId,
  };
  return await api.deleteUser(deleteUserRequest);
};

export const addUserToOrg = async (user: User, memberId: number) => {
  const newUserRequest: AddUserToOrgRequest = {
    memberId: memberId,
    userId: user.userId as string,
  };
  return await api.addUserToOrg(newUserRequest);
};

export const createUserAndAddToOrg = async (
  username: string,
  memberId: number
): Promise<User> => {
  let user: User = {};
  try {
    const createUserResult = await createUser(username);
    user = createUserResult;
  } catch (e) {
    const code = await extractErrorCode(e);
    // if the user already exists, supress the error and continue
    if (code === 409) {
      const getUsersRequest: GetUsersRequest = {
        email: username,
      };
      // get the user's details
      const getUserResult = await api.getUsers(getUsersRequest);
      user = getUserResult[0];
    }
  }
  await addUserToOrg(user, memberId);
  return user;
};
