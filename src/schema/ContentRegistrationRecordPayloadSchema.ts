export const schema = {
  type: 'object',
  properties: {
    formData: { type: 'object' },
    fileName: { type: 'string' },
    recordType: { type: 'string' },
    recordId: { type: 'string' },
    recordExtension: { type: 'string' },
    schemaName: { type: 'string' },
    schemaVersion: { type: 'string' },
    recordName: { type: 'string' }, // deprecated
  },
  required: [
    'formData',
    'fileName',
    'recordType',
    'recordId',
    'recordExtension',
    'schemaName',
    'schemaVersion',
  ],
  additionalProperties: false,
};
