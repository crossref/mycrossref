// vite.config.js
import { fileURLToPath, URL } from 'node:url';

import { defineConfig, splitVendorChunkPlugin, loadEnv } from 'vite';
import { sentryVitePlugin } from '@sentry/vite-plugin';

import vue from '@vitejs/plugin-vue';
import vuetify from 'vite-plugin-vuetify';
import fs from 'fs';
import VitePluginVueDevTools from 'vite-plugin-vue-devtools';
import VueRouter from 'unplugin-vue-router/vite'

const certFile = '.certs/cert.pem';
const keyFile = '.certs/key.pem';

let httpsConfig = false;

if (fs.existsSync(certFile) && fs.existsSync(keyFile)) {
  httpsConfig = {
    key: fs.readFileSync(keyFile),
    cert: fs.readFileSync(certFile),
  };
}

export default ({ mode }) => {
  const env = loadEnv(mode, process.cwd(), '');
  const isDevelopment = mode === 'development';
  const routesFolders = [
    { src: 'src/pages' }
  ];

  // Only load the sandbox folder in development mode
  if (isDevelopment) {
    routesFolders.push({
      src: 'src/sandbox',
      path: 'sandbox/'
    });
  }

  // https://vitejs.dev/config/
  const viteConfig = defineConfig({
    plugins: [
        VueRouter({
          routesFolder:
            routesFolders
        }),
      vue(),
      vuetify({ autoImport: true }),
      sentryVitePlugin({
        authToken: process.env.SENTRY_AUTH_TOKEN,
        org: 'crossref',
        project: 'console',
      }),
        VitePluginVueDevTools(),
    ],
    define: {
      'process.env': { ...env, ENV_SET_BY_VITE: true },
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      },
      extensions: [
        '.js',
        '.json',
        '.jsx',
        '.mjs',
        '.ts',
        '.tsx',
        '.vue',
        '.css',
        '.scss',
      ],
    },
    envPrefix: ['VITE_', 'VUE_APP_'],
    optimizeDeps: { exclude: ['fsevents'] },
    server: {
      port: 8000,
      // https: httpsConfig,
      host: '0.0.0.0',
      headers: { 'Cross-Origin-Opener-Policy': 'same-origin-allow-popups' },
    },
    build: {
      sourcemap: true,
      rollupOptions: {
        output: {
          manualChunks: {
            xmlbuilder2: ['xmlbuilder2'],
            jsonata: ['jsonata'],
            sentry: ['@sentry/tracing', '@sentry/vue'],
          },
        },
      },
      target: 'es2022',
    },
  });
  return viteConfig;
};
