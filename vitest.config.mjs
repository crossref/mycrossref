import { fileURLToPath } from 'node:url';
import { mergeConfig } from 'vite';
import { configDefaults, defineConfig } from 'vitest/config';
import viteConfig from './vite.config.mjs';

export default mergeConfig(
  viteConfig('test'),
  defineConfig({
    test: {
      environment: 'jsdom',
      exclude: [
        ...configDefaults.exclude,
        // Exclude Playwright tests
        '**/playwright/**',
        '**/playwright-ct/**',
        // FIXME: make these tests work under Vitest and Vue 3
        // 'tests/unit/XmlDeposit/XmlDeposit.spec.ts',
        'tests/unit/Components/NotificationSnackbar.spec.ts',
        'tests/unit/Components/TheLanguageMenu.spec.ts',
        'tests/unit/Components/TheSearchBox.spec.ts',
      ],
      root: fileURLToPath(new URL('./', import.meta.url)),
      transformMode: {
        web: [/\.[jt]sx$/],
      },
      globals: true,
      deps: {
        inline: [
          'vuetify',
          '@pvale/vue-vuetify',
        ],
      },
      setupFiles: './vitest/setup.ts',
    },
  })
);
