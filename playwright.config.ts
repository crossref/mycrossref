import { fetchConfigFromEnv } from './src/utils/fetchConfigFromEnv';
import { defineConfig, devices } from '@playwright/test';
import type { PlaywrightTestConfig } from '@serenity-js/playwright-test';
import * as dotenv from 'dotenv';

dotenv.config();

const port = process.env.E2E_WEBSERVER_PORT || process.env.PORT || 3000;
const host = process.env.E2E_WEBSERVER_HOST || 'localhost';
const scheme = process.env.E2E_WEBSERVER_SCHEME || 'http';
const url = `${scheme}://${host}:${port}`;
const outputDirectory =
  process.env.E2E_TEST_RESULTS_OUTPUT_DIR || 'test-results/';
const config: PlaywrightTestConfig = defineConfig({
  /* Fail the build on CI if you accidentally left test.only in the source code. */
  forbidOnly: !!process.env.CI,
  /* Retry on CI only */
  retries: process.env.CI ? 2 : 0,
  testDir: './tests/playwright',
  testIgnore: '*/spec/**',
  outputDir: outputDirectory,
  /* Maximum time one test can run for. */
  timeout: 30_000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 5000,
  },
  /* Run tests in files in parallel */
  fullyParallel: true,
  /*
   * Opt out of parallel tests on CI.
   * And in other envs while we are resetting data between each test,
   * to avoid race conditions.
   */
  workers: process.env.CI ? 1 : 1,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  /*
  In CI, use the dot reporter (short output) and also the html reporter
  (easy acceptance testing and output reviewing) but don't try to open it
  in a browser automatically after the tests complete.
  In non-CI environments, use the list reporter for easy-reading terse output.
  */
  reporter: process.env.CI ? [['dot'], ['html', { open: 'never' }]] : 'list',
  use: {
    storageState: 'storageState.json',
    baseURL: url,
    permissions: ['clipboard-read', 'clipboard-write'],
    // Always record traces
    trace: 'on',
    /* Maximum time each action such as `click()` can take. Defaults to 0 (no limit). */
    actionTimeout: 0,
    headless: true,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    // Always record videos
    video: 'on',
    // Only screenshot failing tests
    screenshot: 'only-on-failure',
    acceptDownloads: true,
    // CI speed could be reduced if the videos are too fast for human reviewers
    launchOptions: {
      slowMo: process.env.CI ? 0 : 0,
    },
  },
  /* Configure projects for major browsers */
  projects: [
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
    },
  ],
  webServer: {
    command: 'npm run serve-e2e',
    url: url,
    // Always re-use an existing web service if it's running - in CI
    // we provide the app through an nginx container.
    reuseExistingServer: true,
    stdout: 'ignore',
    stderr: 'pipe',
  },
});
export default config;
