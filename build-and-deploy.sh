#!/bin/sh

# Exit on any non zero return value
set -ex

sh build.sh
sh deploy.sh
